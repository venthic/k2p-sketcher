import * as React from "react";
import {DiagramWidget} from '@projecstorm-react-diagrams';
import TrayWidget from '../components/TrayWidget';
import TrayItemWidget from '../components/TrayItemWidget';
import SideBar from '../components/SketchSideBar';
import {observer} from 'mobx-react-lite';
import {StoreContext} from '../App';
import Grid from '@material-ui/core/Grid';
import Button from "@material-ui/core/Button/Button";
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Box from '@material-ui/core/Box';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import logo from '../static/img/logo_dark_blue.png';
import styled from '@emotion/styled';
import SendMappingCatalog from "../components/SendMappingCatalog";
import PublishTwoToneIcon from '@material-ui/icons/PublishTwoTone';
import GetAppTwoToneIcon from '@material-ui/icons/GetAppTwoTone';
import InfoTwoToneIcon from '@material-ui/icons/InfoTwoTone';
import { makeStyles } from '@material-ui/core/styles';
import pattern from '../static/img/absurdidad.png';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: '20px 16px',
        color: theme.palette.text.secondary,
        backgroundColor: '#09142714',
        
      },
      wrapper: {
        background: `url(${pattern})`,
        backgroundRepeat: 'repeat',
        height: '100vh',
      },
      welcomeTxt: {
        color: '#003347',
      },
      snackbar: {
        backgroundColor: '#003347',
        boxShadow: 'none',
        textAlign: 'justify',
        marginBottom: '20px',
        fontSize:'0.8rem',
        minWidth:'0px'
      },
      logoBox: {
        padding: '25px',
        textAlign: 'center',
      },
      welcomeTxt: {
        fontSize: '24px',
        textAlign: 'center',
        color: '#003347',
        fontWeight:700
      },
      tooltipButton: {
        color: '#003347',
        fontSize:'22px'
    },
  }));

const Sketch = observer((props) => {
    //const sketchboard = React.useContext(StoreContext);
    const classes = useStyles();
        
    return (
        <div className={classes.wrapper}>
            <SideBar/>
            <Grid style={{height:'100vh'}} container spacing={24}>
                <Grid item xs={3}>
                    {/*<DataInput handleFile={(file) => {
                        const reader = new FileReader();
                        const rABS = !!reader.readAsBinaryString;
                        reader.onload = (e) => {
                            const str = e.target.result;
                            sketchboard.load(str);
                        };
                        if(rABS) reader.readAsBinaryString(file); else reader.readAsArrayBuffer(file);
                    }}/>*/}
                    {/*<Box className={classes.snackbar}><Button onClick={(e)=>{sketchboard.generateScreenshot()}}>Img</Button>You can drag & drop the units from the left panel to the workflow area. You can delete an existing unit by selecting it (Shift+mouse click) and pressing the delete button or clicking on the x button(up right of the units)</Box>*/}
                    <Box className={classes.logoBox}>
                        <img width={'70%'} src={logo} alt="GWPP - K2P"/>
                    </Box>
                    <Paper className={classes.paper}>
                    <Grid container>
                    <Grid item xs={12} alignContent='center'>
                    <Typography variant="h4" component="h4" align='center' className={classes.welcomeTxt}>
                            K2P - Sketcher Tool
                    </Typography><br/>
                    <SnackbarContent
                        className={classes.snackbar}
                        message={
                        'Sketch your Wastewater Treatment System by dragging & dropping from the three main node types to the canvas. Connect the nodes and click on them to provide more information.'
                        }></SnackbarContent>
                        
                    </Grid>
                    </Grid>
                    
                    <TrayWidget >
                        <TrayItemWidget model={{ type: 'out' }} color="rgb(145, 103, 103)" ><center><span class="fa-stack fa-2x"><i class="fas fa-circle fa-stack-2x"></i><i className="fa fa-poo fa-stack-1x fa-inverse" style={{color:'rgb(145, 103, 103)'}}></i></span><div style={{fontSize:'0.8vw'}}>Source</div></center></TrayItemWidget>
                        <TrayItemWidget model={{ type: 'in-out' }} color="#507D8C" ><center><span class="fa-stack fa-2x"><i class="fas fa-circle fa-stack-2x"></i><i className="fa fa-cog fa-stack-1x fa-inverse" style={{color:'#507D8C'}}></i></span><div style={{fontSize:'0.8vw'}}>Unit Process</div></center></TrayItemWidget>
                        <TrayItemWidget model={{ type: 'in' }} color="rgb(0, 178, 134)" ><center><span class="fa-stack fa-2x"><i class="fas fa-circle fa-stack-2x"></i><i className="fa fa-tint fa-stack-1x fa-inverse" style={{color:'rgb(0, 178, 134)'}}></i></span><div style={{fontSize:'0.8vw'}}>End Use</div></center></TrayItemWidget>
                    </TrayWidget>
                    <Typography variant={'h6'} style={{color: '#003347',  textAlign:'center', paddingTop: '20px', paddingBottom: '20px'}}>
                    Re-use options
                    
                        <Tooltip 
                            title={
                            <React.Fragment>
                                <p>By choosing to Publish your Sketch, you submit the information of your Wastewater Treatment System to the K2P Database. Your data will be subject to curation and re-use by other users.</p> 
                                <p>By choosing to Download, you will get a JSON file corresponding to your Sketch locally, that can later be uploaded online to feed the Pathogen Flow Tool.</p> 
                            </React.Fragment>
                            }
                        >
                            <InfoTwoToneIcon className={classes.tooltipButton}></InfoTwoToneIcon>
                        </Tooltip>
                        
                    </Typography>
                    <ButtonGroup fullWidth aria-label="Full width outlined button group" size="large" style={{height:'60px'}}>
                        <Button style={{
                                    backgroundColor:'#66af34',
                                    color: '#fff'
                                }} 
                                onClick={(e)=>{alert('wew');/*sketchboard.setCatalogueModalState(true)*/}}>
                                    
                            <PublishTwoToneIcon fontSize="large"/> Publish
                        </Button>
                        <Button style={{ 
                                    color: '#fff',
                                    backgroundColor: 'rgb(24, 150, 85)'
                                }}
                                onClick={(e)=>{alert('wew');/*sketchboard.download()*/}}>
                            <GetAppTwoToneIcon fontSize="large"/> Download
                        </Button>
                        
                    </ButtonGroup>
                   
                    {/*<Button style={{border:'1px solid grey', margin:'10px', fontSize:'24px'}} onClick={(e)=>{sketchboard.save()}}>save the diagram for future reuse</Button>*/}
                    </Paper>
                </Grid>
                <Grid item xs={9}>
                    {/* <DiagramLayer id="diagram-layer" onDrop={event => {sketchboard.onSketchDrop(event)}} onDragOver={event => {event.preventDefault(); }}>
                        <DiagramWidget supressDeleteAction={false} deleteKeys= {[46,8]} maxNumberPointsPerLink={0} diagramEngine={sketchboard.engine} />
                    </DiagramLayer> */}

                    
                </Grid>
            </Grid>
            <SendMappingCatalog/>
        </div>
    );
});

const DiagramLayer = styled.div`
    padding-right: 10px;
`;
export default Sketch;
