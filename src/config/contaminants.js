export const contaminants = [
  {
      name: 'Barium', 
      mclg: 2,
      mcl: 2,
      al: null,
      default: 0.01,
      category: 'Inorganic Contaminants',
      effects: 'Increase in blood pressure',
      sources: 'Discharge of drilling wastes; discharge from metal refineries; erosion of natural deposits',
      unit: 'ppm'
  },
  {
    name: 'Nitrate', 
    mclg: 10,
    mcl: 10,
    al: null,
    default: 9.33,
    category: 'Inorganic Contaminants',
    effects: 'Infants below the age of six months who drink water containing nitrate in excess of the MCL could become seriously ill and, if untreated, may die. Symptoms include shortness of breath and blue-baby syndrome.',
    sources: 'Runoff from fertilizer use; leaking from septic tanks, sewage; erosion of natural deposits',
    unit: 'ppm'
  },
  {
    name: 'Fluoride', 
    mclg: 4,
    mcl: 4,
    al: null,
    default: 0.76,
    category: 'Inorganic Contaminants',
    effects: 'Bone disease (pain and tenderness of the bones); Children may get mottled teeth',
    sources: 'Water additive which promotes strong teeth; erosion of natural deposits; discharge from fertilizer and aluminum factories',
    unit: 'ppm'
},
{
    name: 'Turbidity', 
    mclg: null,
    mcl: null,
    al: null,
    default: 0.17,
    category: 'Inorganic Contaminants',
    effects: 'Turbidity is a measure of the cloudiness of water. It is used to indicate water quality and filtration effectiveness (such as whether disease-causing organisms are present). Higher turbidity levels are often associated with higher levels of disease-causing microorganisms such as viruses, parasites and some bacteria. These organisms can cause symptoms such as nausea, cramps, diarrhea, and associated headaches.',
    sources: 'Soil runoff',
    unit: 'NTU'
},
{
    name: 'Total Trihalomethanes', 
    mclg: 80,
    mcl: null,
    al: null,
    default: 43.7,
    category: 'Disinfectants & Disinfection By-Products',
    effects: '',
    sources: 'Byproduct of drinking water disinfection',
    unit: 'ppb'
},
{
    name: 'Haloacetic Acids', 
    mclg: null,
    mcl: 60,
    al: null,
    default: 17.75,
    category: 'Disinfectants & Disinfection By-Products',
    effects: '',
    sources: 'Byproduct of drinking water disinfection',
    unit: 'ppb'
},
{
    name: 'Chlorine', 
    mclg: 4,
    mcl: 4,
    al: null,
    default: 3.4,
    category: 'Disinfectants & Disinfection By-Products',
    effects: '',
    source: 'Water additive used to control microbes',
    unit: 'ppm'
},
{
    name: 'Copper', 
    mclg: 1.3,
    mcl: null,
    al: 1.3,
    default: 0.442,
    category: 'Lead & Copper',
    effects: '',
    source: 'Corrosion of household plumbing systems; erosion of natural deposits; leaching from wood preservatives',
    unit: 'ppb'
},
{
    name: 'Lead', 
    mclg: 0,
    mcl: null,
    al: 15,
    default: 2,
    category: 'Lead & Copper',
    effects: 'If present, elevated levels of lead can cause serious health problems, especially for pregnant women and young children.  Lead in drinking water is primarily from materials and components associatedwith service lines and home plumbing.  We are responsible for providing high quality drinking water, but cannot control the variety of materials used in plumbing components.  When your water has been sitting for several hours, you can minimize the potential for lead exposure by flushing your tap for 30 seconds to 2 minutes before using water for drinking or cooking. ',
    source: 'Corrosion of household plumbing systems; erosion of natural deposits.',
    unit: 'ppm'
},
{
    name: 'Beta/photon emitters', 
    mclg: 0,
    mcl: 4,
    al: null,
    default: 3.5,
    category: 'Radiological Contaminants',
    effects: 'If present, elevated levels of lead can cause serious health problems, especially for pregnant women and young children.  Lead in drinking water is primarily from materials and components associatedwith service lines and home plumbing.  We are responsible for providing high quality drinking water, but cannot control the variety of materials used in plumbing components.  When your water has been sitting for several hours, you can minimize the potential for lead exposure by flushing your tap for 30 seconds to 2 minutes before using water for drinking or cooking. ',
    source: 'Decay of natural and man-made deposits.',
    unit: 'pCi/L'
},
{
    name: 'Cryptosporidium', 
    mclg: 4,
    mcl: null,
    al: null,
    default: 3.5,
    category: 'Microbial contaminants',
    effects: 'Gastrointestinal illness (such as diarrhea, vomiting, and cramps)',
    source: 'Human and animal fecal waste',
    unit: 'TT'
},
];