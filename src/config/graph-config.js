import * as React from 'react';
import { observable, action, reaction, computed } from 'mobx';

export const NODE_KEY = 'id'; // Key used to identify nodes

// These keys are arbitrary (but must match the config)
// However, GraphView renders text differently for empty types
// so this has to be passed in if that behavior is desired.
export const UNIT_PROCESS_TYPE = 'unitProcess'; 
export const SOURCE_TYPE = 'source';
export const END_USE_TYPE = 'endUse';
export const SPECIAL_CHILD_SUBTYPE = 'specialChild';
export const EMPTY_EDGE_TYPE = 'emptyEdge';
export const SPECIAL_EDGE_TYPE = 'specialEdge';
export const COMPLEX_CIRCLE_TYPE = 'complexCircle';

export const nodeTypes = [UNIT_PROCESS_TYPE, SOURCE_TYPE, END_USE_TYPE];
export const edgeTypes = [EMPTY_EDGE_TYPE, SPECIAL_EDGE_TYPE];

const EmptyNodeShape = (
  <symbol viewBox="0 0 154 154" width="154" height="154" id="unitProcess" fill="#fff">
  <circle cx="77" cy="77" r="76" />
  </symbol>
);

const SpecialShape = (
  <symbol viewBox="-27 0 154 154" id="special" width="154" height="154" id="source" fill="#fff">
    <rect transform="translate(50) rotate(45)" width="109" height="109" />
  </symbol>
);

const SkinnyShape = (
  <symbol viewBox="0 0 154 84" width="154" height="84" id="endUse" fill="#fff">
  <rect x="0" y="0" rx="2" ry="2" width="154" height="84" />
</symbol>
);

const SpecialChildShape = (
  <symbol viewBox="0 0 154 154" id="specialChild">
    <rect
      x="2.5"
      y="0"
      width="154"
      height="154"
      fill="rgba(30, 144, 255, 0.12)"
    />
  </symbol>
);

const EmptyEdgeShape = (
  <symbol viewBox="0 0 50 50" id="emptyEdge">
  </symbol>
);

export default {
  EdgeTypes: {
    emptyEdge: {
      shape: EmptyEdgeShape,
      shapeId: '#emptyEdge',
    }
  },
  NodeSubtypes: {
    specialChild: {
      shape: SpecialChildShape,
      shapeId: '#specialChild',
    },
  },
  NodeTypes: {
    unitProcess: {
      shape: EmptyNodeShape,
      shapeId: '#unitProcess',
      typeText: 'Unit process',
      allowedConnections: [UNIT_PROCESS_TYPE, END_USE_TYPE]
    },
    source: {
      shape: SpecialShape,
      shapeId: '#source',
      typeText: 'Source',
      allowedConnections: [UNIT_PROCESS_TYPE, END_USE_TYPE]
    },
    endUse: {
      shape: SkinnyShape,
      shapeId: '#endUse',
      typeText: 'End use',
      allowedConnections: []
    }
  },
};