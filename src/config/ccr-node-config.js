import {
  SOURCE_TYPE,
  UNIT_PROCESS_TYPE,
  END_USE_TYPE
} from './graph-config'

export const subTypes = [
  {
    name: "Water Source",
    machineName: "water source",
    subtypeOf: SOURCE_TYPE,
    icon: "",
    fields: [
      {
        label: "Description",
        propName: "description",
        conditional: false,
        required: true,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
      {
        label: "Cryptosporidium concentration",
        propName: "cryptoConcentration",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Source Water Assessment",
        propName: "flowRate",
        conditional: true,
        required: false,
        type: "textarea",
      },
      {
        label: "SWA contact information",
        propName: "swa_contact",
        conditional: true,
        required: false,
        type: "textfield",
      },
      {
        label: "SWA attachment",
        propName: "swa_attachment",
        conditional: true,
        required: false,
        type: "textfield",
      },
      {
        label: "Location",
        propName: "location",
        conditional: true,
        required: true,
        type: "geofield",
      }
    ]
  },
  {
    name: "Rapid Mixing",
    machineName: "rapid mixing",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "anaerobic_pond",
    fields: [
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Flocculation",
    machineName: "flocculation",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Filtration",
    machineName: "media filter",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "media_filter",
    fields: [
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      }
    ]
  },
  {
    name: "Sedimentation",
    machineName: "settler or clarifier",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "settler",
    fields: [
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Disinfection",
    machineName: "disinfection",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
      {
        label: "Type of disinfectant",
        propName: "typeOfDisinfectant",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Free Chlorine', value:'free chlorine'}, 
          {key: 'Ozone', value:'ozone'},
          {key: 'Chlorine', value:'chlorine dioxide'}  
        ],
        defaultValue: ''
      },
      {
        label: "Dose (CT Value) in mg-min/L",
        propName: "dose",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "pH",
        propName: "pH",
        conditional: true,
        required: true,
        type: "number",
      },
    {
      label: "Notes",
      propName: "description",
      conditional: false,
      required: false,
      type: "textarea",
      placeholder: 'Provide additional notes about the node.'
    }
    ]
  },
  {
    name: "Consumer",
    machineName: "consumer",
    subtypeOf: END_USE_TYPE,
    icon: "consumer",
    fields: [
      
    {
      label: "Population served",
      propName: "population",
      conditional: true,
      required: true,
      type: "number",
    },
      {
        label: "Description",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  }
]
