import {
  SOURCE_TYPE,
  UNIT_PROCESS_TYPE,
  END_USE_TYPE
} from './graph-config'

export const subTypes = [
  {
    name: "Sewerage",
    machineName: "sewerage",
    subtypeOf: SOURCE_TYPE,
    icon: "",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Average Daily Flow Rate (m3/day)",
        propName: "flowRate",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Fecal Sludge",
    machineName: "fecal sludge",
    subtypeOf: SOURCE_TYPE,
    icon: "",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Average Daily Volume Received (m3/day)",
        propName: "flowRate",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Anaerobic Pond",
    machineName: "anaerobic pond",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "anaerobic_pond",
    fields: [
      {
        label: "Surface Area (m2)",
        propName: "surfaceArea",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Depth (meters)",
        propName: "depth",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Facultative Pond",
    machineName: "facultative pond",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
      {
        label: "Surface Area (m2)",
        propName: "surfaceArea",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Depth (meters)",
        propName: "depth",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Maturation Pond",
    machineName: "maturation pond",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
      {
        label: "Surface Area (m2)",
        propName: "surfaceArea",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Depth (meters)",
        propName: "depth",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Aerated Pond",
    machineName: "aerated pond",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "aerated_pond",
    fields: [
      {
        label: "Surface Area (m2)",
        propName: "surfaceArea",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Depth (meters)",
        propName: "depth",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Trickling Filter",
    machineName: "trickling filter",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "trickling_filter",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Free-Water Surface Constructed Wetland",
    machineName: "fws wetland",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "free_water_surface_constructed_wetland",
    fields: [
      {
        label: "Surface Area (m2)",
        propName: "surfaceArea",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Depth (meters)",
        propName: "depth",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Subsurface Constructed Wetland",
    machineName: "ss wetland",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "subsurface_constructed_wetland",
    fields: [
      {
        label: "Hydraulic Retention Time (days)",
        propName: "retentionTime",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Type of Flow",
        propName: "typeOfFlow",
        conditional: true,
        required: true,
        type: "select",
        options: [
          {key: 'Horizontal Flow', value:'horizontal flow'}, 
          {key: 'Vertical flow', value:'vertical flow'} 
        ],
        defaultValue: ''
      },
      {
        label: "Hydraulic Loading Rate (cm / day)",
        propName: "HydraulicLoading",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Imhoff Tank",
    machineName: "imhoff tank",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "imhoff_tank2",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Anaerobic Baffled Reactor (ABR)",
    machineName: "anaerobic baffled reactor",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "anaerobic_baffled_reactor",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Media Filter",
    machineName: "media filter",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "media_filter",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Upflow Anaerobic Sludge Blanket Reactor (UASB)",
    machineName: "uasb reactor",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "upflow_anaerobic_sludge_blanket_reactor",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Activated Sludge",
    machineName: "activated sludge",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "activated_sludge",
    fields: [
      {
        label: "Hydraulic Retention Time (hours)",
        propName: "retentionTime",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Solids Retention Time (days)",
        propName: "solidsRetention",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Settler or Clarifier",
    machineName: "settler or clarifier",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "settler",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Sludge Drying Bed",
    machineName: "sludge drying bed",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "sludge_drying_bed",
    fields: [
      {
        label: "Initial Moisture Content (%)",
        propName: "moistureContent",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Holding Time (days)",
        propName: "holdingTime",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'solid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Composting Facility",
    machineName: "composting",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "co-composting",
    fields: [
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'solid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Anaerobic Sludge Digester",
    machineName: "anaerobic digester",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
      {
        label: "Volume (m3)",
        propName: "volume",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Temperature (Celsius)",
        propName: "temperature",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Chlorination",
    machineName: "chlorination",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
    {
      label: "Initial Free Chlorine (mg/L)",
      propName: "concentration",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "Contact Time (hours)",
      propName: "contactTime",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "Matrix",
      propName: "matrix",
      conditional: false,
      required: true,
      type: "select",
      options: [
        {key: 'Liquid', value:'liquid'},
        {key: 'Solid', value:'solid'}
      ],
      defaultValue: 'liquid'
    },
    {
      label: "Notes",
      propName: "description",
      conditional: false,
      required: false,
      type: "textarea",
      placeholder: 'Provide additional notes about the node.'
    },
    ]
  },
  {
    name: "Ammonia Disinfection",
    machineName: "ammonia",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
    {
      label: "Concentration (mg/L as N)",
      propName: "concentration",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "pH",
      propName: "pH",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "Contact Time (days)",
      propName: "contactTime",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "Matrix",
      propName: "matrix",
      conditional: false,
      required: true,
      type: "select",
      options: [
        {key: 'Liquid', value:'liquid'},
        {key: 'Solid', value:'solid'}
      ],
      defaultValue: 'liquid'
    },
    {
      label: "Notes",
      propName: "description",
      conditional: false,
      required: false,
      type: "textarea",
      placeholder: 'Provide additional notes about the node.'
    },
    ]
  },
  {
    name: "Lime Treatment",
    machineName: "lime treatment",
    subtypeOf: UNIT_PROCESS_TYPE,
    icon: "",
    fields: [
    {
      label: "Dose (g per dry kg)",
      propName: "dose",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "pH",
      propName: "pH",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "Contact Time (days)",
      propName: "contactTime",
      conditional: true,
      required: true,
      type: "number",
    },
    {
      label: "Matrix",
      propName: "matrix",
      conditional: false,
      required: true,
      type: "select",
      options: [
        {key: 'Liquid', value:'liquid'},
        {key: 'Solid', value:'solid'}
      ],
      defaultValue: 'liquid'
    },
    {
      label: "Notes",
      propName: "description",
      conditional: false,
      required: false,
      type: "textarea",
      placeholder: 'Provide additional notes about the node.'
    },
    ]
  },
  {
    name: "Discharge to Surface Water",
    machineName: "discharge to surface water",
    subtypeOf: END_USE_TYPE,
    icon: "discharge_surface_water",
    fields: [
      {
        label: "Use Category",
        propName: "useCategory",
        conditional: true,
        required: true,
        type: "select",
        options: [
          {key: 'drinking water source', value:'Drinking water source'}, 
          {key: 'recreational water', value:'Recreational water'},
          {key: 'other', value:'Other (no exposure)'}  
        ],
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Irrigation",
    machineName: "irrigation",
    subtypeOf: END_USE_TYPE,
    icon: "irrigation",
    fields: [
      {
        label: "Use Category",
        propName: "useCategory",
        conditional: true,
        required: true,
        type: "select",
        options: [
          {key: 'edible crops consumed raw', value:'Edible crops consumed raw'}, 
          {key: 'edible crops consumed cooked', value:'Edible crops consumed cooked'},
          {key: 'crops for animal feed', value:'Crops for animal feed'},
          {key: 'other', value:'Other non-edible crops'}  
        ],
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Groundwater Recharge",
    machineName: "groundwater recharge",
    subtypeOf: END_USE_TYPE,
    icon: "groundwater_recharge",
    fields: [
      {
        label: "Depth to groundwater",
        propName: "depthGroundwater",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Fill and Cover",
    machineName: "fill and cover",
    subtypeOf: END_USE_TYPE,
    icon: "fill_and_cover",
    fields: [
      {
        label: "Depth to groundwater",
        propName: "depthGroundwater",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Soil Type",
        propName: "soilType",
        conditional: true,
        required: true,
        type: "number",
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Land Application",
    machineName: "land application",
    subtypeOf: END_USE_TYPE,
    icon: "land_application2",
    fields: [
      {
        label: "Use Category",
        propName: "useCategory",
        conditional: true,
        required: true,
        type: "select",
        options: [
          {key: 'edible crops consumed raw', value:'Edible crops consumed raw'}, 
          {key: 'edible crops consumed cooked', value:'Edible crops consumed cooked'},
          {key: 'crops for animal feed', value:'Crops for animal feed'},
          {key: 'other', value:'Other non-edible crops'}  
        ],
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  },
  {
    name: "Dump in Surface Water",
    machineName: "dump in surface water",
    subtypeOf: END_USE_TYPE,
    icon: "dump_surface_water",
    fields: [
      {
        label: "Use Category",
        propName: "useCategory",
        conditional: true,
        required: true,
        type: "select",
        options: [
          {key: 'drinking water source', value:'Drinking water source'}, 
          {key: 'recreational water', value:'Recreational water'},
          {key: 'other', value:'Other (no exposure)'}  
        ],
      },
      {
        label: "Matrix",
        propName: "matrix",
        conditional: false,
        required: true,
        type: "select",
        options: [
          {key: 'Liquid', value:'liquid'}, 
          {key: 'Solid', value:'solid'} 
        ],
        defaultValue: 'liquid'
      },
      {
        label: "Notes",
        propName: "description",
        conditional: false,
        required: false,
        type: "textarea",
        placeholder: 'Provide additional notes about the node.'
      },
    ]
  }
]
