import * as React from 'react';
import fetchJsonp from 'fetch-jsonp';

export default function loadCKANResource(id) {

    fetchJsonp('http://data.waterpathogens.org/api/3/action/package_show?id=a1423a05-7680-4d1c-8d67-082fbeb00a50&callback=cb', {
        jsonpCallbackFunction: 'cb'
    })
    .then(function(response) {
    return response.json()
    }).then(function(json) {
    return json;
    }).catch(function(ex) {
    console.log('parsing failed', ex)
    })
}