import { createMuiTheme } from '@material-ui/core/styles';
import { grey } from '@material-ui/core/colors';

const defaultTheme = {
    palette: {
        primary: {
            light: '#003347',
            main: '#003347',
            dark: '#003347',
        },
        secondary: {
            light: '#fff5f8',
            main: '#42919e',
            dark: '#33475b',
        },
        warning: {
            main: '#ffc071',
            dark: '#ffb25e',
        },
        error: {
            xLight: '#FF2100',
            main: '#C60013',
            dark: '#8E0013',
        },
        success: {
            main: '#4CBF1F',
            xLight: '#4CBF1F',
            dark: '#179c7e',
        },
    },
    typography: {
        fontFamily: "'Cabin', sans-serif",
        fontSize: 12,
        fontWeightLight: 300, // Work Sans
        fontWeightRegular: 400, // Work Sans
        fontWeightMedium: 700, // Roboto Condensed
        fontFamilySecondary: "'Cabin', sans-serif",
    },
};

const theme = (theme = defaultTheme) => {
    const rawTheme = createMuiTheme(theme);
    const fontHeader = {
        color: rawTheme.palette.text.primary,
        fontWeight: rawTheme.typography.fontWeightMedium,
        fontFamily: rawTheme.typography.fontFamilySecondary,
        textTransform: 'uppercase',
    };
    return {
        ...rawTheme,
        overrides: {
            MuiCard: {
                root: {
                    color: '#343434',
                    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
                    background: '#fff',
                    border: '1px solid #e7e7e7',
                    borderRadius: '3px',
                    padding: '10px',
                    fontFamily: 'Cabin'
                }
            },
            MuiSpeedDial: {
                primary: {
                    color: '#343434',
                    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
                    background: '#fff',
                    border: '1px solid #e7e7e7',
                    borderRadius: '3px',
                    padding: '10px',
                    fontFamily: 'Cabin'
                }
            },
            tooltipPlacementBottom: {
                    fontSize: 30
                
            }
        },
        palette: {
            ...rawTheme.palette,
            background: {
                ...rawTheme.palette.background,
                default: rawTheme.palette.common.white,
                placeholder: grey[200],
            },
        },
        typography: {
            ...rawTheme.typography,
            fontHeader,

            h1: {
                ...rawTheme.typography.h1,
                ...fontHeader,
                letterSpacing: 0,
                fontSize: 24,
            },
            h2: {
                ...rawTheme.typography.h2,
                ...fontHeader,
                fontSize: 20,
            },
            h3: {
                ...rawTheme.typography.h3,
                ...fontHeader,
                fontSize: 18,
            },
            h4: {
                ...rawTheme.typography.h4,
                ...fontHeader,
                fontSize: 15,
            },
            h5: {
                ...rawTheme.typography.h5,
                fontSize: 14,
                fontWeight: rawTheme.typography.fontWeightLight,
            },
            h6: {
                ...rawTheme.typography.h6,
                ...fontHeader,
                fontSize: 18,
            },
            subtitle1: {
                ...rawTheme.typography.subtitle1,
                fontSize: 18,
                lineHeight: 1
            },
            subtitle2: {
                ...rawTheme.typography.subtitle2,
                fontSize: 16,
                lineHeight: 1
            },
            body1: {
                ...rawTheme.typography.body2,
                fontWeight: rawTheme.typography.fontWeightRegular,
                fontSize: 16,
            },
            body2: {
                ...rawTheme.typography.body1,
                fontSize: 14,
            }
        }
    };
};

export default theme;
