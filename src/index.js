import React from 'react';
import ReactDOM from 'react-dom';
import {configure} from "mobx";
import App from './App';
import registerServiceWorker from './registerServiceWorker';
configure({enforceActions: 'observed'});

ReactDOM.render(<App dev={true}/>, document.getElementById('root'));

registerServiceWorker();
