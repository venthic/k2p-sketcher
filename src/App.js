import React from 'react';
import './styles/sketch.css';
import './styles/react-tags.css';
import './static/fonts/fonts.css';
import { ThemeProvider } from "@material-ui/core/styles";
import Graph from './components/Graph';
import WaterTreatmentGraph from './components/ccr/WaterTreatmentGraph';
import CCRReport from './components/ccr/CCRReport';
import theme from "./config/Theme.js";
import {
    BrowserRouter as Router,
    Switch,
    Route
  } from "react-router-dom";

const App = (props) =>{
    return(
        <ThemeProvider theme={theme()}>
            <Router basename="/sketcher">
                
                <Switch>
                    <Route exact path="/">
                        <Graph/>
                    </Route>
                    <Route path="/ccr">
                        <WaterTreatmentGraph/>
                    </Route>
                    <Route exact path="/ccr-preview">
                        <CCRReport/>
                    </Route>
                </Switch>
            </Router>
        </ThemeProvider>
    )
}
export default App;
