import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import MuiAlert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import Divider from '@material-ui/core/Divider';
import LRVforPDF from "./LRVforPDF.js";
import SankeyChart from "./SankeyChart.js";
import { SOURCE_TYPE } from "../config/graph-config.js";

function Alert(props) {
    return <MuiAlert elevation={2} {...props} />;
} 

const GenerateReportModal = observer(({ context }) => {

    const [error, setError] = useState(false);
    const [errorName, setErrorName] = useState(false);
    const [errorAuthorName, setErrorAuthorName] = useState(false);
    const [errorOrganization, setErrorOrganization] = useState(false);
    const [errorEmail, setErrorEmail] = useState(false);
    const [errorPhone, setErrorPhone] = useState(false);
    const [errorStreet, setErrorStreet] = useState(false);
    const [errorCity, setErrorCity] = useState(false);
    const [errorState, setErrorState] = useState(false);
    const [errorCountry, setErrorCountry] = useState(false);
    // const [errorDescription, setErrorDescription] = useState(false);
    const [errorPopulation, setErrorPopulation] = useState(false);
    const [errorCapacitySewage, setErrorCapacitySewage] = useState(false);
    const [errorCapacityFecal, setErrorCapacityFecal] = useState(false);
    const [errorConclusion, setErrorConclusion] = useState(false);
    const [errorResultDetails, setErrorResultDetails] = useState(false);

    const [errorMessage, setErrorMessage] = useState([]);

    const graph = context;
    var report = graph.reportData;

    // ***************** Set default values for testing purposes ****************
    // graph.setReportValue('authorName', 'Panagis Katsivelis');
    // graph.setReportValue('organization', 'Venthic Technologies');
    // graph.setReportValue('email', 'panagis.katsivelis@venthic.com');
    // graph.setReportValue('city', 'Athens');
    // graph.setReportValue('country', 'Greece');
    // graph.setReportValue('name', 'Panagis\' Report');
    // graph.setReportValue('capacitySewage', '1000');
    // graph.setReportValue('capacityFecal', '1000');
    // graph.setReportValue('population', '1000000');
    // graph.setReportValue('resultDetails', 'Panagis approves');
    // graph.setReportValue('conclusion', 'This is the conclusion');

    const handleImage = (file) => {
        graph.setReportValue('photo', file);
        var reader = new FileReader();
        // reader.readAsDataURL( file );
        var baseString = '';
        reader.onloadend = function () {
            baseString = reader.result;
            report.photo = baseString;
        };
        reader.readAsDataURL(file);
    }

    const validate = (report) => {
        
            let flag = true;
            setErrorMessage([]);

            if(!report.authorName || report.authorName === ""){
                setErrorAuthorName(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the author is required!']);
                flag = false;
            } else{
                setErrorAuthorName(false)
            } 

            if(!report.organization || report.organization === ""){
                setErrorOrganization(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the organization is required!']);
                flag = false;
            } else setErrorOrganization(false);

            if(!report.email || report.email === ""){
                setErrorEmail(true);
                setErrorMessage(errorMessage => [...errorMessage,'The email address is required!']);
                flag = false;
            } else { 
                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(report.email)) {
                    setErrorEmail(false);
                }
                else {
                    setErrorEmail(true);
                    setErrorMessage(errorMessage => [...errorMessage,'The email address provided is not valid!']);
                    flag = false;
                }
                
            }

            // if(!report.phone || report.phone === ""){
            //     setErrorPhone(true);
            //     setErrorMessage(errorMessage => [...errorMessage, 'The phone number is required!']);
            //     flag = false;
            // } else setErrorPhone(false);

            // if(!report.street || report.street === ""){
            //     setErrorStreet(true);
            //     setErrorMessage(errorMessage => [...errorMessage,'The name of the street is required!']);
            //     flag = false;
            // } else setErrorStreet(false);

            if(!report.city || report.city === ""){
                setErrorCity(true);
                setErrorMessage(errorMessage => [...errorMessage,'The name of the city is required!']);
                flag = false;
            } else setErrorCity(false);

            // if(!report.state || report.state === ""){
            //     setErrorState(true);
            //     setErrorMessage(errorMessage => [...errorMessage, 'The name of the Administrative Division is required!']);
            //     flag = false;
            // } else setErrorState(false);

            if(!report.country || report.country === ""){
                setErrorCountry(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the country is required!']);
                flag = false;
            } else setErrorCountry(false);

            if(!report.name || report.name === ""){
                setErrorName(true);
                setErrorMessage(errorMessage => [...errorMessage,'The name of the treatment plant is required!']);
                flag = false;
            } else setErrorName(false);


            if(!report.resultDetails || report.resultDetails === ""){
                setErrorResultDetails(true);
                setErrorMessage(errorMessage => [...errorMessage, 'Your interpretation of the results is required!']);
                flag = false;
            } else setErrorResultDetails(false);

            if(!report.conclusion || report.conclusion === ""){
                setErrorConclusion(true);
                setErrorMessage(errorMessage => [...errorMessage,'A conclusion is required!']);
                flag = false;
            } else setErrorConclusion(false);

            if(!report.population === "" || report.population === 0){
                setErrorPopulation(true);
                setErrorMessage(errorMessage => [...errorMessage, 'Population served is required!']);
                flag = false;
            } else setErrorPopulation(false);

            if( report.capacitySewage === "" && report.capacityFecal=== "" ){
                setErrorCapacitySewage(true);
                setErrorCapacityFecal(true);
                setErrorMessage(errorMessage => [...errorMessage, 'Sewage or Fecal capacity (or both) has to be specified.']);
                flag = false;
            } else {
                setErrorCapacitySewage(false);
                setErrorCapacityFecal(false);
            }
            if (!flag) { setError(true) }
            return flag;
        
    }

    return (
        <div>
        <Dialog
            maxWidth='lg'
            fullWidth={true}
            id="screenshotDialog"
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={graph.generateReportModalState}
            onClose={(e) => {
                graph.setGenerateReportModalState(false);
            }}
        >
            <DialogTitle>
                <center>Generate Report</center>
            </DialogTitle>
            {!graph.generatePDFButton  ?
                <DialogContent style={{height: '20px', overflowY:'hidden'}}>
                    
                    {(graph.screenshotObject === 'bar') ?
                        <div>
                            <center>Printing LRV Diagram...</center>
                            <LRVforPDF style={{height: '300px'}} context={graph}/>
                        </div>: 
                            (graph.screenshotObject === 'flow') ? 
                                <div>
                                    <center>Printing Flow Diagrams...</center>
                                    
                                        {graph.calculationData.flowCharts.map((chart, i) => {
                                            return <div style={{visibility: 'hidden'}} className={'sankeyPlot-'+i} id="sankeyPlot"><SankeyChart graph={graph} json={graph.calculationData.flowCharts[i]} i={i}/></div>
                                        })}
                                        
                                    </div>:<center>Generating PDF File...</center>
                    }
                </DialogContent>
                :
                <DialogContent>
                    <Grid container spacing={1}>
                        {error &&
                            <Grid item xs={12} md={12}> 
                                <Alert severity="error">
                                    <ul>
                                    {
                                        errorMessage.map((message)=>{
                                            return <li>{message}</li> 
                                        })
                                    }
                                    </ul>
                                </Alert>
                                
                            </Grid>
                        }
                        </Grid>
                    <Grid container spacing={2} style={{marginBottom:'10px'}}>
                        <Grid item xs={12} sm={12}>
                            <Typography variant="h3" gutterBottom>
                                Author
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorAuthorName} defaultValue={report.authorName} required id="authorName" label="Name" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('authorName', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorOrganization} defaultValue={report.organization} required id="organization" label="Institution/Organization" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('organization', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorEmail} defaultValue={report.email} required id="email" label="Email" fullWidth type="email" variant='outlined'
                                onChange={(e) => {graph.setReportValue('email', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorPhone} defaultValue={report.phone} id="phone" label="Phone Number" fullWidth type="tel" variant='outlined'
                                onChange={(e) => {graph.setReportValue('phone', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorStreet} defaultValue={report.street} id="street" label="Address Line" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('street', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField  error={errorCity} defaultValue={report.city} required id="city" label="City" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('city', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField error={errorState} defaultValue={report.state} id="state" label="Administrative Division (state, department, province, etc.)" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('state', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorCountry} defaultValue={report.country} required id="country" label="Country" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('country', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Typography variant="h3" gutterBottom>
                                Treatment Plant
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <TextField error={errorName} style={{width: '320px'}} defaultValue={report.name} required id="name" label="Name" variant='outlined'
                                onChange={(e) => {graph.setReportValue('name', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}
                                helperText="e.g. ______ Treatment Plant"  />
                        </Grid>
                        {/* <Grid item xs={12} sm={12}>
                            <TextField error={errorDescription} defaultValue={report.description} required id="description" label="The treatment plant has the following unit processes: ..." fullWidth multiline rows="4" variant='outlined'
                                onChange={(e) => {graph.setReportValue('description', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid> */}
                        <Divider orientation="horizontal"/>
                        <Grid item xs={12} sm={6}>
                            <FormControl component="fieldset">
                            <RadioGroup aria-label="existing" name="existing system (sketched as is)" defaultValue={"an existing system"} onChange={(e) => {graph.setReportValue('existing', e.target.value);}}>
                                <FormControlLabel value="an existing system" control={<Radio />} label="Existing system (sketched as is)"/>
                                <FormControlLabel value="an existing system (with proposed changes)" control={<Radio />} label="Existing system (with proposed changes)" />
                                <FormControlLabel value="a proposed new system" control={<Radio />} label="Proposed new system" />
                            </RadioGroup>
                            </FormControl>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            
                            <FormControl component="fieldset"> 
                                <TextField size="small" style={{width: '320px'}} error={errorPopulation} required id="population" type='number' defaultValue={report.population} label="Equivalent population served" fullWidth variant='outlined'
                                    onChange={(e) => {graph.setReportValue('population', e.target.value);}} 
                                    onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  /><br/>
                                {(graph.graph.nodes.filter(node => node.type === SOURCE_TYPE && node.payload.subType=== 'sewerage').length > 0) && 
                                    <TextField size="small" style={{width: '360px', marginTop: '15px'}} error={errorCapacitySewage} id="capacity-sewage" defaultValue={report.capacitySewage} type='number' label="Maximum Capacity (sewage flow rate, m3/day)" fullWidth variant='outlined'
                                        onChange={(e) => {graph.setReportValue('capacitySewage', e.target.value);}} 
                                        onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                                }
                                <br/>
                                {(graph.graph.nodes.filter(node => node.type === SOURCE_TYPE && node.payload.subType=== 'fecal sludge').length > 0) &&
                                <TextField size="small" style={{width: '360px', marginTop: '15px'}} error={errorCapacityFecal} id="capacity-fecal" defaultValue={report.capacityFecal} type='number' label="Maximum Capacity (fecal sludge flow rate, m3/day)" fullWidth variant='outlined'
                                    onChange={(e) => {graph.setReportValue('capacityFecal', e.target.value);}} 
                                    onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                                }
                            </FormControl>
                        </Grid>
                        <Divider orientation="horizontal"/>
                            {/* <TextField error={errorConditions} required id="conditions" label="The treatment plant configuration represents..." fullWidth multiline rows="4" variant='outlined'
                                onChange={(e) => {report.conditions = e.target.value;}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  /> */}
                        {/* <Grid item xs={12} sm={6}>
                            <FormControl component="fieldset">
                            <RadioGroup aria-label="treats" name="treats" defaultValue={"sewage"} onChange={(e) => {graph.setReportValue('treats', e.target.value);}}>
                                <FormControlLabel value="sewage" control={<Radio />} label="Treats sewage"/>
                                <FormControlLabel value="fecal sludge" control={<Radio />} label="Treats fecal sludge" />
                                <FormControlLabel value="sewage and fecal sludge" control={<Radio />} label="Treats both sewage and fecal sludge" />
                            </RadioGroup>
                            </FormControl>
                        </Grid> */}
                        <Grid item xs={12} sm={6}>
                            <TextField id="systemDetails" defaultValue={report.systemDetails} label="Additional details about the treatment plant" fullWidth multiline rows="4" variant='outlined'
                                onChange={(e) => {graph.setReportValue('systemDetails', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Divider orientation="horizontal"/>
                        
                        <Grid item xs={12} sm={6}>
                        <div style={{height:'120px', display: 'block', backgroundColor:'#ddd', borderRadius:'5px', textAlign:'center', padding:'20px'}}>
                                <label htmlFor="raised-button-file" style={{fontWeight: 700}}>Upload a photograph of the treatment system</label> 
                                <input 
                                    accept="image/*" 
                                    id="raised-button-file" 
                                    multiple 
                                    type="file" 
                                    style={{marginTop: '20px'}}
                                    onChange={(e)=>{handleImage(e.target.files[0]); if (report.photoCredit === '') { graph.setReportValue('photoCredit', report.authorName);}}}
                                    /> <br/>
                                    <TextField size="small" style={{width: '320px', marginTop: '20px'}} id="photoCredit" defaultValue={report.photoCredit} label="Photo Credit" fullWidth variant='outlined'
                                    onChange={(e) => {graph.setReportValue('photoCredit', e.target.value);}}></TextField>
                            </div>
                            
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <TextField required error={errorResultDetails} defaultValue={report.resultDetails} id="resultDetails" label='Based on the results of the Sketcher Tool analysis, the system...' fullWidth multiline rows="4" variant='outlined'
                                onChange={(e) => {graph.setReportValue('resultDetails', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  
                                helperText="Finish the sentence to describe your interpretation of the results of the Sketcher tool analysis"
                                />
                        </Grid>
                        <Divider orientation="horizontal"/>
                        <Grid item xs={12} sm={12}>
                            <TextField required id="conclusion" defaultValue={report.conclusion} error={errorConclusion} label="In conclusion, ..." fullWidth multiline rows="4" variant='outlined'
                                onChange={(e) => {graph.setReportValue('conclusion', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  
                                helperText="Finish this sentence, which will become the final paragraph of your report"
                                />
                        </Grid> 
                    </Grid>
                    {(report.existing.includes('existing')) && <Grid item xs={12}>
                        <h4>For existing systems only:</h4>
                        
                        <form style={{marginBottom:'15px'}}>
                        <TextField variant='outlined' defaultValue={report.bacteriaName}
                            helperText='I have my own lab data on the measured % removal of E. coli, coliforms, or a bacteria indicator'
                             onChange={(e) => {graph.setReportValue('bacteriaName', e.target.value);}} onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}} size="small" id="bacteriaName" label="Name of bacteria"/>&nbsp;

                        <TextField 
                            size="small" 
                            id="bacteriaRemoval" 
                            type='number' 
                            label="Average % removal" 
                            variant="outlined"
                            defaultValue={report.bacteriaRemoval}
                            onChange={(e) => {graph.setReportValue('bacteriaRemoval', e.target.value);}} 
                            onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  
                            />
                            
                        </form>
                        <form style={{marginBottom: '15px'}}>
                            <TextField variant='outlined'  defaultValue={report.virusName} size="small" onChange={(e) => {graph.setReportValue('virusName', e.target.value);}} onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}} id="virusName" label="Name of virus" helperText='I have my own lab data on the measured % removal of a virus' />&nbsp;
                            <TextField type='number' defaultValue={report.virusRemoval} variant='outlined' size="small" onChange={(e) => {graph.setReportValue('virusRemoval', e.target.value);}} onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}} id="virusRemoval" label="Average % removal" />
                        </form>
                        <form style={{marginBottom: '15px'}}>
                            <TextField variant='outlined'  defaultValue={report.protozoaName} size="small" onChange={(e) => {graph.setReportValue('protozoaName', e.target.value);}} onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}} id="protozoaName" label="Name of protozoa" helperText='I have my own lab data on the measured % removal of a protozoa' />&nbsp;
                            <TextField type='number' defaultValue={report.protozoaRemoval} variant='outlined' size="small" onChange={(e) => {graph.setReportValue('protozoaRemoval', e.target.value);}} onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}} id="protozoaRemoval" label="Average % removal" />
                        </form>
                        <form style={{marginBottom: '15px'}}>
                            <TextField variant='outlined' size="small"  defaultValue={report.helminthName} onChange={(e) => {graph.setReportValue('helminthName', e.target.value);}} onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}} id="helminthName" label="Name of helminth" helperText='I have my own lab data on the measured % removal of a helminth' />&nbsp;
                            <TextField type='number' defaultValue={report.helminthRemoval} variant='outlined' onChange={(e) => {graph.setReportValue('helminthRemoval', e.target.value);}} size="small" id="helminthRemoval" label="Average % removal" />
                        </form>
                    </Grid>
                    }
                </DialogContent>
            }
            <DialogActions>
                <Button onClick={() => {graph.setGenerateReportModalState(false);}} color="primary">Cancel</Button>
                {graph.generatePDFButton ?
                    <Button onClick={() => { 
                        if (validate(report)) { 
                            graph.takeScreenshotForReport(report)
                        }
                    }
                } color="primary" autoFocus >Generate PDF</Button>:
                    <Button color="primary" autoFocus disabled >Generate PDF</Button>
                }
            </DialogActions>
        </Dialog>
        </div>

    )
});

export default GenerateReportModal;