import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Grid from "@material-ui/core/Grid";
import MenuBookTwoToneIcon from '@material-ui/icons/MenuBookTwoTone';
import PlayCircleFilledTwoToneIcon from '@material-ui/icons/PlayCircleFilledTwoTone';
import Tutorial from "./Tutorial"
import GraphConfig, {
    NODE_KEY,
  } from '../config/graph-config';
import { withStyles } from "@material-ui/core/styles";

  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const InfoModal = observer(({context}) => {
    const graph = context;
    const [showTutorial, setShowTutorial] = useState(false);

    return (
        <Dialog
            maxWidth='xl'
            fullWidth={showTutorial}
            open={graph.infoState}
            onClose={(e) => {
                graph.setInfoState(false);
                setShowTutorial(false);
            }}
        >
            <DialogTitle>
                <center>Welcome to the Treatment Plant Sketcher Tool<LightTooltip title='The beta version of the Treatment Plant Sketcher Tool has been tested with latest versions of Google Chrome (80+) and Mozilla Firefox (74+) browsers.'><sup style={{ textTransform: 'lowercase',
    fontSize: '10px',
    color: '#ea7b7b'}}>beta</sup></LightTooltip></center>
            </DialogTitle>     
            <DialogContent>
                 {!showTutorial?
                    <div className="helpOptions"> 
                    
                    <a className="tourButton" onClick={() => {
                        graph.setRun(true); graph.setInfoState(false);
                        graph.setSelectedNode(graph.graph.nodes.filter(node => node[NODE_KEY] == '2')[0])
                        if (graph.GraphView) {
                            graph.GraphView.panToNode('2', true);
                        }
                        }}>
                            <PlayCircleFilledTwoToneIcon/>
                        <span>Take Tour</span>
                    </a>
                    <a className="manualButton" onClick={() => setShowTutorial(true)}>
                        <MenuBookTwoToneIcon/>
                        <span>View Manual</span>

                    </a></div>:
                    <Tutorial />
                }
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {
                    graph.setInfoState(false);
                    setShowTutorial(false);
                }} color="secondary">
                    Skip
                    </Button>
                
            </DialogActions>
        </Dialog>
    )
});

export default InfoModal;
