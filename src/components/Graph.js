// @flow
/*
  Copyright(c) 2018 Uber Technologies, Inc.
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at
          https://www.apache.org/licenses/LICENSE-2.0
  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

/*
  Example usage of GraphView component
*/

import * as React from 'react';
import Canvg, { presets } from "canvg";
import defaultModel from "../models/SketchConstsModel";
import SketchSideBar from "./SketchSideBar";
import NodeDial from "./NodeDial";
import InfoModal from "./InfoModal";
import GenerateReportModal from "./GenerateReportModal";
import ScreenshotModal from "./ScreenshotModal";
import SankeyModal from "./SankeyModal";
import Snackbar from '@material-ui/core/Snackbar';
import Grid from '@material-ui/core/Grid';
import MuiAlert from '@material-ui/lab/Alert';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import SendMappingCatalog from "./SendMappingCatalog";
import ImportModal from "./ImportModal";
import fileDownload from 'js-file-download';
import { observable, action, reaction, computed } from 'mobx';
import { observer } from "mobx-react";
import { ThemeProvider, withStyles } from "@material-ui/core/styles";
import theme from "../config/Theme.js";
import Fab from '@material-ui/core/Fab';
import PlayArrowTwoToneIcon from '@material-ui/icons/PlayArrowTwoTone';
import queryString from "query-string";
import Tooltip from "@material-ui/core/Tooltip";
import Joyride, {STATUS } from 'react-joyride';
import fetchJsonp from 'fetch-jsonp';
import {
  GraphView,
  type IEdgeType as IEdge,
  type INodeType as INode,
  type LayoutEngineType,
} from 'react-digraph';
import GraphConfig, {
  edgeTypes,
  EMPTY_EDGE_TYPE,
  UNIT_PROCESS_TYPE,
  NODE_KEY,
  nodeTypes,
  SOURCE_TYPE,
  END_USE_TYPE,
} from '../config/graph-config'; // Configures node/edge types
import SketchNodeModel from '../models/SketchNodeModel';
import HeaderBar from './HeaderBar';
import ReportPDF from "./ReportPDF";
import {ReactSVG} from 'react-svg';

type IGraph = {
  nodes: INode[],
  edges: IEdge[],
};


const sample: IGraph = defaultModel;

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
    fontSize: 14,
    fontWeight: 500
  },
}))(Tooltip);

type IGraphProps = {};

type IGraphState = {
  graph: any,
  selected: any,
  totalNodes: number,
  copiedNode: any,
  layoutEngineType?: LayoutEngineType,
};

const steps= [
  {
    content: <p>Welcome to the Treatment Plant Sketcher tool. For a quick overview 
      on how to use the tool, click “Next” to skip the tutorial click “skip”</p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    placement: 'center',
    target: 'body',
  },
  {
    content: <p>Sketch a diagram of your wastewater treatment system 
      using the icons on the left of the screen </p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    floaterProps: {
      disableAnimation: true,
    },
    spotlightPadding: 20,
    target: '#nodeDial',
  },
  {
    content: <p> To speed up the sketching process you can also import a 
      pre-existing sketch by clicking on the “Import” box at the top of 
      the Screen” </p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    floaterProps: {
      disableAnimation: true,
    },
    spotlightPadding: 20,
    target: '#importButton',
  },
  {
    content: <p> For each source, unit process and end use,  
      provide the information in the pop-up window on the right.</p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    floaterProps: {
      disableAnimation: true,
    },
    spotlightPadding: 20,
    disableOverlay: true,
    target: '#sidebarContent',
  },
  {
    content: <p> Once you have finalized the sketch and included all the 
      information in the pop-up window on the right. Click on the calculate 
      button on the top left of the screen to visualize the pathogen reduction 
      predicted by the system</p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    floaterProps: {
      disableAnimation: true,
    },
    spotlightPadding: 20,
    target: '#calculateButton',
  },
  {
    content: <p> Save an image of your sketch using the “camera icon” 
      and use the “save” button to save a copy of your sketch and input 
      data so that you can import it again later.</p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    floaterProps: {
      disableAnimation: true,
    },
    spotlightPadding: 20,
    target: '#imageButton',
  },
  // {
  //   content: <p> Print a pdf report of your results. Publish your results 
  //     to the K2P database</p>,
  //   locale: { skip: <strong aria-label="skip">SKIP</strong> },
  //   floaterProps: {
  //     disableAnimation: true,
  //   },
  //   spotlightPadding: 20,
  //   target: '#publishButton',
  // },
  {
    content: <p> You are now ready to start using the Treatment Plant Sketcher Tool!</p>,
    locale: { skip: <strong aria-label="skip">SKIP</strong> },
    placement: 'center',
    target: 'body'
  },
];

@observer
class Graph extends React.Component<IGraphProps, IGraphState> {
  GraphView;
  @observable copiedNode;
  @observable graph;
  @observable pannedNode;
  @observable layoutEngineType;
  @observable selected;
  @observable totalNodes;
  @observable validationMsg;
  @observable sendToCatalogueModal;
  @observable infoState;
  @observable importModalState;
  @observable sankeyModalState;
  @observable screenshotModalState;
  @observable generateReportModalState;
  @observable generatePDFButton;
  @observable fileReader;
  @observable title;
  @observable description;
  @observable keywords;
  @observable extraNodes;
  @observable influent;
  @observable effluent;
  @observable uploadResponse;
  @observable importStatus;
  @observable dataUrl;
  @observable isCalculating;
  @observable calculationComplete;
  @observable calculationData;
  @observable pathogenType;
  @observable catalogueSketches;
  @observable reportData;
  @observable screenshotObject;
  @observable barRef;
  @observable flowRefs;
  @observable run;

  constructor(props: IGraphProps) {
    super(props);

    this.copiedNode = null;
    this.graph = sample;
    this.layoutEngineType = undefined;
    this.selected = null;
    this.fileReader = null;
    this.validationMsg = [];
    this.sendToCatalogueModal = false;
    this.importModalState = false;
    this.sankeyModalState = false;
    this.generateReportModalState =false;
    this.generatePDFButton = true;
    this.screenshotModalState = false;
    this.extraNodes = false;
    this.title = '';
    this.description = '';
    this.keywords = [];
    this.influent = '';
    this.effluent = '';
    this.importStatus = false;
    this.pannedNode = '';
    this.infoState = true;
    this.dataUrl = '';
    this.isCalculating = false;
    this.calculationComplete = false; 
    this.pathogenType = 0;
    this.calculationData = [];
    this.catalogueSketches = [];
    this.GraphView = React.createRef();
    this.reportData = {
      authorName:'',
      organization: '',
      street: '',
      city: '',
      state: '',
      street: '',
      country: '',
      phone: '',
      email: '',
      name: '',
      photoCredit: '',
      population: '',
      capacityFecal: '',
      capacitySewage: '',
      existing: 'an existing system',
      // treats: 'sewage',
      systemDetails: '',
      photo: '',
      resultDetails: '',
      conclusion: '',
      bacteriaName: '',
      bacteriaRemoval: '',
      virusName: '',
      virusRemoval: '',
      protozoaName: '',
      protozoaRemoval: '',
      helminthName: '',
      helminthRemoval: ''
    };
    this.screenshotObject = '';
    this.barRef = React.createRef();
    this.flowRefs = [
      React.createRef(),
      React.createRef(),
      React.createRef(),
      React.createRef()
    ]
    this.run= false;
    
  }

  @action
  machineToHuman(type) {
    switch (type) {
      case SOURCE_TYPE:
        return 'source';
      case UNIT_PROCESS_TYPE:
        return 'unit process'
      case END_USE_TYPE:
        return 'end use'
      case 'source':
        return SOURCE_TYPE;
      case 'unit process':
        return UNIT_PROCESS_TYPE;
      case 'end use':
        return END_USE_TYPE;
      default:
        return 'other'
    }
  }

  @action
  setSelectedNode(node) {
    this.selected = node;
  }

  @action
  setCopiedNode(node, x, y) {
    this.copiedNode = { ...this.selected, x, y };
    // this.copiedNode.payload.setProp("number", this.currentNumber);
    // this.copiedNode.title = (this.currentNumber+1)+"";
    // this.copiedNode.payload.setProp("children",[]);
  }

  @action
  setGraph(graph) {
    this.graph = graph;
  }

  @action
  setInfoState(status) {
    this.infoState = status;
  }

  @action
  setGenerateReportModalState(status) {
    this.generateReportModalState = status;
  }

  @action
  setGeneratePDFButton(status) {
    this.generatePDFButton = status;
  }

  @action
  setReportValue(field, value) {
    this.reportData[field] = value;
  }

  @action
  setImportStatus(status) {
    this.importStatus = status;
  }

  @action
  setCatalogueSketches(sketches) {
    this.catalogueSketches = sketches;
  }

  @action
  fetchCatalogueSketches() {
    fetch("https://data.waterpathogens.org/api/3/action/group_package_show?id=sketcher-templates")
            .then(res => res.json())
            .then(
            (res) => {
                this.setCatalogueSketches(res.result);
            },
            (error) => {
                this.pushMessage("Couldn't fetch sketches from the K2P database. Please check again later.");
                console.log(error);
            });
  }

  @action
  setRun(status) {
    this.run = status;
  }

  @computed
  get currentNumber() {
    return this.graph.nodes.length;
  }

  @action
  availableConnections(sourceNode: INode) {
    if (sourceNode.type != END_USE_TYPE) {
      return this.graph.nodes.filter(node => node.type != SOURCE_TYPE).filter(node => node.id != sourceNode.id);
    }
    return [];
  }

  @action
  canConnect(source: INode, target: INode) {
    return this.availableConnections(source).includes(target);
  }

  // Helper to find the index of a given node
  @action
  getNodeIndex(searchNode: INode | any) {
    return this.graph.nodes.findIndex(node => {
      return node[NODE_KEY] === searchNode[NODE_KEY];
    });
  }

  @action
  getNode(id) {
    const node = this.graph.nodes.filter(node => {
      return node[NODE_KEY] === id;
    })[0];
    return node;
  }

  // Helper to find the index of a given edge
  @action
  getEdgeIndex(searchEdge: IEdge) {
    return this.graph.edges.findIndex(edge => {
      return (
        edge.source === searchEdge.source && edge.target === searchEdge.target
      );
    });
  }

  @action
  setReportDataValue(field, value) {
    this.reportData[field] = value;
  }

  // Given a nodeKey, return the corresponding node
  @action
  getViewNode(nodeKey: string) {
    const searchNode = {};

    searchNode[NODE_KEY] = nodeKey;
    const i = this.getNodeIndex(searchNode);

    return this.graph.nodes[i];
  }

  @action
  addNode = (type) => {
    
    const graph = this.graph;

    const nodePayload = new SketchNodeModel(this.currentNumber);

    // using a new array like this creates a new memory reference
    // this will force a re-render
    graph.nodes = [
      {
        id: Date.now(),
        title: this.currentNumber + 1,
        type: type,
        payload: nodePayload,
        x: 0,
        y: 0,
      },
      ...this.graph.nodes,
    ];
    this.setGraph(graph);
  };

  @action
  deleteSelectedNode = () => {
    var graph = this.graph;
    var index = this.getNodeIndex(this.selected);
    const newNodes = graph.nodes.filter((node, i) => i != index);

    for (var i in newNodes) {
      const spot = newNodes[i].payload.children.indexOf(this.selected[NODE_KEY]);
      if (spot > -1) {
        newNodes[i].payload.children.splice(spot, 1);
      }
    }
    // using a new array like this creates a new memory reference
    // this will force a re-render

    const newEdges = graph.edges.filter((edge, i) => {
      return (
        edge.source !== this.selected[NODE_KEY] && edge.target !== this.selected[NODE_KEY]
      );
    });

    graph.edges = newEdges;
    graph.nodes = newNodes;

    //graph.nodes = [...this.graph.nodes];

    this.setGraph(graph);
    this.refreshNodes();
    
    this.setSelectedNode(this.graph.nodes[this.graph.nodes.length-1]);
  };

  refreshNodes = () => {
    var graph = this.graph;
    var newNodes = graph.nodes;
    for (var i=0; i<newNodes.length; i++) {
      newNodes[i].payload.setProp("number", (i+1)+"");
      newNodes[i].payload.setProp("name", (i+1)+"");
      newNodes[i].title = (i+1)+"";
      this.setSelectedNode(this.graph.nodes[this.graph.nodes.length-1]);
    }
    graph.nodes = newNodes;

    this.setGraph(graph);
  }

  handleChange = (event: any) => {
    this.setState(
      {
        totalNodes: parseInt(event.target.value || '0', 10),
      },
    );
  };

  /*
   * Handlers/Interaction
   */

  // Called by 'drag' handler, etc..
  // to sync updates from D3 with the graph
  @action
  onUpdateNode = (viewNode: INode) => {
    const graph = this.graph;
    const i = this.getNodeIndex(viewNode);

    graph.nodes[i] = viewNode;

    this.setGraph(graph);
  };

  // Node 'mouseUp' handler
  onSelectNode = (viewNode: INode | null) => {
    // Deselect events will send Null viewNode
    this.setSelectedNode(viewNode);
  };

  // Edge 'mouseUp' handler
  onSelectEdge = (viewEdge: IEdge) => {
    this.setState({ selected: viewEdge });
  };

  // Updates the graph with a new node
  onCreateNode = (x: number, y: number) => {

    const graph = this.graph;

    // This is just an example - any sort of logic
    // could be used here to determine node type
    // There is also support for subtypes. (see 'sample' above)
    // The subtype geometry will underlay the 'type' geometry for a node
    const type = Math.random() < 0.25 ? SOURCE_TYPE : UNIT_PROCESS_TYPE;
    const viewNode = {
      id: Date.now(),
      title: '',
      type,
      x,
      y,
    };

    graph.nodes = [...graph.nodes, viewNode];
    this.setGraph(graph);
  };

  // Deletes a node from the graph
  @action
  onDeleteNode = (viewNode: INode, nodeId: string, nodeArr: INode[]) => {
    const graph = this.graph;
    // Delete any connected edges
    var index = this.getNodeIndex(viewNode);
    const newNodes = graph.nodes.filter((node, i) => i != index);

    for (var i in newNodes) {
      const spot = newNodes[i].payload.children.indexOf(nodeId);
      if (spot > -1) {
        newNodes[i].payload.children.splice(spot, 1);
      }
    }
    // using a new array like this creates a new memory reference
    // this will force a re-render

    const newEdges = graph.edges.filter((edge, i) => {
      return (
        edge.source !== nodeId && edge.target !== nodeId
      );
    });

    graph.edges = newEdges;
    graph.nodes = newNodes;

    //graph.nodes = [...this.graph.nodes];

    this.setGraph(graph);
    this.refreshNodes();
    
    
    this.forceUpdate();
    this.setSelectedNode(this.graph.nodes[this.graph.nodes.length-1]);
  };

  // Creates a new node between two edges
  @action
  onCreateEdge = (sourceViewNode: INode, targetViewNode: INode) => {
    const graph = this.graph;

    // This is just an example - any sort of logic
    // could be used here to determine edge type
    const type =
      sourceViewNode.type === EMPTY_EDGE_TYPE;

    const viewEdge = {
      source: sourceViewNode[NODE_KEY],
      target: targetViewNode[NODE_KEY],
      type,
    };

    // Only add the edge when the source node is not the same as the target
    if (this.canConnect(sourceViewNode, targetViewNode)) {
      graph.edges = [...graph.edges, viewEdge];
      this.setGraph(graph);
      sourceViewNode.payload.addChild(targetViewNode[NODE_KEY]);
      this.setSelectedNode(sourceViewNode);

      // this.setState({
      //   graph,
      //   selected: viewEdge,
      // });
    }
  };

  // Called when an edge is reattached to a different target.
  onSwapEdge = (
    sourceViewNode: INode,
    targetViewNode: INode,
    viewEdge: IEdge
  ) => {
    const graph = this.graph;
    const i = this.getEdgeIndex(viewEdge);
    const edge = JSON.parse(JSON.stringify(graph.edges[i]));

    edge.source = sourceViewNode[NODE_KEY];
    edge.target = targetViewNode[NODE_KEY];
    graph.edges[i] = edge;
    // reassign the array reference if you want the graph to re-render a swapped edge
    graph.edges = [...graph.edges];

    this.setState({
      graph,
      selected: edge,
    });
  };

  // Called when an edge is deleted
  onDeleteEdge = (viewEdge: IEdge, edges: IEdge[]) => {
    const graph = this.graph;

    graph.edges = edges;
    this.setState({
      graph,
      selected: null,
    });
  };

  onUndo = () => {
    // Not implemented
    console.warn('Undo is not currently implemented in the example.');
    // Normally any add, remove, or update would record the action in an array.
    // In order to undo it one would simply call the inverse of the action performed. For instance, if someone
    // called onDeleteEdge with (viewEdge, i, edges) then an undelete would be a splicing the original viewEdge
    // into the edges array at position i.
  };

  @action
  copyNode = () => {
    const graph = this.graph;
    const x = this.selected.x + 10;
    const y = this.selected.y + 10;

    this.setCopiedNode(this.selected, x, y);
    const newPayload = this.copiedNode.payload.cloneSelf();


    const newNode = { ...this.copiedNode, id: Date.now(), title: (graph.nodes.length + 1) };
    newNode.payload = newPayload;
    graph.nodes = [...graph.nodes, newNode];
    this.setGraph(graph);
    this.forceUpdate();
  }


  onCopySelected = () => {
    if (this.selected.source) {
      console.warn('Cannot copy selected edges, try selecting a node instead.');

      return;
    }

    const x = this.selected.x + 10;
    const y = this.selected.y + 10;

    this.setCopiedNode(this.selected, x, y);
    // this.setState({
    //   copiedNode: { ...this.selected, x, y },
    // });
  };

  @action
  onPasteSelected = () => {
    if (!this.copiedNode) {
      console.warn(
        'No node is currently in the copy queue. Try selecting a node and copying it with Ctrl/Command-C'
      );
    }

    const graph = this.graph;
    const newPayload = this.copiedNode.payload.cloneSelf();
    const newNode = { ...this.copiedNode, id: Date.now(), title: (graph.nodes.length + 1) };

    newNode.payload = newPayload;
    graph.nodes = [...graph.nodes, newNode];
    this.setGraph(graph);
    this.forceUpdate();
  };

  handleChangeLayoutEngineType = (event: any) => {
    this.setState({
      layoutEngineType: (event.target.value: LayoutEngineType | 'None'),
    });
};

@action
onSelectPanNode = (event: any) => {
  this.setSelectedNode(this.graph.nodes.filter(node => node[NODE_KEY] == event.target.value)[0])
  if (this.GraphView) {
    this.GraphView.panToNode(event.target.value, true);
  }
};

@action
pushMessage = (msg) => {
  this.validationMsg.push(msg);
}

@action
isValid = () => {
  const graph = this.graph;
  this.validationMsg = [];
  for (var i in graph.nodes) {

    var valid = true;
    if (graph.nodes[i].type == SOURCE_TYPE) {
      if (graph.nodes[i].payload.children.length == 0) {
        valid = false;
        this.pushMessage('Source ' + graph.nodes[i].title + ' is not connected to other nodes.');
      }
    }
    else if (graph.nodes[i].type == UNIT_PROCESS_TYPE) {
      var incomingNodes = false;
      var outgoingNodes = false;
      for (var j in graph.nodes) {
        if (graph.nodes[j].payload.children.includes(graph.nodes[i][NODE_KEY])) {
          incomingNodes = true;

        }

      }
      if (graph.nodes[i].payload.children.length > 0) {
        outgoingNodes = true;
      }
      valid = incomingNodes & outgoingNodes;
      if (!valid) {
        this.pushMessage('Unit Process ' + graph.nodes[i].title + ' requires both ingoing and outgoing connections.');
      }
    }
    else {
      valid = false;
      for (var j in graph.nodes) {
        if (graph.nodes[j].payload.children.includes(graph.nodes[i][NODE_KEY])) {
          valid = true;
        }
      }
      if (!valid) {
        this.pushMessage('End use ' + graph.nodes[i].title + ' is not connected to other nodes.');
      }
    }
  }
  if (this.extraNodes && (this.influent == '' || this.effluent == '')) {
    this.pushMessage('Please also fill-in the options for influent/effluent nodes.')
  }
  return (!this.validationMsg.length > 0);
}

@action
isComplete = () => {
  const graph = this.graph;
  this.validationMsg = [];
  for (var i in graph.nodes) {
    if (!graph.nodes[i].payload.complete) {
      this.pushMessage('Node ' + graph.nodes[i].title + ' is not fully specified.');
    }
    
  }
  return (!this.validationMsg.length > 0);
}

@action
takeScreenshot = (event: any) => {
  var graph = this;
  this.GraphView.handleZoomToFit();
  this.canvasToUrl('.graph', 2000, 1000).then((url) => {
    // console.log(url);
    this.setDataUrl(url);
    this.setScreenshotModalState(true);
  })


  // /alert(dt);
};

@action setScreenshotObject(obj) {
  this.screenshotObject = obj;
}

@action setFlowRefs(i, e) {
  this.flowRefs[i] = e;
}

@action setBarRef(e) {
  this.barRef = e;
}

@action
printFlowCharts(report, url, nodes, urlBars, table, i) {
  console.log(i);
  var flowUrls = [];
  setTimeout(
    () => {
    this.setPathogenType(i);
      this.canvasToUrl('#sankeyPlot', 2000, 1000).then((sankey) => {
        flowUrls[i] = sankey;
        if (i === 3) {
          this.print(report, url, nodes, urlBars, flowUrls, table );
        }
      })}, 2000);
}

@action
takeScreenshotForReport = (report) => {  
  var graph = this;
  this.setGeneratePDFButton(false);
  this.GraphView.handleZoomToFit();
  this.canvasToUrl('.graph', 2000,1000).then((url) => {
    this.setScreenshotObject('bar');
    //this.canvasToUrl('#bars', 1000, 500).then((urlBars) =>{
      //this.print(report, url, this.graph.nodes, urlBars, this.calculationData.table );})
      var urlBars = '';
      setTimeout(
        () => {
          
          let barInstance = this.barRef.getEchartsInstance();
          urlBars = barInstance.getDataURL({
            pixelRatio: 2,
            backgroundColor: '#fff'});
          
          this.setScreenshotObject('flow');
          var flowUrls = [];
          setTimeout(
            () => {
              flowUrls = this.flowRefs.map((ref) => {
              let instance = ref.getEchartsInstance();
              return instance.getDataURL({
                pixelRatio: 2,
                backgroundColor: '#fff'});
              });
              this.setScreenshotObject('');
              this.print(report, url, this.graph.nodes, urlBars, flowUrls, this.calculationData.table);
          
            }, 2000);

        }, 2000);

      
    
    
      
  
  //})
})
  
  // this.canvasToUrl('.graph', 2000, 1000).then((url) => {
  //   this.barsToUrl().then((urlBars) =>{
  //     this.print(report, url, this.graph.nodes, urlBars, this.calculationData.table, this.calculationData.flowCharts );})
  // })
}

print (report, imageUrl, nodes, urlBars, flowUrls, predictedTable) {
  ReportPDF(report,imageUrl, nodes, urlBars, flowUrls, predictedTable, () => {this.setGeneratePDFButton(true)});
};


canvasToUrl(id, x, y) {
  return new Promise((resolve, reject) => {
    setTimeout(
      () => {
        const canvas = document.querySelector('canvas');
        const ctx = canvas.getContext('2d');
        var element = document.querySelector(id);
        console.log(element.width);
        var width = (element.width) ? element.width.baseVal.value : 200;
        var height = (element.height) ? element.height.baseVal.value : 250;
        var svgHTML = document.querySelector(id).innerHTML + '';

        svgHTML = svgHTML.replace("<rect class=\"background\" x=\"-10240\" y=\"-10240\" width=\"40960\" height=\"40960\" fill=\"url(#grid)\"></rect>", "");
        //console.log('<svg xmlns=\"https://www.w3.org/2000/svg\" xmlns:xlink=\"https://www.w3.org/1999/xlink\"><style type=\"text/css\"><![CDATA[.edge {stroke: #003347; stroke-width:2px} .arrow {fill:blue; fill-opacity:1} use.node {stroke: #000}]]></style>'+svgHTML+'</svg>');
        var v = Canvg.fromString(ctx, '<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><style type=\"text/css\"><![CDATA[.edge {stroke: #003347; stroke-width:2px} .arrow {fill:blue; fill-opacity:1} use.node {stroke: #000}]]></style>' + svgHTML + '</svg>');

        v.resize(width, height, '');
        v.render();
        resolve(canvas.toDataURL('image/png'));
      },
      2000
    )
  })
}

barsToUrl() {
  return new Promise((resolve, reject) => {
    setTimeout(
      () => {
        const canvas = document.querySelector('canvas');
        const ctx = canvas.getContext('2d');
        var svgHTML = document.querySelector('#bars').innerHTML + '';

        svgHTML = svgHTML.replace("<rect class=\"background\" x=\"-10240\" y=\"-10240\" width=\"40960\" height=\"40960\" fill=\"url(#grid)\"></rect>", "");
        //console.log('<svg xmlns=\"https://www.w3.org/2000/svg\" xmlns:xlink=\"https://www.w3.org/1999/xlink\"><style type=\"text/css\"><![CDATA[.edge {stroke: #003347; stroke-width:2px} .arrow {fill:blue; fill-opacity:1} use.node {stroke: #000}]]></style>'+svgHTML+'</svg>');
        var v = Canvg.fromString(ctx, '<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><style type=\"text/css\"><![CDATA[.edge {stroke: #003347; stroke-width:2px} .arrow {fill:blue; fill-opacity:1} use.node {stroke: #000}]]></style>' + svgHTML + '</svg>');

        v.resize(1000, 500, '');
        v.render();
        resolve(canvas.toDataURL('image/png'));
      },
      2000
    )
  })

}


flowToUrl() {
  return new Promise((resolve, reject) => {
    setTimeout(
      () => {
        const canvas = document.querySelector('canvas');
        const ctx = canvas.getContext('2d');
        var svgHTML = document.querySelector('#sankeyPlot').innerHTML + '';

        svgHTML = svgHTML.replace("<rect class=\"background\" x=\"-10240\" y=\"-10240\" width=\"40960\" height=\"40960\" fill=\"url(#grid)\"></rect>", "");
        //console.log('<svg xmlns=\"https://www.w3.org/2000/svg\" xmlns:xlink=\"https://www.w3.org/1999/xlink\"><style type=\"text/css\"><![CDATA[.edge {stroke: #003347; stroke-width:2px} .arrow {fill:blue; fill-opacity:1} use.node {stroke: #000}]]></style>'+svgHTML+'</svg>');
        var v = Canvg.fromString(ctx, '<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\"><style type=\"text/css\"><![CDATA[.edge {stroke: #003347; stroke-width:2px} .arrow {fill:blue; fill-opacity:1} use.node {stroke: #000}]]></style>' + svgHTML + '</svg>');

        v.resize(1000, 500, '');
        v.render();
        resolve(canvas.toDataURL('image/png'));
      },
      2000
    )
  })

}

@action
setTitle = (event: any) => {
  const graph = this.graph;
  const index = graph.nodes.indexOf(this.selected);
  this.selected.title = (parseInt(index) + 1);
}

@action editTitle(value){
  this.title = value;
}

@action editDescription(desc){
  this.description = desc;
}

// @action editAuthor(author){
//   this.author = author;
// }

// @action editEmail(email) {
//   this.email = email;
// }

@action
setKeywords = (keywords) => {
  this.keywords = keywords
}

@action
setUploadResponse = (uploadResponse) => {
  this.uploadResponse = uploadResponse
}

@action setDataUrl(url){
  this.dataUrl = url;
}

@action toggleExtraNodes(){
  this.extraNodes = !this.extraNodes;
}

@action setCatalogueModalState(status){
  this.sendToCatalogueModal = status;
}

@action setImportModalState(status){
  this.importModalState = status;
}

@action setSankeyModalState(state){
  this.sankeyModalState = state;
}

@action setPathogenType(type){
  this.pathogenType = type;
}

@action setScreenshotModalState(status){
  this.screenshotModalState = status;
}

@action
setInfluent = (event: any) => {
  const graph = this.graph;
  const node = this.graph.nodes.filter(node => node[NODE_KEY] == event.target.value)[0];
  this.influent = (node) ? node[NODE_KEY] : '';
}

@action
setEffluent = (event: any) => {
  const graph = this.graph;
  const node = this.graph.nodes.filter(node => node[NODE_KEY] == event.target.value)[0];
  this.effluent = (node) ? node[NODE_KEY] : '';
}

@action
connectionEdit = (event: any) => {

  const graph = this.graph;
  const sourceNode = this.selected[NODE_KEY];
  this.selected.payload.removeChildren();
  const newEdges = graph.edges.filter(function (edge) {
    return edge.source != sourceNode;
  })
  graph.edges = newEdges;
  for (var i in event.target.value) {
    graph.edges.push({ source: sourceNode, target: event.target.value[i], type: EMPTY_EDGE_TYPE });
    this.selected.payload.addChild(event.target.value[i]);
  }
}

@action
createEdge = (id: any) => {
  const graph = this.graph;
  var sourceNode = this.selected[NODE_KEY];
  var targetNode = id;
  // if (Array.isArray(event.target.value)) {
  //   targetNode = event.target.value[0];
  // }
  // else {
  //   targetNode = event.target.value;
  // }
  const newEdge = { source: sourceNode, target: targetNode, type: EMPTY_EDGE_TYPE };

  graph.edges = [...graph.edges, newEdge];
  this.selected.payload.addChild(targetNode);
  this.forceUpdate();
};

@action
eraseValidationMsg = () => {
  this.validationMsg = [];
}

@action
download = () => {
  const json = this.graph;
  /* CSV
       const fields =
       [
       'name',
       'type',
       'subtype',
       'temperature',
       'volume'
       ]
   ;
   const opts = { fields };*/
  if (this.isValid()) {
    try {
      /* CSV
      const csv = json2csv(this.exportNodes, opts);
      var csvBlob = new Blob([csv], {type: 'text/csv'});
      fileDownload(csvBlob, 'sketch.csv');*/
      var jsonBlob = new Blob([JSON.stringify(this.exportNodes)], { type: 'application/json' });
      fileDownload(jsonBlob, 'sketch.json');
    } catch (err) {
      console.error(err);
    }
  }
}

@action postData(callback){
  const me = this;
  var json = this.exportNodes;
  const dataToSend = {
    name: this.title.replace(/\s/g, '').toLowerCase(),
    title: this.title,
    description: this.description,
    author: 'K2P Sketcher',
    keywords: this.keywords,
    location: 'Uganda',
    fileJson: json,
  };
  me.setCatalogueModalState(true);
  fetch('https://server.waterpathogens.org/flowtool/sendSketchToCatalogue', {
    method: "POST",
    credentials: "include",
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(dataToSend)
  }).then((res) => res.json()).then((data) => {
    this.setUploadResponse(data.response);
    if (this.uploadResponse == "ok") {
      //setTimeout(function(){document.location.href = `https://${ window.location.host }`},500);
      me.setCatalogueModalState(false);
      if (callback) callback();
    }
    else {
      this.setUploadResponse("error");
    }
  });
}

@action
loadData = (data) => {
  const graph = this.graph;
  const genericKeys = ['children', 'parents', 'x', 'y', 'name', 'number'];

  var newNodes = [];
  var newEdges = [];

  for (var i in data) {
    var newPayload = new SketchNodeModel(data[i].number - 1);

    for (var key in data[i]) {
      if (data[i].hasOwnProperty(key) && !genericKeys.includes(key)) {
        newPayload.setProp(key, data[i][key]);
      }
    }
    const newNode = {
      id: data[i].name + '',
      title: data[i].name + '',
      type: this.machineToHuman(data[i].ntype),
      payload: newPayload,
      x: (data[i].x) ? data[i].x : Math.floor(Math.random() * (701)) + 100,
      y: (data[i].y) ? data[i].y : Math.floor(Math.random() * (401)) + 100,
    }
    newNodes.push(newNode);
    if (data[i].children.length > 0) {
      const nodeChildren = data[i].children;
      for (var j = 0; j <= nodeChildren.length - 1; j++) {
        const newEdge = { source: newNode.id, target: nodeChildren[j], type: EMPTY_EDGE_TYPE };

        newEdges.push(newEdge);
        newNode.payload.addChild(nodeChildren[j]);
      }
    }
  }
  graph.nodes = newNodes;
  this.setGraph(graph);

  graph.edges = newEdges;

  this.setGraph(graph);
  this.setImportModalState(false);
  this.setImportStatus(true);
  this.validationMsg = [];
}

@action importData = (e) => {
  const content = this.fileReader.result;
  try {
    const data = JSON.parse(content);
    this.loadData(data);
    this.GraphView.handleZoomToFit();
  }
  catch (e) {
    this.pushMessage('There was an error parsing the imported file.');
    console.log(e);
  }
}

@action fetchSketch(url) {
  fetch(url)
  .then(res => res.blob()) // Gets the response and returns it as a blob
  .then(blob => {
    this.handleFile(blob);
});
}

@action handleFile(file){

  //var json = JSON.parse(str);
  this.fileReader = new FileReader();
  this.fileReader.onloadend = this.importData;
  var a = this.fileReader.readAsText(file);
  // this.model  = new DiagramModel();
  // this.model.deSerializeDiagram(json.sketch, this.engine);
  // this.engine.setDiagramModel(this.model);
  // this.nodes = [];
  // for (var node in this.model.nodes){
  //     this.model.nodes[node].addListener(this.nodeListener);
  //     var details = json.details.find((det)=>det.name==this.model.nodes[node].name);
  //     for(var data in details){
  //         console.log(data);
  //         this.model.nodes[node][data] = details[data];
  //     }
  //     this.nodes.push(this.model.nodes[node]);
  // }
  // console.log(this.nodes);
  // refreshCanvas();
}

@action
styleEdge = (id, element, edge, edgeContainer, isEdgeSelected) => {
  edgeContainer.style.stroke = '#f0f0f0 !important';
  edgeContainer.style.backgroundColor = '#f0f0f0 !important';
  edgeContainer.style.color = '#f0f0f0 !important';
}

@action
toggleIsCalculating = () => {
  this.isCalculating = !this.isCalculating
}

@action
toggleCalculationComplete = () => {
  this.calculationComplete = !this.calculationComplete
}

@action
setCalculationData = (data) => {
  this.calculationData = data;
}

@action 
calculate = () => {
  if (this.isValid() && this.isComplete()) {
    this.setSelectedNode(null);
    this.toggleIsCalculating();
    const data = this.exportNodes;
    const requestOptions = {
      method: 'POST',
      headers: {'Content-type': 'application/json'},
      body: JSON.stringify(data)
    };
    fetch('https://server.waterpathogens.org/flowtool/sketcher', requestOptions)
      .then(response => response.json())
      .then((data) => { this.toggleIsCalculating();this.setCalculationData(JSON.parse(data.results))})
      .catch((error) => {console.log(error);this.pushMessage('Unexpected Server error occurred.');})

    this.toggleCalculationComplete();
  }
}

  @action
  renderNodeText = (data, id) => {
var svgIcon = null;
    // Uncomment for custom icons
    //if (data.payload.subType == 'None') {
      if (data.type == SOURCE_TYPE) {
        var svgIcon = <svg viewBox="0 0 512 512" y="-25" x="-10" height="20" width="20"><path fill="#003347" 
          d="M451.4 369.1C468.7 356 480 335.4 480 312c0-39.8-32.2-72-72-72h-14.1c13.4-11.7 22.1-28.8 22.1-48 0-35.3-28.7-64-64-64h-5.9c3.6-10.1 5.9-20.7 5.9-32 0-53-43-96-96-96-5.2 0-10.2.7-15.1 1.5C250.3 14.6 256 30.6 256 48c0 44.2-35.8 80-80 80h-16c-35.3 0-64 28.7-64 64 0 19.2 8.7 36.3 22.1 48H104c-39.8 0-72 32.2-72 72 0 23.4 11.3 44 28.6 57.1C26.3 374.6 0 404.1 0 440c0 39.8 32.2 72 72 72h368c39.8 0 72-32.2 72-72 0-35.9-26.3-65.4-60.6-70.9zM192 256c17.7 0 32 14.3 32 32s-14.3 32-32 32-32-14.3-32-32 14.3-32 32-32zm159.5 139C341 422.9 293 448 256 448s-85-25.1-95.5-53c-2-5.3 2-11 7.8-11h175.4c5.8 0 9.8 5.7 7.8 11zM320 320c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"></path>
        </svg>;
      }
      if (data.type == UNIT_PROCESS_TYPE) {
        var svgIcon = <svg viewBox="0 0 512 512" y="-25" x="-10" height="20" width="20"><path fill="#003347" d="M487.4 315.7l-42.6-24.6c4.3-23.2 4.3-47 0-70.2l42.6-24.6c4.9-2.8 7.1-8.6 5.5-14-11.1-35.6-30-67.8-54.7-94.6-3.8-4.1-10-5.1-14.8-2.3L380.8 110c-17.9-15.4-38.5-27.3-60.8-35.1V25.8c0-5.6-3.9-10.5-9.4-11.7-36.7-8.2-74.3-7.8-109.2 0-5.5 1.2-9.4 6.1-9.4 11.7V75c-22.2 7.9-42.8 19.8-60.8 35.1L88.7 85.5c-4.9-2.8-11-1.9-14.8 2.3-24.7 26.7-43.6 58.9-54.7 94.6-1.7 5.4.6 11.2 5.5 14L67.3 221c-4.3 23.2-4.3 47 0 70.2l-42.6 24.6c-4.9 2.8-7.1 8.6-5.5 14 11.1 35.6 30 67.8 54.7 94.6 3.8 4.1 10 5.1 14.8 2.3l42.6-24.6c17.9 15.4 38.5 27.3 60.8 35.1v49.2c0 5.6 3.9 10.5 9.4 11.7 36.7 8.2 74.3 7.8 109.2 0 5.5-1.2 9.4-6.1 9.4-11.7v-49.2c22.2-7.9 42.8-19.8 60.8-35.1l42.6 24.6c4.9 2.8 11 1.9 14.8-2.3 24.7-26.7 43.6-58.9 54.7-94.6 1.5-5.5-.7-11.3-5.6-14.1zM256 336c-44.1 0-80-35.9-80-80s35.9-80 80-80 80 35.9 80 80-35.9 80-80 80z"></path></svg>
      }
      if (data.type == END_USE_TYPE) {
        var svgIcon = <svg viewBox="0 0 352 512" y="-25" x="-10" height="20" width="20"><path fill="#003347" d="M205.22 22.09c-7.94-28.78-49.44-30.12-58.44 0C100.01 179.85 0 222.72 0 333.91 0 432.35 78.72 512 176 512s176-79.65 176-178.09c0-111.75-99.79-153.34-146.78-311.82zM176 448c-61.75 0-112-50.25-112-112 0-8.84 7.16-16 16-16s16 7.16 16 16c0 44.11 35.89 80 80 80 8.84 0 16 7.16 16 16s-7.16 16-16 16z"></path></svg>
      }
    //}
    // else {
    //   var src = require('../static/img/Icons/aerated_pond.svg');
    //   var injected = <ReactSVG src={src} wrapper="span" afterInjection={(error, svg) => {
    //       var svgIcon = svg;
    //     }}/>;
    // }
    
    return (
      <g id={id}>
        {(svgIcon != null)? svgIcon: null}
      <circle cx="-10" cy="24" r= "4" stroke="none" fill={data.payload.complete ? "#0D8446":"#C60013"} />
      <text className="node-text selected" y="10" textAnchor="middle">
        {(data.payload.subType != 'None') ? 
          <tspan opacity="0.5" fontSize={data.payload.subType.length > 20 ? "13px": "16px" }>{data.payload.subType}</tspan>
          :<tspan opacity="0.5">{this.machineToHuman(data.type)}</tspan>}
        <tspan x="0" dy="18" fontSize="10px">{data.title}</tspan>
        <title>{id}</title>
      </text>
      </g>
    );
  }

  @action
  renderDefs = () => {
    let defIndex = 0;
    const graphConfigDefs = [];

    const color = '#003347';

    // code removed to fill in graphConfigDefs with other defs.

    return (
      <React.Fragment>
        {graphConfigDefs}

        return (
        <marker
          id={`end-arrow-yolo`}
          key={`end-arrow-yolo`}
          markerHeight={String(this.edgeArrowSize)}
          markerWidth={String(this.edgeArrowSize)}
          orient="auto"
          refX={String(this.edgeArrowSize / 2)}
          viewBox={`0 -${this.edgeArrowSize / 2} ${this.edgeArrowSize} ${
            this.edgeArrowSize
            }`}
        >
          <path
            d={`M0,-${this.edgeArrowSize / 2}L${this.edgeArrowSize},0L0,${this
              .edgeArrowSize / 2}`}
            style={{ fill: color }}
          />
        </marker>
        );
      })
      </React.Fragment>
    );
  }

  @computed get exportNodes(){
    return this.graph.nodes.map((node) => {
      return {
        x: node.x,
        y: node.y,
        name: node.title+"",
        parents: this.graph.nodes.filter((parent) => parent.payload.children.includes(node[NODE_KEY])).map((node) => (node.title+"")),
        children: node.payload.children.map((id) => this.getNode(id).title+""),
        ntype: this.machineToHuman(node.type),
        subType: node.payload.subType,
        temperature: node.payload.temperature,
        retentionTime: node.payload.retentionTime,
        surfaceArea: node.payload.surfaceArea,
        volume: node.payload.volume,
        flowRate: node.payload.flowRate,
        depth: node.payload.depth,
        contactTime: node.payload.contactTime,
        concentration: node.payload.concentration,
        pH: node.payload.pH,
        dose: node.payload.dose,
        useCategory: node.payload.useCategory,
        soilType: node.payload.soilType,
        depthGroundwater: node.payload.depthGroundwater,
        HydraulicLoading: node.payload.HydraulicLoading,
        solidsRetention: node.payload.solidsRetention,
        typeOfFlow: node.payload.typeOfFlow,
        description: node.payload.description,
        moistureContent: node.payload.moistureContent,
        holdingTime: node.payload.holdingTime,
        matrix: node.payload.matrix,
        number: node.payload.number,
        influentNode: (node[NODE_KEY] == this.influent),
        effluentNode: (node[NODE_KEY] == this.effluent)
      }
    });
  }

  handleJoyrideCallback = data => {
    const {status} = data;
    if ([STATUS.FINISHED, STATUS.SKIPPED].includes(status)) {
      this.setRun(false);
    }
  }


  /*
   * Render
   */

  render() {

    const { nodes, edges } = this.graph;
    const selected = this.selected;
    const { NodeTypes, NodeSubtypes, EdgeTypes } = GraphConfig;

    const parsed = queryString.parse(window.location.search);
    if (parsed['id'] && !this.importStatus) {
      fetchJsonp('https://data.waterpathogens.org/api/3/action/package_show?id=a1423a05-7680-4d1c-8d67-082fbeb00a50&callback=cb', {
        jsonpCallbackFunction: 'cb'
      })
        .then(function (response) {
          return response.json()
        }).then((json) => {
          console.log(json.result.resources[0].url);
          fetch(json.result.resources[0].url)
            .then(res => res.json())
            .then((out) => {
              this.loadData(out);
            })
            .catch(err => { throw err });
        }).catch(function (ex) {
          console.log('parsing failed', ex)
        })
    }
    else { }


      return (
      <ThemeProvider theme={theme()}>
        <Joyride
          callback={this.handleJoyrideCallback}
          continuous={true}
          getHelpers={this.getHelpers}
          run={this.run}
          scrollToFirstStep={true}
          showProgress={true}
          showSkipButton={true}
          steps={steps}
          locale={{last:'Finish'}}
          // styles={{
          //   options: {
          //     zIndex: 20000,
          //   },
          // }}
        />
        <div id="main">
          <HeaderBar context={this} nodes={nodes}></HeaderBar>
          <div id="graph">
            <SendMappingCatalog context={this} />
            <ImportModal context={this}/>
            <ScreenshotModal context={this} />
            <InfoModal context={this} />
            <SankeyModal context={this} />
            <NodeDial context={this}/>
            <GenerateReportModal context={this}/>
            <LightTooltip title='Click on the Calculate button after you finish sketching your treatment system and inputting the required data, to visualize the predicted reduction of pathogens in the system.'>
            <Fab id="calculateButton" variant="extended" disabled={this.isCalculating || this.calculationComplete} style={{top: '20px', left:'90px', position:'absolute' }} onClick={() => this.calculate()}>
                <PlayArrowTwoToneIcon/>
                Calculate
              </Fab>
              </LightTooltip>
            <Snackbar
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              open={this.validationMsg.length > 0}
            >
              <Alert onClose={() => { this.eraseValidationMsg() }} severity="error">
                <ul>
                  {this.validationMsg.map((msg) => <li>{msg}</li>)}
                </ul>
              </Alert>
            </Snackbar>
            <Snackbar
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              autoHideDuration={6000}
              open={this.importStatus}
              onClose={() => { this.setImportStatus(false) }}
            >
              <Alert autoHideDuration={6000} severity="success">
                {<span>Sketch import complete!</span>}
              </Alert>
            </Snackbar>
            <Grid container style={{ height: '100vh' }}>
              <Grid item xs={12} md={8} lg={9}>
              
                <GraphView
                ref={el => (this.GraphView = el)}
                nodeKey={NODE_KEY}
                nodes={nodes}
                edges={edges}
                selected={selected}
                nodeTypes={NodeTypes}
                nodeSubtypes={NodeSubtypes}
                edgeTypes={EdgeTypes}
                onSelectNode={this.onSelectNode}
                onCreateNode={this.onCreateNode}
                onUpdateNode={this.onUpdateNode}
                onDeleteNode={this.onDeleteNode}
                onSelectEdge={this.onSelectEdge}
                onCreateEdge={this.onCreateEdge}
                onSwapEdge={this.onSwapEdge}
                onDeleteEdge={this.onDeleteEdge}
                renderNodeText={this.renderNodeText}
                onUndo={this.onUndo}
                edgeArrowSize={5}
                maxTitleChars={20}
                afterRenderEdge={this.styleEdge}
                onCopySelected={this.onCopySelected}
                onPasteSelected={this.onPasteSelected}
                layoutEngineType={this.layoutEngineType}
                readOnly={this.calculationComplete}
              />
              
              </Grid>
              <SketchSideBar context={this} nodes={nodes} edges={edges}/>
            </Grid>
            <canvas></canvas>
          </div>
          
        </div>
      </ThemeProvider>
    );
  }
}

export default Graph;