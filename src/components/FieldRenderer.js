import {observer} from "mobx-react-lite";
import * as React from "react";
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from "@material-ui/core/OutlinedInput/OutlinedInput";
import Select from '@material-ui/core/Select';

const FieldRenderer = observer(({context, field}) => {
    const graph = context; 
    switch (field.type) {
        case 'select':
          return <FormControl required={field.required} variant='outlined' style={{width: '100%'}}>
                    <InputLabel
                        id={field.propName}>{field.label}
                    </InputLabel>
                    <Select 
                        input={
                            <OutlinedInput
                              labelWidth={100}
                              name={field.propName}
                              id={"outlined-"+field.propName+"-simple"}
                            />
                          }
                        labelId={field.propName} 
                        fullWidth 
                        value={graph.selected.payload[field.propName]} 
                        defaultValue={field.defaultValue ? field.defaultValue: '' } 
                        onChange={(e) => {
                            graph.selected.payload.setProp(field.propName, e.target.value);
                        }} >
                        <MenuItem value={''}>{'None'}</MenuItem>
                        {field.options.map(option => {return <MenuItem value={option.value} >{option.key}</MenuItem>} )}
                    </Select>
                </FormControl>;
        case 'textarea':
          return <TextField
                    id={field.propName}
                    label={field.label}
                    required={field.required}
                    placeholder={field.placeholder}
                    rows={'2'}
                    variant='outlined'
                    multiline
                    fullWidth
                    value={graph.selected.payload[field.propName]}
                    onChange={(e) => { graph.selected.payload.setProp(field.propName, e.target.value) }}
                    margin="normal"
                    onKeyUp={(e) => { e.stopPropagation(); }}
                />;
        case 'number':
          return <TextField 
                    id={field.propName} 
                    label={field.label} 
                    variant="outlined"
                    type='number'
                    required={field.required} 
                    fullWidth
                    value={graph.selected.payload[field.propName]}
                    onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp(field.propName, e.target.value); }} 
                /> ;
        default:
          return null;
    }

});

export default FieldRenderer;
