import React from 'react';
import { WithContext as ReactTags } from 'react-tag-input';

const KeyCodes = {
    comma: 188,
    enter: 13,
};

const delimiters = [KeyCodes.comma, KeyCodes.enter];

class MultiInput extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            // eslint-disable-next-line
            tags: props.value&&(JSON.stringify(props.value)!="[\"\"]")?props.value.map(function (value) { return {id:value, text:value} }):[],
            suggestions: props.suggestions?props.suggestions:[]
        };
        this.handleDelete = this.handleDelete.bind(this);
        this.handleInputBlur = this.handleInputBlur.bind(this);
        this.handleAddition = this.handleAddition.bind(this);
        this.handleDrag = this.handleDrag.bind(this);
    }

    handleDelete(i) {
        const { tags } = this.state;
        const newTags = tags.filter((tag, index) => index !== i);
        this.setState({ tags: newTags });
        this.props.setParentValue(newTags.map(item => item.text));
    }

    handleAddition(tag) {
        const newTags = [...this.state.tags, tag];
        this.setState({tags: newTags});
        this.props.setParentValue(newTags.map(item => item.text));
    }
    handleInputBlur(tag) {
        // eslint-disable-next-line
        if(tag && tag!='') {
            tag = {id:tag, text:tag}
            const newTags = [...this.state.tags, tag];
            this.setState({tags: newTags});
            this.props.setParentValue(newTags.map(item => item.text));
        }
    }
    handleDrag(tag, currPos, newPos) {
        const tags = [...this.state.tags];
        const newTags = tags.slice();

        newTags.splice(currPos, 1);
        newTags.splice(newPos, 0, tag);

        this.setState({ tags: newTags });
        this.props.setParentValue(newTags.map(item => item.text));
    }

    render() {
        const { tags, suggestions } = this.state;
        return (
            <ReactTags tags={tags}
               suggestions={suggestions}
               handleDelete={this.handleDelete}
               handleAddition={this.handleAddition}
               handleDrag={this.handleDrag}
               delimiters={delimiters}
               placeholder={this.props.placeholder}
               handleInputBlur={this.handleInputBlur}
            />
        )
    }
};
export default MultiInput;