import React, {useState, useEffect, useRef} from 'react';
import { Sankey, SankeyNode, SankeyLink, Tooltip } from 'reaviz';
import chroma from "chroma-js";
import ReactEchartsCore from 'echarts-for-react/lib/core';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/chart/sankey';

const SankeyChart = ({ graph, json, i }) => {
    
    const data = json.nodes.map( (node) => {
        return {name: node.name}
    });
    const links = json.links.map((link, i) => {
        return {
            source: json.nodes.filter((node) => parseInt(node.newID) - 1 === link.source)[0].name,
            target: json.nodes.filter((node) => parseInt(node.newID) - 1  === link.target)[0].name,
            value: link.value
        }
    });
    console.log(data);
    console.log(links);
    const levels = [{
        depth: 0,
        itemStyle: {
            color: '#A07958'
        },
        lineStyle: {
            color: 'source',
            opacity: 0.6
        }
        }, {
        depth: 1,
        itemStyle: {
            color: '#B99A7D'
        },
        lineStyle: {
            color: 'source',
            opacity: 0.6
        }
    }, {
        depth: 2,
        itemStyle: {
            color: '#CBB79F'
        },
        lineStyle: {
            color: 'source',
            opacity: 0.6
        }
    }, {
        depth: 3,
        itemStyle: {
            color: '#9CB9B7'
        },
        lineStyle: {
            color: 'source',
            opacity: 0.6
        }
    },
    {
        depth: 4,
        itemStyle: {
            color: '#5E8F8B'
        },
        lineStyle: {
            color: 'source',
            opacity: 0.6
        }
    },
    {
        depth: 5,
        itemStyle: {
            color: '#487F7C'
        },
        lineStyle: {
            color: 'source',
            opacity: 0.6
        }
    }
    ];

    const option = {
        tooltip: {
            trigger: 'item'
        },
        series: {
            type: 'sankey',
            layout: 'none',
            focusNodeAdjacency: 'allEdges',
            data: data,
            links: links,
            levels: levels,
            label: {
                formatter: (params) => { 
                    var incomingLinks = links.filter((link) => {return link.target === params.data.name});
                    var sum = 0;
                    for (var i in incomingLinks) {
                        sum += incomingLinks[i].value;
                    }
                    sum = (sum === 0) ? 100 : sum;
                    return params.data.name+' ('+sum+'%)';
                }
            },
            itemStyle: {
                borderWidth: 0,
                fontFamily: 'Cabin, sans-serif'
            }
        }
    };
    return <ReactEchartsCore
        echarts={echarts}
        option={option}
        notMerge={true}
        lazyUpdate={true}
        style={{height: '350px'}}
        ref={(e) => {graph.setFlowRefs(i, e)}}
        opts={{
            // Exporting format, can be either png, or jpeg
            type: 'png',
            // Resolution ratio of exporting image, 1 by default.
            pixelRatio: 2,
            // Background color of exporting image, use backgroundColor in option by default.
            backgroundColor: '#fff',
        }}
    />
    
    // <Sankey
    //                 colorScheme={colorScheme}
    //                 height={500}
    //                 gradient={true}
    //                 id="sankeyPlot"
    //                 nodes={json.nodes.map((node, i) => {
    //                     var id = parseInt(node.newID);
    //                     var normalizedID = id - 1
    //                     return (<SankeyNode 
    //                                 key={'node-'+i} 
    //                                 title={node.name} 
    //                                 id={normalizedID+""}
    //                                 />)
    //                 })}
    //                 links={json.links.map((link, i) => { 
    //                     return (<SankeyLink 
    //                                 key={'link-'+i} 
    //                                 source={(link.source)+""} 
    //                                 target={(link.target)+""} 
    //                                 value={link.value.toFixed(2)} 
    //                                 gradient={true} 
    //                             />)
    //                 })}
    //             />
}

export default SankeyChart;