import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React, {useState} from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import MuiAlert from '@material-ui/lab/Alert';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import Divider from '@material-ui/core/Divider';
import ContaminantsForm from "./ContaminantsForm.js";
import SankeyChart from "./SankeyChart.js";
import { Link } from 'react-router-dom'; 
import { SOURCE_TYPE } from "../../config/graph-config.js";

function Alert(props) {
    return <MuiAlert elevation={2} {...props} />;
} 

const GenerateReportModal = observer(({ context }) => {

    const [error, setError] = useState(false);
    const [errorName, setErrorName] = useState(false);
    const [errorAuthorName, setErrorAuthorName] = useState(false);
    const [errorOrganization, setErrorOrganization] = useState(false);
    const [errorEmail, setErrorEmail] = useState(false);
    const [errorPhone, setErrorPhone] = useState(false);
    const [errorStreet, setErrorStreet] = useState(false);
    const [errorCity, setErrorCity] = useState(false);
    const [errorState, setErrorState] = useState(false);
    const [errorCountry, setErrorCountry] = useState(false);
    // const [errorDescription, setErrorDescription] = useState(false);
    const [errorPopulation, setErrorPopulation] = useState(false);
    const [errorCapacitySewage, setErrorCapacitySewage] = useState(false);
    const [errorCapacityFecal, setErrorCapacityFecal] = useState(false);
    const [errorConclusion, setErrorConclusion] = useState(false);
    const [errorResultDetails, setErrorResultDetails] = useState(false);

    const [errorMessage, setErrorMessage] = useState([]);

    const graph = context;
    var report = graph.reportData;

    // ***************** Set default values for testing purposes ****************
    // graph.setReportValue('authorName', 'Panagis Katsivelis');
    // graph.setReportValue('organization', 'Venthic Technologies');
    // graph.setReportValue('email', 'panagis.katsivelis@venthic.com');
    // graph.setReportValue('city', 'Athens');
    // graph.setReportValue('country', 'Greece');
    // graph.setReportValue('name', 'Panagis\' Report');
    // graph.setReportValue('capacitySewage', '1000');
    // graph.setReportValue('capacityFecal', '1000');
    // graph.setReportValue('population', '1000000');
    // graph.setReportValue('resultDetails', 'Panagis approves');
    // graph.setReportValue('conclusion', 'This is the conclusion');

    const handleImage = (file) => {
        graph.setReportValue('photo', file);
        var reader = new FileReader();
        // reader.readAsDataURL( file );
        var baseString = '';
        reader.onloadend = function () {
            baseString = reader.result;
            report.photo = baseString;
        };
        reader.readAsDataURL(file);
    }

    const validate = (report) => {
        
            let flag = true;
            setErrorMessage([]);

            if(!report.authorName || report.authorName === ""){
                setErrorAuthorName(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the author is required!']);
                flag = false;
            } else{
                setErrorAuthorName(false)
            } 

            if(!report.organization || report.organization === ""){
                setErrorOrganization(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the organization is required!']);
                flag = false;
            } else setErrorOrganization(false);

            if(!report.email || report.email === ""){
                setErrorEmail(true);
                setErrorMessage(errorMessage => [...errorMessage,'The email address is required!']);
                flag = false;
            } else { 
                if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(report.email)) {
                    setErrorEmail(false);
                }
                else {
                    setErrorEmail(true);
                    setErrorMessage(errorMessage => [...errorMessage,'The email address provided is not valid!']);
                    flag = false;
                }
                
            }

            // if(!report.phone || report.phone === ""){
            //     setErrorPhone(true);
            //     setErrorMessage(errorMessage => [...errorMessage, 'The phone number is required!']);
            //     flag = false;
            // } else setErrorPhone(false);

            // if(!report.street || report.street === ""){
            //     setErrorStreet(true);
            //     setErrorMessage(errorMessage => [...errorMessage,'The name of the street is required!']);
            //     flag = false;
            // } else setErrorStreet(false);

            if(!report.city || report.city === ""){
                setErrorCity(true);
                setErrorMessage(errorMessage => [...errorMessage,'The name of the city is required!']);
                flag = false;
            } else setErrorCity(false);

            // if(!report.state || report.state === ""){
            //     setErrorState(true);
            //     setErrorMessage(errorMessage => [...errorMessage, 'The name of the Administrative Division is required!']);
            //     flag = false;
            // } else setErrorState(false);

            if(!report.country || report.country === ""){
                setErrorCountry(true);
                setErrorMessage(errorMessage => [...errorMessage, 'The name of the country is required!']);
                flag = false;
            } else setErrorCountry(false);

            if(!report.name || report.name === ""){
                setErrorName(true);
                setErrorMessage(errorMessage => [...errorMessage,'The name of the treatment plant is required!']);
                flag = false;
            } else setErrorName(false);


            if(!report.resultDetails || report.resultDetails === ""){
                setErrorResultDetails(true);
                setErrorMessage(errorMessage => [...errorMessage, 'Your interpretation of the results is required!']);
                flag = false;
            } else setErrorResultDetails(false);

            if(!report.conclusion || report.conclusion === ""){
                setErrorConclusion(true);
                setErrorMessage(errorMessage => [...errorMessage,'A conclusion is required!']);
                flag = false;
            } else setErrorConclusion(false);

            if(!report.population === "" || report.population === 0){
                setErrorPopulation(true);
                setErrorMessage(errorMessage => [...errorMessage, 'Population served is required!']);
                flag = false;
            } else setErrorPopulation(false);

            if( report.capacitySewage === "" && report.capacityFecal=== "" ){
                setErrorCapacitySewage(true);
                setErrorCapacityFecal(true);
                setErrorMessage(errorMessage => [...errorMessage, 'Sewage or Fecal capacity (or both) has to be specified.']);
                flag = false;
            } else {
                setErrorCapacitySewage(false);
                setErrorCapacityFecal(false);
            }
            if (!flag) { setError(true) }
            return flag;
        
    }

    return (
        <div>
        <Dialog
            maxWidth='lg'
            fullWidth={true}
            id="screenshotDialog"
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={graph.generateReportModalState}
            onClose={(e) => {
                graph.setGenerateReportModalState(false);
            }}
        >
            <DialogTitle>
                <center>Generate Report</center>
            </DialogTitle>
            {!graph.generatePDFButton  ?
                <DialogContent style={{height: '20px', overflowY:'hidden'}}>
                    
                    {(graph.screenshotObject === 'bar') ?
                        <div>
                        </div>: 
                            (graph.screenshotObject === 'flow') ? 
                                <div>
                                    <center>Printing Flow Diagrams...</center>
                                    
                                        {graph.calculationData.flowCharts.map((chart, i) => {
                                            return <div style={{visibility: 'hidden'}} className={'sankeyPlot-'+i} id="sankeyPlot"><SankeyChart graph={graph} json={graph.calculationData.flowCharts[i]} i={i}/></div>
                                        })}
                                        
                                    </div>:<center>Generating PDF File...</center>
                    }
                </DialogContent>
                :
                <DialogContent>
                    <Grid container spacing={1}>
                        {error &&
                            <Grid item xs={12} md={12}> 
                                <Alert severity="error">
                                    <ul>
                                    {
                                        errorMessage.map((message)=>{
                                            return <li>{message}</li> 
                                        })
                                    }
                                    </ul>
                                </Alert>
                                
                            </Grid>
                        }
                    </Grid>
                    <Grid container spacing={2} style={{marginBottom:'10px'}}>
                        <Grid item xs={12} sm={12}>
                            <Typography variant="h3" gutterBottom>
                                Water Utility Information
                            </Typography>
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorAuthorName} defaultValue={report.authorName} required id="authorName" label="Author Name" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('authorName', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorOrganization} defaultValue={report.organization} required id="organization" label="Water Utility Title" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('organization', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorEmail} defaultValue={report.email} required id="email" label="Email" fullWidth type="email" variant='outlined'
                                onChange={(e) => {graph.setReportValue('email', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorPhone} defaultValue={report.phone} id="phone" label="Phone Number" fullWidth type="tel" variant='outlined'
                                onChange={(e) => {graph.setReportValue('phone', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorStreet} defaultValue={report.street} id="street" label="Address Line" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('street', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField  error={errorCity} defaultValue={report.city} required id="city" label="City" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('city', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} md={6}>
                            <TextField error={errorState} defaultValue={report.state} id="state" label="Administrative Division (state, department, province, etc.)" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('state', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField error={errorCountry} defaultValue={report.country} required id="country" label="Country" fullWidth variant='outlined'
                                onChange={(e) => {graph.setReportValue('country', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        <Grid item xs={12} sm={12}>
                            <Typography variant="h3" gutterBottom>
                                Report data
                            </Typography>
                        </Grid>
                        <ContaminantsForm context={context}/>
                        <Grid item xs={12} sm={6}>
                            
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField id="systemDetails" defaultValue={report.systemDetails} label="Additional health information" fullWidth multiline rows="4" variant='outlined'
                                onChange={(e) => {graph.setReportValue('systemDetails', e.target.value);}} 
                                onKeyPress={e => {if (e.key === 'Enter') e.preventDefault();}}  />
                        </Grid>
                        
                        <Grid item xs={12} sm={6}>
                            <div style={{height:'120px', display: 'block', backgroundColor:'#ddd', borderRadius:'5px', textAlign:'center', padding:'20px'}}>
                                <label htmlFor="raised-button-file" style={{fontWeight: 700}}>Upload a photograph of the treatment utility</label> 
                                <input 
                                    accept="image/*" 
                                    id="raised-button-file" 
                                    multiple 
                                    type="file" 
                                    style={{marginTop: '20px'}}
                                    onChange={(e)=>{handleImage(e.target.files[0]); if (report.photoCredit === '') { graph.setReportValue('photoCredit', report.authorName);}}}
                                    /> <br/>
                                    <TextField size="small" style={{width: '320px', marginTop: '20px'}} id="photoCredit" defaultValue={report.photoCredit} label="Photo Credit" fullWidth variant='outlined'
                                    onChange={(e) => {graph.setReportValue('photoCredit', e.target.value);}}></TextField>
                            </div>
                            
                        </Grid>
                       
                    </Grid>
                    
                </DialogContent>
            }
            <DialogActions>
                <Link to='/ccr-preview' target='_blank' style={{textDecoration: 'none'}}><Button color="primary" autoFocus >Generate Online Report</Button></Link>
                <Button color="primary" autoFocus disabled >Generate PDF</Button>
                <Button  onClick={() => {graph.setGenerateReportModalState(false);}} color="primary">Cancel</Button>
            </DialogActions>
        </Dialog>
        </div>

    )
});

export default GenerateReportModal;