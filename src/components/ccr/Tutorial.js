import React, {useState} from "react";
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import YouTube from 'react-youtube';

const  Tutorial = () => {
    const classes = useStyles();
    const [activeStep, setActiveStep] = useState(0);
    const steps = getSteps();
  
    const handleNext = () => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };
  
    const handleBack = () => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };
  
    const handleReset = () => {
      setActiveStep(0);
    };

    const opts = {
        height: '351', 
        width: '576',
        playerVars: {
          autoplay: 0,
        }
    };
    return (
        <div className={classes.root}>
            <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
                <Step key={label}>
                    <StepLabel>{label}</StepLabel>
                </Step>
            ))}
            </Stepper>
            <div>
                <Box>
                    {activeStep === steps.length ? (
                        <div>
                            <Box display="flex" justifyContent="center">
                                <Typography variant="h3" className={classes.instructions}>You are now ready to start using the Treatment Plant Sketcher Tool!</Typography>
                            </Box>
                            <Box display="flex" justifyContent="center">
                                <Button variant="contained" color="primary" onClick={handleReset}>Start Over</Button>
                            </Box>
                            
                        </div>
                    ) : (
                        <div>
                            {getYoutubeLink(activeStep) !== ''?
                                <Box display="flex" justifyContent="center">
                                    <YouTube videoId={getYoutubeLink(activeStep)} opts={opts}/>
                                </Box>
                                : null
                            }
                            <Typography align='justify' className={classes.instructions}>{getStepContent(activeStep)}</Typography>
                            <div>
                                <Button disabled={activeStep === 0} onClick={handleBack} className={classes.backButton}>Back</Button>
                                <Button variant="contained" color="primary" onClick={handleNext}>
                                    {activeStep === steps.length - 1 ? 'Finish' : 'Next'}
                                </Button>
                            </div>
                        </div>
                    )}
                </Box>
            </div>
        </div>
    );


}
function getSteps() {
    return ['What is the Sketcher Tool?', 'Step 1. Sketch the System.', 'Step 2. Specify the Nodes.', 'Step 3. Run the Analysis.', 
    'Step 4. Print a Report.', 'Step 5. Download and Upload Sketches.'];
}

function getYoutubeLink(step) {
    switch (step) {
        case 2:
            return 'nzhrneh7H-o'
        case 3:
            return 'Oi7ASVFd6os'
        case 5:
            return 'D-Ky75mx2SE'
        default:
            return '';
    }
}

function getStepContent(step) {
    switch (step) {
        case 0:
            return <p>The GWPP-K2P Treatment Plant Sketcher Tool predicts the effectiveness of a wastewater or fecal sludge treatment system at removing and reducing pathogens (viruses, bacteria, protozoa, helminths). The tool allows you to create a “sketch” of a treatment system, specifying how it is designed and operated, then estimate the proportion of pathogens that are reduced by the system and visualize what proportion of the pathogens end up in the liquid effluent vs. the sludge/biosolids. You can add different treatment unit processes, change the way they are configured, and change their design and operational parameters to understand what impacts these changes have pathogen reduction. For more information about viruses, bacteria, protozoa, and helminth pathogens, please refer to the online GWPP book, Sanitation and Disease in the 21st Century: Health and Microbiological Aspects of Excreta and Wastewater Management (www.waterpathogens.org).</p>;
        case 1:
            return <p>A “sketch” is essentially a flow diagram consisting of nodes and arrows, which depicts a wastewater or fecal sludge treatment system. There are three different types of nodes, which correspond to sources, unit processes, and end use of products (liquid effluent and sludge/biosolids) from the treatment plant:
            <ul><li><b>Source:</b> wastewater and/or fecal sludge received at the plant.</li><li><b>Unit processes:</b> the treatment processes and technologies used at the plant.</li>
            <li><b>End uses:</b> the final disposal or reuse of treated effluent and sludge/biosolids</li></ul>
            The nodes correspond to the sources, unit processes, and end use of products from the treatment plant, and the arrows indicate the direction of flow. To use the tool, you need to specify the sources of waste (wastewater or fecal sludge) received at the plant, then add and specify the configuration and characteristics of the different treatment unit processes used at the plant, then add and specify the end use of the final treated effluent and sludge/biosolids. Then, you need to connect each of the nodes with arrows. Once the sketch is complete, you can click calculate to see the predicted pathogen reduction. Sketches files can be downloaded and saved to your computer, and imported back into the Sketcher Tool so that you can to upload them and modify or reanalyze them later, and c. Completed sketches can also be published to our online open data portal.
            <br/>By default, the webpage loads with three nodes already added: a source, a unit process, and an end use. 
            To add new nodes to your wastewater or fecal sludge treatment system, hover over the + icon to select new nodes to add to your sketch. Use the mouse to click and drag the nodes to wherever 
            you want them on the screen. Click on a node to select it. Click the “delete” button or press the delete key on your keyboard to delete it. Click the “clone” button or press Ctrl+C, Ctrl+V on 
            your keyboard to copy-paste it (create a duplicate). To connect two nodes, first select (by clicking on) the first node, then select the dropdown menu labeled “Node Connections” to select the 
            number of the node(s) to which you would like to draw the arrow. You can also connect two nodes with your mouse, by selecting the first node and dragging the mouse to the second node while holding 
            the "Shift” button. More than one arrow can leave from a given node to indicate one or more flow paths of liquid effluent and one or more flow paths of residual solids (sludge) removal.</p>;
        case 2:
            return <p>When you click on a node for a source, unit process, or end-use, a panel on the right side of the screen will contain some further information that 
            must be entered for that node in order to make the sketch complete. The additional information consists of key design and operational parameters about the system 
            and its unit processes that are important factors for pathogen reduction efficiency. For instance, if you specify one of the unit process nodes as a “facultative 
            pond,” you will be prompted to enter information about the size and depth of the pond. If you select “sludge drying bed” as a unit process, you will be prompted to 
            enter information about the moisture content of the sludge to be treated and the holding time, or the number of days sludge is stored in the drying bed. All nodes 
            must be specified with a matrix, which must be specified as either liquid or solid. For example, select liquid for unit processes designed to treat sewage and solid 
            for unit processes designed to treat sludge. For a source, you must indicate whether it is fecal sludge or sewage. To do this, click on the node to select it, then 
            provide information in the pop-up window on the right (use the dropdown menu to select whether it is fecal sludge or sewage). The matrix for sewage should be liquid 
            and the matrix for fecal sludge should be solid (use dropdown to select solid or liquid). You must also specify the average daily volume received. You may add in any 
            notes in the box provided if you wish to do so. A node will show a red circle if there is still missing information that needs to be added. The circle will turn green 
            once all required information is provided. Note that if you try to run the model without first entering this information, an error message will pop up at the bottom right 
            of the screen.</p>;
        case 3:
            return <p>Click on the calculate button on the top left to see the reduction of viral, bacterial, protozoan, and helminth pathogens predicted for your sketched system. The output appears on the right-hand side of the screen in a graph and in a table. The graph shows the percentage of pathogens that are predicted to be discharged in the liquid effluent vs. the sludge/biosolids, and the table shows the predicted overall log10 reduction and percent (%) reduction values for each pathogen type (viruses, bacteria, protozoa, and helminths). You can click on the small circles for each pathogen group to visualize the predicted flow of pathogens throughout the sketched system. The width of the bars in these flow charts indicate the percentage of pathogens still surviving at each point in the treatment system, and at the discharge points for liquid effluent and sludge/biosolids.</p>;
        case 4:
            return <p><i>Coming soon...</i></p>; 
            // <p>Print a pdf report of your sketch and the results of the analysis and submit the sketched treatment system to K2P database for publication 
            // through the publish button (if desired). Note that this will allow your sketched system to be viewed by other users.</p>;
        case 5:
            return <p>Note that if you refresh the webpage, click the “back” button on your browser, close your browser, or shut down your computer, you will lose your sketch. However, there is a way to save your work and continue at another time. Save your sketch by clicking the “DownloadSave” button at the upper right-hand side of the screen. This will prompt you to download something called a JSON file, which is . Give it a name and note that the extension will be *.JSON. A JSON file is just a computer-readable file in Javascript language that contains all of the information needed to re-create your sketch at a future time. You do not need to open the file nor do you need any kind of special software to save it. Just give it a name and save it in a folder on your computer where you will be able to find it later. Note that depending on your browser’s settings, files downloaded from the internet may automatically save into a default folder (usually, the default location for this is a folder on your hard drive called “Downloads”). The default name of the file is “sketch.json”. You can change the name of this file to give it a more meaningful title, but note that the file’s extension should be *.json. The next time when you are ready to continue working on your sketch, you can bring up the Sketcher tool page website and click the “Import” button located at the upper right-hand side of the screen. It will prompt you to select a file. Navigate to the folder on your computer where you saved the file, select your JSON file, and click “Open” to upload your saved sketch. Now you can continue working on it. If you want to save a visual representation of your sketch as an image file, you can click on the “camera icon” and use the “save” button to save a copy of your sketch (i.e., a “screenshot” of your sketch) so that you can see how it was configured. Note that you will not be able to use this image file to import your sketch back into the Sketcher Tool. To do that, you must use the “DownloadSave” button and save a JSON file, as explained above.</p>;
        default:
         return 'Unknown step';
    }
}

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
    },
    backButton: {
      marginRight: theme.spacing(1),
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
}));

export default Tutorial;