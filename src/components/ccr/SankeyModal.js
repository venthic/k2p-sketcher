import { observer } from "mobx-react-lite";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import Tab from "@material-ui/core/Tab/Tab";
import Tabs from "@material-ui/core/Tabs/Tabs";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import MuiAlert from '@material-ui/lab/Alert';
import SankeyChart from './SankeyChart.js';



const SankeyModal = observer(({ context }) => {
    const graph = context.graph;
    const json = (context.calculationData.flowCharts) ? context.calculationData.flowCharts[context.pathogenType] : [];
    

        

    return (
        <div>
            <Dialog
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={context.sankeyModalState}
                onClose={(e) => {
                    context.setImportModalState(false)
                }}
                fullWidth
                maxWidth={'lg'}
            >

                <DialogContent id="sankeyPlot">
                    {(json && json.nodes) &&
                        <div>
                            <Tabs
                                value={context.pathogenType}
                                indicatorColor="primary"
                                textColor="primary"
                                aria-label="pathogen tabs"
                                centered
                            >
                                <Tab onClick={() => context.setPathogenType(0)} label="Viruses" />
                                <Tab onClick={() => context.setPathogenType(1)} label="Bacteria" />
                                <Tab onClick={() => context.setPathogenType(2)} label="Protozoa" />
                                <Tab onClick={() => context.setPathogenType(3)} label="Helminths" />
                            </Tabs>

                            <SankeyChart graph={context} json={json} i={0}/>
                
                        </div>
                    }


                </DialogContent>

                <DialogActions>
                    <span style={{fontSize: '13px', fontStyle: 'italic',color: '#6a6a6a'}}>Numeric values represent the percentage of pathogen groups remaining in the system at any given point in the treatment train.</span>
                    <Button onClick={() => {
                        context.setSankeyModalState(false)
                    }} color="primary">
                        Close
                    </Button>

                </DialogActions>
            </Dialog>

        </div>
    )
});

export default SankeyModal;
