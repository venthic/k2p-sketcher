import {observer} from "mobx-react-lite";
import { makeStyles } from '@material-ui/core/styles';
import * as React from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import SourceMap from "./SourceMap";
import Button from "@material-ui/core/Button/Button";


const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    maxWidth: 752,
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
    margin: theme.spacing(4, 0, 2),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
}));

const MapModal = observer(({context}) => {

return (

  <Dialog
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={context.mapModalState}
            onClose={(e) => {
                context.setMapModalState(false)
            }}
            maxWidth='lg'
            fullWidth
        >
            <DialogTitle>
                <center>Sketch area</center>
            </DialogTitle>
            
                <DialogContent >
                  <div style={{width: '100%', height: 300}}>
                <SourceMap style={{height: '100%'}} context={context}/>
                </div>

                </DialogContent>
            
            <DialogActions>
                <Button onClick={() => {
                    context.setMapModalState(false)
                }} color="primary">
                    Close
                    </Button>
                
            </DialogActions>
        </Dialog>
); 
});


export default MapModal;
