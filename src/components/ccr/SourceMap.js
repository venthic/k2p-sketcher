import React, {useState, useEffect, useRef} from 'react';
import {observer} from "mobx-react-lite";
import MapGL from "react-map-gl";
import {
  Editor,
  EditingMode,
  DrawPolygonMode,
  DrawCircleFromCenterMode,
  DrawRectangleMode
} from "react-map-gl-draw"

const SourceMap = ({ context }) => {

    const DEFAULT_VIEWPORT = {
        width: 800,
        height: 600,
        longitude: -122.45,
        latitude: 37.78,
        zoom: 14,
      };

      const MODES = [
        { id: 'drawPolygon', text: 'Draw Polygon', handler: DrawPolygonMode },
        { id: 'drawCircle', text: 'Draw Circle', handler: DrawCircleFromCenterMode },
        { id: 'drawRectangle', text: 'Draw Rectangle', handler: DrawRectangleMode },
        { id: 'editing', text: 'Edit Feature', handler: EditingMode },
      ];

      const [viewport, setViewport] = useState(DEFAULT_VIEWPORT);
      const [modeId, setModeId] = useState(null);
      const [modeHandler, setModeHandler] = useState(null);
    
    const switchMode = (evt) => {
        const id = evt.target.value === modeId ? null : evt.target.value;
        const mode = MODES.find((m) => m.id === id);
        const modeHandler = mode ? new mode.handler() : null;
        setModeId(mode);
        setModeHandler(modeHandler);
      };
    
      const _renderToolbar = () => {
        return (
          <div
            style={{ position: "absolute", top: 0, right: 0, maxWidth: "320px" }}
          >
            <select onChange={(e) => switchMode(e)}>
              <option value="">--Please choose a draw mode--</option>
              {MODES.map((mode) => (
                <option key={mode.id} value={mode.id}>
                  {mode.text}
                </option>
              ))}
            </select>
          </div>
        );
      };
    
// return <div></div>;
    return (<MapGL
        {...viewport}
        width="100%"
        height="100%"
        mapboxApiAccessToken={'pk.eyJ1IjoicGFuYWdpcyIsImEiOiJja2FwN2d1YXAwN2Z4MnNtc3A2bWRxZjA4In0.IFTmzqOx7alZqusO7RGXIw'}
        mapStyle={"mapbox://styles/mapbox/light-v10"}
        onViewportChange={(viewport) => setViewport(viewport)}
    >
        <Editor
        // to make the lines/vertices easier to interact with
        clickRadius={12}
        mode={modeHandler}
        onSelect={(_) => {}}
        onUpdate={(data) => context.setSourceMapData(data)}
        />
        {_renderToolbar()}
    </MapGL>);
};

export default SourceMap;