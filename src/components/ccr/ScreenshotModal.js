import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React from "react";
import styled from "@emotion/styled";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import FormControl from "@material-ui/core/FormControl/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import Typography from "@material-ui/core/Typography/Typography";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import ChipInput from 'material-ui-chip-input';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import GraphConfig, {
    NODE_KEY
} from '../../config/graph-config';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

const ImportModal = observer(({ context }) => {
    const graph = context.graph;

    return (
        <div>
        <Dialog
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={context.screenshotModalState}
            onClose={(e) => {
                context.setScreenshotModalState(false)
            }}
        >
            <DialogTitle>
                <center>Sketch Screenshot</center>
            </DialogTitle>
            
                <DialogContent>
                    
                    <img src={context.dataUrl} alt="Sketch Screenshot" width="500"/>
                </DialogContent>
            
            <DialogActions>
                <Button onClick={() => {
                    context.setScreenshotModalState(false)
                }} color="primary">
                    Cancel
                    </Button>
                    <Button href={context.dataUrl} color="secondary" download="sketch.png">
                    Download
                    </Button>
            </DialogActions>
        </Dialog>
        {/* <Snackbar
        autoHideDuration={6000} 
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={context.uploadResponse == 'ok'}
          onClose={()=>context.setUploadResponse('')}
        >
          <Alert severity="success">
            <p>
              The sketch has been uploaded successfully!
            </p>
          </Alert>
          </Snackbar>
          <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          autoHideDuration={6000} 
          open={context.uploadResponse == 'error'}
          onClose={()=>context.setUploadResponse('')}
        >
          <Alert severity="error">
            <p>
              An unexpected error occurred.
            </p>
          </Alert>
          </Snackbar> */}
        </div>
    )
});

const PostFormLabel = styled.label`
    font-family: 'Roboto', sans-serif;
    display: block;
    margin-bottom: 20px !important;
    font-size: 16px;
    font-weight: 400;
`;

const PostFormInput = styled(TextField)`
    width: 270px;
    height: 20px;
    margin-bottom:20px;
`;

export default ImportModal;
