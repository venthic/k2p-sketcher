import { observer } from "mobx-react-lite";
import * as React from "react";
import Button from "@material-ui/core/Button/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup/ButtonGroup";
import OutlinedInput from "@material-ui/core/OutlinedInput/OutlinedInput";
import Grid from "@material-ui/core/Grid/Grid";
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import ListItemText from '@material-ui/core/ListItemText';
import Select from '@material-ui/core/Select';
import Checkbox from '@material-ui/core/Checkbox';
import Chip from '@material-ui/core/Chip';
import Tooltip from '@material-ui/core/Tooltip';
import Typography from '@material-ui/core/Typography';
import DeleteTwoToneIcon from '@material-ui/icons/DeleteTwoTone';
import FileCopyTwoToneIcon from '@material-ui/icons/FileCopyTwoTone';
import { ThemeProvider, withStyles } from "@material-ui/core/styles";
import FieldRenderer from "./FieldRenderer.js";
import theme from "../../config/Theme.js";
import MouseTwoToneIcon from '@material-ui/icons/MouseTwoTone';
import GraphConfig, {
    UNIT_PROCESS_TYPE,
    SOURCE_TYPE,
    END_USE_TYPE,
    NODE_KEY
} from '../../config/graph-config';
import {subTypes} from '../../config/ccr-node-config';

const useStyles = makeStyles(theme => ({
    paper: {
        padding: theme.spacing(5, 2),
        color: theme.palette.text.secondary,
        backgroundColor: '#09142714',

    },
    wrapper: {
        height: '100vh',
    },
    welcomeTxt: {
        color: '#003347',
    },
    snackbar: {
        margin: theme.spacing(1),
        backgroundColor: '#003347',
        boxShadow: 'none',
        textAlign: 'center',
        marginBottom: '30px',
    },
    logoBox: {
        padding: '25px',
        textAlign: 'center',
    },
    formControl: {
        width: '100%'
    }
}));
const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const SketchSideBar = observer(({ context, nodes, edges }) => {
    const graph = context;
    const classes = useStyles();
    
    return (
    <ThemeProvider theme={theme()}>
        <Grid item md={4} lg={3} xs={12} style={{borderLeft: '1px solid #003347'}}>
            <Container style={{paddingTop:'20px'}} id="sidebarContent">
                <div role="presentation">

                    <div className="graph-header">
                        
                        {(graph.selected && !graph.calculationComplete) ? <div>
                            
                            <div style={{ padding: '5px' }} id="payloadHolder">
                                <Typography variant='h3' style={{marginBottom:'20px'}}>{graph.selected.title} {graph.selected.payload.subType}</Typography>

                                <div className="nodeField">
                                                <FormControl variant='outlined' required={true} className={classes.formControl}>
                                                <InputLabel
                                                    id="subType">Description
                                                </InputLabel>
                                                    <Select 
                                                        labelId="subType" 
                                                        value={graph.selected.payload.subType} 
                                                        onChange={(e) => { /*graph.setTitle(e);*/ graph.selected.payload.setProp('subType', e.target.value); }} 
                                                        input={
                                                            <OutlinedInput
                                                              labelWidth={100}
                                                              name="subType"
                                                              id="outlined-subType-simple"
                                                            />
                                                          }
                                                    >
                                                        <MenuItem value={'None'}>None</MenuItem>
                                                        {subTypes.filter(type => graph.selected.type == type.subtypeOf).map(type => (
                                                            <MenuItem value={type.machineName}>{type.name}</MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                        </div>
                                        { (graph.selected.payload.subType != 'None') &&
                                            
                                            subTypes.filter(type => type.machineName == graph.selected.payload.subType)[0].fields.map((field) => {
                                                return <div className="nodeField"><FieldRenderer context={graph} field={field}/></div>
                                            })
                                        }
                                        
                                
                                        {/*
                                        {((graph.selected.payload) && ['anaerobic pond', 'facultative pond', 'aerated pond', 'fws', 'maturation pond', 'ss', 'activated sludge', 'biogas reactor'].includes(graph.selected.payload.subType)) &&
                                            <div className="nodeField">
                                                
                                                <TextField 
                                                    id="outlined-basic" 
                                                    label={'Hydraulic Retention Time ' +( (graph.selected.payload.subType == "activated sludge") ? '(hours)' : '(days)')} 
                                                    variant="outlined"
                                                    type='number' 
                                                    value={graph.selected.payload.retentionTime}
                                                        onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('retentionTime', e.target.value) }}
                                                    />
                                                </div>
                                        }
                                        {((graph.selected.payload) && ['anaerobic pond', 'facultative pond', 'aerated pond', 'fws', 'maturation pond'].includes(graph.selected.payload.subType)) &&
                                            <div className="nodeField">
                                                    <TextField 
                                                    id="outlined-basic" 
                                                    label={'Depth (meters)'} 
                                                    variant="outlined"
                                                    type='number' 
                                                    value={graph.selected.payload.depth}
                                                    onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('depth', e.target.value); }} 
                                                    />                                                    
                                                </div>
                                        }
                                        {((graph.selected.payload) && ['anaerobic pond', 'facultative pond', 'aerated pond', 'fws', 'maturation pond', 'biogas reactor'].includes(graph.selected.payload.subType)) &&
                                            <div className="nodeField">
                                                    <TextField 
                                                        id="outlined-basic" 
                                                        label={'Temperature (Celsius)'} 
                                                        variant="outlined"
                                                        type='number' 
                                                        value={graph.selected.payload.temperature}
                                                        onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('temperature', e.target.value) }}
                                                    /> 
                                                </div>
                                        }
                                        {(graph.selected.payload && graph.selected.payload.subType == 'ss') &&
                                            <div className="nodeField">
                                                
                                                    <Select value={graph.selected.payload.typeOfFlow} onChange={(e) => {
                                                        graph.selected.payload.setProp('typeOfFlow', e.target.value);
                                                    }} inputProps={{ name: 'type', id: 'type', }}>
                                                        <MenuItem value={'horizontal flow'}>Horizontal Flow</MenuItem>
                                                        <MenuItem value={'vertical flow'}>Vertical Flow</MenuItem>
                                                    </Select>
                                             </div>
                                        }
                                        {(graph.selected.payload.subType == 'ss') &&
                                            <div className="nodeField">
                                                    <TextField 
                                                        id="outlined-basic" 
                                                        label={'Hydraulic Loading Rate (cm / day)'} 
                                                        variant="outlined"
                                                        type='number' 
                                                        value={graph.selected.payload.HydraulicLoading}
                                                        onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('HydraulicLoading', e.target.value) }} 
                                                    />
                                                </div>
                                        }
                                        {(graph.selected.payload && graph.selected.payload.subType == 'media filter') &&
                                            <div className="nodeField">
                                                    
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="filterTypeLabel">Type of filter</InputLabel>
                                                    <Select 
                                                        labelId="filterTypeLabel" 
                                                        variant='outlined' 
                                                        value={graph.selected.payload.typeOfFilter} 
                                                        onChange={(e) => {
                                                            graph.selected.payload.setProp('typeOfFilter', e.target.value);
                                                        }} 
                                                        inputProps={{ name: 'type', id: 'type', }}>
                                                        <MenuItem value={'anaerobic filter'}>Anaerobic Filter</MenuItem>
                                                        <MenuItem value={'trickling filter'}>Trickling Filter</MenuItem>
                                                    </Select>
                                                </FormControl>
                                                </div>
                                        }
                                        {(graph.selected.payload.subType == 'activated sludge') &&
                                            <div className="nodeField">
                                                <TextField 
                                                        id="outlined-basic" 
                                                        label={'Solids Retention Time (days)'} 
                                                        variant="outlined"
                                                        type='number'
                                                        value={graph.selected.payload.solidsRetention}
                                                        onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('solidsRetention', e.target.value) }}
                                                />
                                                        
                                                </div>
                                        }
                                        {(['discharge to surface water', 'dump in surface water'].includes(graph.selected.payload.subType)) &&
                                            <div className="nodeField">
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="useCategoryLabel1">Use Category</InputLabel>
                                                    <Select variant='outlined' labelId="useCategoryLabel1" value={graph.selected.payload.useCategory} onChange={(e) => {
                                                        graph.selected.payload.setProp('useCategory', e.target.value);
                                                    }} inputProps={{ name: 'type', id: 'type', }}>
                                                        <MenuItem value={'drinking water source'}>Drinking water source</MenuItem>
                                                        <MenuItem value={'recreational water'}>Recreational water</MenuItem>
                                                        <MenuItem value={'other'}>Other (no exposure)</MenuItem>
                                                    </Select>
                                                    </FormControl>
                                                </div>
                                        }
                                        {(['land application', 'irrigation'].includes(graph.selected.payload.subType)) &&
                                            <div className="nodeField">
                                                <FormControl className={classes.formControl}>
                                                    <InputLabel id="useCategoryLabel2">Use Category</InputLabel>
                                                    <Select variant='outlined' labelId="useCategoryLabel2" value={graph.selected.payload.useCategory} onChange={(e) => {
                                                        graph.selected.payload.setProp('useCategory', e.target.value);
                                                    }} inputProps={{ name: 'type', id: 'type', }}>
                                                        <MenuItem value={'edible crops consumed raw'}>Edible crops consumed raw</MenuItem>
                                                        <MenuItem value={'edible crops consumed cooked'}>Edible crops consumed cooked</MenuItem>
                                                        <MenuItem value={'crops for animal feed'}>Crops for animal feed</MenuItem>
                                                        <MenuItem value={'other non-edible crops'}>Other non-edible crops</MenuItem>
                                                    </Select>
                                                    </FormControl>
                                                </div>
                                        }
                                        {(['fill and cover', 'groundwater recharge'].includes(graph.selected.payload.subType)) &&
                                            <div className="nodeField">
                                                    <TextField 
                                                            id="outlined-basic" 
                                                            label={'Depth to groundwater'} 
                                                            variant="outlined"
                                                            type='number'
                                                            value={graph.selected.payload.depthGroundwater}
                                                            onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('depthGroundwater', e.target.value) }}
                                                    />
                                                    
                                                </div>
                                        }
                                        {(graph.selected.payload && graph.selected.payload.subType == 'fill and cover') &&
                                            <div className="nodeField">
                                                        <TextField 
                                                            id="outlined-basic" 
                                                            label={'Soil type'} 
                                                            variant="outlined"
                                                            type='number'
                                                            value={graph.selected.payload.soilType}
                                                            onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('soilType', e.target.value) }}
                                                            />
                                                </div>
                                        }
                                        {(graph.selected.payload && graph.selected.payload.subType == 'sludge drying bed') &&
                                            <div className="nodeField">
                                                    <TextField 
                                                        id="outlined-basic" 
                                                        label={'Initial Moisture Content (%)'} 
                                                        variant="outlined"
                                                        type='number'
                                                        value={graph.selected.payload.moistureContent}
                                                        onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('moistureContent', e.target.value) }}
                                                        />
                                                </div>
                                        }
                                        {(graph.selected.payload && graph.selected.payload.subType == 'sludge drying bed') &&
                                            <div className="nodeField">
                                                <TextField 
                                                        id="outlined-basic" 
                                                        label={'Holding Time (days)'} 
                                                        variant="outlined"
                                                        type='number'
                                                        value={graph.selected.payload.holdingTime}
                                                        onKeyUp={(e) => { e.stopPropagation(); }} onChange={(e) => { graph.selected.payload.setProp('holdingTime', e.target.value) }} 
                                                        />
                                                </div>
                                        }
                                        <div className="nodeField">
                                            <FormControl className={classes.formControl}>
                                                <InputLabel id="matrixLabel">Matrix</InputLabel>
                                                <Select variant='outlined' value={graph.selected.payload.matrix} onChange={(e) => {
                                                    graph.selected.payload.setProp('matrix', e.target.value);
                                                }} inputProps={{ name: 'type', id: 'type', }}>
                                                    <MenuItem value={'liquid'}>Liquid</MenuItem>
                                                    <MenuItem value={'solid'}>Solid</MenuItem>
                                                </Select>
                                                </FormControl>
                                            </div>
                                        <div className="nodeField">
                                                {(graph.selected.payload) && <TextField
                                                    id="node description"
                                                    label='Notes'
                                                    placeholder="Provide additional notes about the node."
                                                    rows={'2'}
                                                    variant='outlined'
                                                    multiline
                                                    value={graph.selected.payload.description}
                                                    onChange={(e) => { graph.selected.payload.setProp('description', e.target.value) }}
                                                    margin="normal"
                                                    onKeyUp={(e) => { e.stopPropagation(); }}
                                                />
                                                }
                                            </div>
                                            */}
                            </div>
                            <div>
                                <p>
                                    <LightTooltip placement='left' title='Use this to connect two nodes with an arrow; select a node, then use the the drop-down menu to indicate to which other node(s) you would like for it to connect. '>
                                <FormControl disabled={graph.selected.type==END_USE_TYPE} id="nodeConnections" className={classes.formControl}>
                                        <InputLabel id="nodeConnectionsLabel">Node Connections</InputLabel>
                                        <Select
                                            labelId="nodeConnectionsLabel"
                                            id="nodeConnections"
                                            multiple
                                            value={(graph.selected.payload.children.length >0) ? 
                                                graph.selected.payload.children
                                            :[]}
                                            variant='outlined'
                                            onChange={graph.connectionEdit}
                                            input={<Input variant='outlined' id="select-multiple-chip" />}
                                            renderValue={(selected) => (
                                                <div className={classes.chips}>
                                                    {selected.map((value) => (
                                                        <Chip key={value} label={nodes.filter(node => node[NODE_KEY] == value )[0].title} className={classes.chip} />
                                                    ))}
                                                </div>
                                            )}
                                        >
                                            {graph.availableConnections(graph.selected).map((node) => (
                                                
                                                <MenuItem key={node.title} value={node[NODE_KEY]}>
                                                    <Checkbox size='small' checked={graph.selected.payload.children.indexOf(node[NODE_KEY]) > -1} />
                                                    <ListItemText primary={node.title} />
                                                </MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                    </LightTooltip>     
                                    </p>
                                    <p style={{textAlign:'center'}}>
                                        <ButtonGroup>
                                        
                                        <Button startIcon={<FileCopyTwoToneIcon/>}onClick={() => graph.copyNode()}>Clone</Button>
                                        <Button variant='outlined' startIcon={<DeleteTwoToneIcon />} onClick={() => graph.deleteSelectedNode()}>Delete</Button>
                                        </ButtonGroup>
                                    </p>
                            </div>
                        </div>:
                        <div style={{height:'300px', display: ((graph.calculationComplete) ? 'none': 'block'), backgroundColor:'#ddd', borderRadius:'5px', marginTop: '50px', textAlign:'center'}}>
                            <p><MouseTwoToneIcon style={{marginTop: '100px'}} fontSize='large'/></p>
                            <Typography variant='subtitle1' style={{color: 'rgb(53, 53, 53)'}}>Select a node.</Typography>
                        </div>
                        }
                        </div>
                </div>
                
            </Container>
        </Grid>
        </ThemeProvider>
    )
});

export default SketchSideBar;
