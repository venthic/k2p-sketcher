import {observer} from "mobx-react-lite";
import * as React from "react";
import {StackedNormalizedBarSeries, StackedNormalizedBarChart, Gradient, Bar, GradientStop, RangeLines, multiCategory, schemes} from "reaviz";

import ReactEchartsCore from 'echarts-for-react/lib/core';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/chart/bar';
import { withStyles } from "@material-ui/styles";
import theme from "../config/Theme.js";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import chroma from 'chroma-js';
import Paper from '@material-ui/core/Paper';
import BubbleChartTwoToneIcon from '@material-ui/icons/BubbleChartTwoTone';
import RestorePageTwoToneIcon from '@material-ui/icons/RestorePageTwoTone';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PictureAsPdfTwoToneIcon from '@material-ui/icons/PictureAsPdfTwoTone';
import ArrowBackTwoToneIcon from '@material-ui/icons/ArrowBackTwoTone';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
    fontSize: 14,
    fontWeight: 500
  },
}))(Tooltip);


function valueFormat(value) {
  if (value <= 99) {
    return parseInt(value);
  } else if ( value <= 99.9) {
    return value.toFixed(1);
  } else if ( value <= 99.99) {
    return value.toFixed(2);
  } else if ( value <= 99.999) {
    return value.toFixed(3);
  } else {
    return value.toFixed(4);
  }
}

function colorFormat(value) {
  if (value < 1) {
    return '#fe5252';
  } else if ( value < 2) {
    return 'orange';
  } else if ( value < 3) {
    return '#ffcb00';
  } else if ( value < 4) {
    return '#6ec980';
  } else {
    return '#1d9b4e';
  }
}

const LRVforPDF = observer(({context}) => {
  const rx = 0;
  const ry = 0;
  const height = 270;
  const width = 250;
  const rangeWidth = 3;
  const hasGradient = true;
  const hasRangelines = true;
  const rounded = false;
  const color = chroma
    .scale(['#4E71BE', '#DF8344'])
    .correctLightness()
    .colors(2);


   const option = {
      tooltip: {
          trigger: 'axis',
          axisPointer: {            
              type: 'shadow'        
          }
      },
      legend: {
          data: ['Percent % in Liquid Effluent', 'Percent % in Sludge/Biosolids']
      },
      textStyle: {
        fontFamily: 'Cabin, sans-serif'  
      },
      grid: {
          containLabel: true
      },
      xAxis: [
          {
              type: 'category',
              data: ['Virus', 'Bacteria', 'Protozoa', 'Helminths'],
              axisLabel: {
                interval: 0
              }
          }
      ],
      yAxis: [
          {
              type: 'value'
          }
      ],
      series: [
            {
                name: 'Percent % in Liquid Effluent',
                type: 'bar',
                stack: 'lrv',
                data: (context.calculationData.stackChart ) ? context.calculationData.stackChart.series[0].data: [],
                itemStyle: {
                  color: new echarts.graphic.LinearGradient(
                    0, 0, 0, 1,
                    [
                        {offset: 0, color: '#4E71BE'},
                        {offset: 0.5, color: '#188df0'},
                        {offset: 1, color: '#B0BFE2'}
                    ]
                ),
                colorAlpha: 0.7
              }
            },
            {
                name: 'Percent % in Sludge/Biosolids',
                type: 'bar',
                stack: 'lrv',
                data: (context.calculationData.stackChart ) ? context.calculationData.stackChart.series[1].data: [],
                color: '#4E71BE',
                itemStyle: {
                  color: new echarts.graphic.LinearGradient(
                      0, 0, 0, 1,
                      [
                          {offset: 0, color: '#DF8344'},
                          {offset: 0.5, color: '#F1C8AB'},
                          {offset: 1, color: '#FBEFE8'}
                      ]
                  ),
                  colorAlpha: 0.7
              }
            },
            
        ]
      
  };
  

  const data = (context.calculationData.stackChart ) ? context.calculationData.stackChart.xAxis.categories.map((object, i) => {
    
    return {
      key: context.calculationData.stackChart.xAxis.categories[i],
      data: [ 
        {
            key: "Percent % in Liquid Effluent",
            data: context.calculationData.stackChart.series[0].data[i] 
        },
        {
          key: "Percent % in Sludge/Biosolids",
          data: context.calculationData.stackChart.series[1].data[i] 
        }
      ]
    }
  }): [];

const gradient = hasGradient ? (
  <Gradient
    stops={[
      <GradientStop offset="5%" stopOpacity={0.1} key="start" />,
      <GradientStop offset="90%" stopOpacity={0.7} key="stop" />
    ]}
  />
) : null;

const rangelines = hasRangelines ? (
  <RangeLines position="top" strokeWidth={rangeWidth} />
) : null;

return (

  <div>
    <div id="bars">
    <ReactEchartsCore
        echarts={echarts}
        option={option}
        style={{height: '300px', width:'300px'}}
        ref={(e) => {context.setBarRef(e)}}
        opts={{
            // Exporting format, can be either png, or jpeg
            type: 'png',
            // Resolution ratio of exporting image, 1 by default.
            pixelRatio: 2,
            // Background color of exporting image, use backgroundColor in option by default.
            backgroundColor: '#fff',
        }}
    />
  </div> 
    </div>
); 
});


export default LRVforPDF;
