import {observer} from "mobx-react-lite";
import * as React from "react";
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import OutlinedInput from "@material-ui/core/OutlinedInput/OutlinedInput";
import Select from '@material-ui/core/Select';
import IconButton from "@material-ui/core/IconButton";
import Button from "@material-ui/core/Button";
import { contaminants } from '../../config/contaminants';
import DeleteIcon from '@material-ui/icons/Delete';

const ContaminantsForm = observer(({context}) => {
    
  const handleRemoveContaminant = (idx) => () => {
    //this.setState({ shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx) });
  }

  const handleAddContaminant = () => () => {
    //this.setState({ shareholders: this.state.shareholders.filter((s, sidx) => idx !== sidx) });
  }

    return ( <div >
      <h4>Contaminants measurements</h4>
    
      {contaminants.map((c, idx) => (
        <div className="contaminant">
                  <FormControl variant='outlined'>
                    <InputLabel
                        id={'contaminant'+idx}>Contaminant
                    </InputLabel>
                    <Select 
                        style={{width: 200}}
                        input={
                            <OutlinedInput
                              labelWidth={100}
                              name={idx}
                              id={"outlined-"+idx+"-simple"}
                            />
                          }
                        labelId={'contaminant'+idx} 
                        fullWidth
                        defaultValue={c.name} 
                        onChange={(e) => {
                            
                        }} >
                      {contaminants.filter(con => con.name !== c.name ).map((cont) => {return <MenuItem value={cont.name}>{cont.name}</MenuItem>})}
                    </Select>
                </FormControl>
                <TextField 
                    id={'measure'+idx} 
                    label={'Measurement 1'} 
                    variant="outlined" 
                    style={{width: 200}}
                    value={c.default}
                />
                <TextField 
                    id={'measure'+idx} 
                    label={'Measurement 2 (for ranges)'} 
                    variant="outlined" 
                    style={{width: 200}}
                    value={''}
                />
                <FormControl variant='outlined'>
                    <InputLabel
                        id={'unit'+idx}>Unit
                    </InputLabel>
                    <Select 
                        style={{width: 200}}
                        input={
                            <OutlinedInput
                              labelWidth={100}
                              name={idx}
                              id={"outlined-"+idx+"-simple"}
                            />
                          }
                        labelId={'unit'+idx} 
                        fullWidth 
                        onChange={(e) => {
                            
                        }} >
                          <MenuItem value='ppm'>ppm</MenuItem>
                          <MenuItem value='ppb'>ppb</MenuItem>
                          <MenuItem value='pCi/L'>ppb</MenuItem>
                          <MenuItem value='NTU'>NTU</MenuItem>
                    </Select>
                </FormControl>
          <IconButton onClick={(idx) => handleRemoveContaminant(idx)} className="small"><DeleteIcon /></IconButton>
        <br/><br/>
        </div>
      ))}
      <Button variant='outlined' style={{float:'right'}} onClick={() => handleAddContaminant()} className="small">+ measurement</Button>
    </div>
    );
});

export default ContaminantsForm;
