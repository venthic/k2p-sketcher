import React, {useState} from 'react';
import { observer } from "mobx-react-lite";
import Fullpage, { FullPageSections, FullpageSection, FullpageNavigation } from '@ap.cx/react-fullpage'
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Slider, { Range } from 'rc-slider';
import { contaminants } from '../../config/contaminants';
import SourceMapView from './SourceMapView';
import DataTable from 'react-data-table-component';
import Tooltip from "@material-ui/core/Tooltip";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import LocalHospitalSharpIcon from '@material-ui/icons/LocalHospitalSharp';
import { AlertTriangle, XOctagon, CheckCircle, HelpCircle, ChevronsDown, PhoneOutgoing, User, Headphones } from 'react-feather';
import waterutility from '../../static/img/waterutility.png';
import sketch from '../../static/img/sketch.PNG';
import Box from '@material-ui/core/Box';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import Button from '@material-ui/core/Button';
import Timer from 'react-compound-timer';
import CheckCircleTwoToneIcon from '@material-ui/icons/CheckCircleTwoTone';
import '../../styles/slider.css';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
    fontSize: 14,
    fontWeight: 500
  }
}))(Tooltip);

const useStyles = makeStyles(theme => ({
  enhancedBox: {
      padding: 20,
      marginTop: 10,
      marginBottom: 10,
      borderRadius: 2,
      backgroundColor: '#eee',
      boxShadow: '0 1px 15px rgba(0,0,0,.04), 0 1px 6px rgba(0,0,0,.04)',
      border: '1px solid #e7e7e7'
  }
}));

const CCRReport = observer(({ context }) => {

  const classes = useStyles();
  const [filter, setFilter] = useState('All')
  const data = contaminants.filter(c => (c.category === filter || filter=== 'All')).map( ( c => {
    var marks = new Object();
    var max = 0;
    var check = '';
    if (c.mclg !== null) {
      marks[c.mclg] = <LightTooltip title='MCLG: The level of a contaminant in drinking water below which there is no known or expected risk to health.  MCLGs allow for a margin of safety.'><AlertTriangle style={{width: '20px', color:'orange'}}/></LightTooltip>;
      if (c.al === null ) {
        if (c.mcl !== null) {
          if (c.mcl > c.mclg) {
            max = c.mcl;
            marks[c.mcl] = <LightTooltip title='The highest level of a contaminant that is allowed in drinking water. '><AlertTriangle style={{width: '20px', color:'red'}}/></LightTooltip>;
            marks[(c.mcl+c.mclg)/2] = (c.mcl+c.mclg)/2;
          }
          else {
            max = c.mclg;
            marks[c.mclg] = <LightTooltip title='MCLG: The level of a contaminant in drinking water below which there is no known or expected risk to health.  MCLGs allow for a margin of safety.'><AlertTriangle style={{width: '20px', color:'orange'}}/></LightTooltip>;
            marks[(c.mclg+c.mcl)/2] = (c.mclg+c.mcl)/2;
          }
         
          if (c.default < max) {
            check = 'no violation';
          }
          else {
            check = 'violation'
          }
        } 
        else {
          max = c.mclg*2;
          marks[c.mclg/2] = c.mclg/2;
          marks[c.mclg+(c.mclg/2)] = c.mclg+(c.mclg/2);
          if (c.mclg > c.default) {
            check = 'no violation';
          }
          else {
            check = 'violation'
          }
        }
        
      }
      else {
        max = c.al*2;
        marks[c.al] = <LightTooltip title='Action Level (AL):  The concentration of a contaminant which, if exceeded, triggers treatment or other requirements that a water system must follow'><AlertTriangle style={{width: '20px', color:'orange'}}/></LightTooltip>;
        marks[c.al/2] = c.al/2;
        marks[c.al+(c.al/2)] = c.al+(c.al/2);
        if (c.default < c.al) {
          check = 'no violation'
        }
        else {
          check = 'violation';
        }
      }
    }
    else {
      if (c.al !== null) {
        max = c.al*2;
        marks[c.al/2] = c.al/2;
        marks[c.al+(c.al/2)] = c.al+(c.al/2);
        if (c.al > c.default) {
          check = 'no action needed';
        }
        else {
          check = 'action needed';
        }
      }
      else {
        if (c.mcl !== null) {
          marks[c.mcl] = <LightTooltip title='The highest level of a contaminant that is allowed in drinking water. '><AlertTriangle/></LightTooltip>;
          max = c.mcl*2;
          marks[c.mcl/2] = c.mcl/2;
          marks[c.mcl+(c.mcl/2)] = c.mcl+(c.mcl/2);
          check = 'no violation';
        }
        else {
          check = 'N/A';
        }
      }
    }
    var markObject = {default: c.default, marks: marks, max: max};
    return {
      id: c.name,
      title: c.name,
      measurements: markObject,
      check: check,
      category: c.category,
      effects: c.effects,
      sources: c.sources
    }
  })); 

  const ExpandedContaminant = ({ data }) => { return (data.check !== 'N/A') ? (data.category === 'Microbial contaminants') ? <Grid container spacing={2}>
  <Grid item md={6}><img style={{width: '100%'}} src={sketch}/></Grid>
  <Grid item md={6}>
    <p style={{fontSize: '1em'}}>There are no maximum levels set for pathogens. However, there are treatment and disinfection requirements. For example, depending on the source water quality and the types of treatment processes used, we need to add a certain amount of disinfectant (chlorine, ozone) to the water to make it safe to drink.</p>
  <p style={{fontSize: '1em'}}>This is a sketch of the treatment plant used to purify your water. Based on our analysis of the sketch, your treatment system is <b style={{color: 'rgba(0,175,150,1)'}}>in compliance</b> with the EPA regulations for disinfection. To learn more, check out the Global Water Pathogen Project (<a href="https://www.waterpathogens.org">www.waterpathogens.org</a>).</p></Grid>
</Grid>:<div style={{padding: '20px'}}>
    <Grid container spacing={4}>
      <Grid item md={8}>
        <Slider 
          min={0} 
          step={0.1}
          max = {data.measurements.max}
          marks={data.measurements.marks} 
          disabled
          style={{marginBottom: '20px'}}
          defaultValue={data.measurements.default} 
        />
      </Grid>
      <Grid item md={4}>
        <span style={{fontSize: '11px'}}><b>Last measurement</b>:<br/>
        November 15, 2020</span>
      </Grid>
    </Grid>

    </div>:  
<div style={{padding: '20px'}}>
  {data.measurements.default}
  </div>};

  const columns = [
    {
      name: 'Contamintant',
      selector: 'title',
      sortable: true,
      maxWidth: '180px'
    },
    {
      name: 'Results',
      selector: 'check',
      sortable: false,
      maxWidth: '30px',
      cell: (row) => { 
        switch (row.check) {
          case 'violation':
            return <LightTooltip title='Violation detected'><XOctagon style={{color: '#ff3333'}}/></LightTooltip>;
            break;
          case 'no violation':
            return <LightTooltip title='No violation detected'><CheckCircle style={{color: '#009933'}}/></LightTooltip>;
            break;
          case 'action needed':
            return <LightTooltip title='Action needed'><AlertTriangle style={{color: '#ffc34d'}}/></LightTooltip>;
            break;
          case 'no action needed':
            return <LightTooltip title='No action needed'><CheckCircle style={{color: '#009933'}}/></LightTooltip>;
            break;
          default:
            return <span>N/A</span>
        }
      }
    },
    {
      name: 'Category',
      selector: 'category',
      sortable: true,
      right: false,
      maxWidth: '150px'
    },
    {
      name: 'Health impact',
      selector: 'effects',
      sortable: false,
      right: false,
      maxWidth: '50px',
      cell: (row) => {return <LightTooltip title={row.effects}><LocalHospitalSharpIcon style={{width:'20px', color: '#343434'}}/></LightTooltip>}
    },
    {
      name: 'Sources',
      selector: 'sources',
      sortable: true,
      right: false,
      maxWidth: '50px',
      cell: (row) => {return <LightTooltip title={row.sources} style={{width:'19px', color: '#343434'}}><HelpCircle/></LightTooltip>}
    }
  ];

    return (
      <Fullpage className='ccr'>

        <FullpageNavigation/>
        <FullPageSections>
          <FullpageSection style={{
            backgroundColor: '#fff',
            height: '100vh',
            padding: '5%',
          }}>
            
        <Grid container>
          
          <Grid item md='6'>
          <h1 style={{fontSize: '6em', color: '#003347'}}>Hello.</h1>
            <p>We are the <b>Venthic County Water District Utility.</b></p>
            <p>We provide water to your household.</p>
            <p>Here's how...</p>  
            <Box className={classes.enhancedBox} style={{  margin: 'auto', fontSize:'1.2em', textAlign:'justify', width: '60%', marginTop: '30px' }}>
            <Grid container>
                <Grid item xs={4} style={{paddingLeft:'10%', paddingTop:'5%'}}><ChevronsDown size={48}/></Grid>
                <Grid item xs={8}>To view the next page, click on the navigation bubles on the right part of the screen or, simply, use your keyboard arrow keys.</Grid>
              </Grid>
              
            </Box>
          </Grid>
          <Grid item md='6'>
            <img style={{width:'100%'}} src={waterutility} alt='Water Utility photo'/>
          </Grid>
        </Grid>
        </FullpageSection>
          <FullpageSection style={{
            backgroundColor: '#F5FFFE',
            padding: '10em',
          }}>
          <Grid container spacing={4}>
          <Grid item md='12'>
            <h1 style={{fontSize: '3em'}}>Where does your water come from?</h1>
          </Grid>
          <Grid item md='6'>
            <p>Water flows from the source lakes through pipes to Saginaw's water treatment plant, where it is treated to meet drinking water and public health standards. City chemists and plant operators analyzed over 34,000 samples in 2020 to be sure the water supplied to homes and businesses is of the highest quality. This report is a summary of test results from samples taken during 2019.</p>
            <Box className={classes.enhancedBox} style={{ margin: 'auto', fontSize:'1.2em', textAlign:'justify', width: '70%'}}>
              <Grid container>
                <Grid item xs={4} style={{paddingLeft:'10%', paddingTop:'5%'}}><PhoneOutgoing size={48}/></Grid>
                <Grid item xs={8}>If you would like more information about the relevant Source Water Assessment report, please contact your water department at (810) 787-6555.</Grid>
              </Grid>
            </Box>  
          </Grid>
          <Grid item md='6'>
            <SourceMapView context={context}/>
          </Grid>
        </Grid>
          </FullpageSection>
          <FullpageSection style={{
            backgroundColor: '#F5FFFE',
            padding: '1em',
          }}>
            <Grid container>
          <Grid item md='12'>
            <h1 style={{fontSize: '3em', paddingLeft:'5%'}}>What contaminants are monitored in your drinking water?</h1>
          </Grid>
          <Grid item md='6' style={{height: '80vh', overflowY: 'auto', paddingLeft:'5%'}}>
          <DataTable
            columns={columns}
            data={data}
            pagination
            expandableRows
            expandOnRowClicked
            paginationPerPage={15}
            expandableRowsComponent={<ExpandedContaminant />}
            noHeader
            subHeader
            subHeaderComponent={
            <ButtonGroup variant="text" color="primary" aria-label="text primary button group">
              <Button style={{fontSize: '0.7em', letterSpacing: '1px', textTransform: 'capitalize', padding: '5px'}} onClick={() => setFilter('All')}>All</Button>
              {contaminants.map(c => c.category).filter(function(item, pos, self) {
                  return self.indexOf(item) === pos;
              }).map(cat => <Button style={{fontSize: '0.7em', letterSpacing: '1px', textTransform: 'capitalize', padding: '5px'}} onClick={() => setFilter(cat)}>{cat}</Button>)}
            </ButtonGroup>
            }
          />
            {/* { contaminants.map((contaminant) => { 
              
              return <Slider 
                dots
                min={0} 
                step={0.1}
                max = {max}
                marks={marks} 
                disabled
                style={{marginBottom: '20px'}}
                defaultValue={contaminant.default} 
                />})} */}
          </Grid>
          <Grid item md='6' style={{padding: '0 5% 0 5%'}}>
            <p>Drinking water, including bottled water, may reasonably be expected to contain at least small amounts of some contaminants. The presence of contaminants does not necessarily indicate that the water poses a health risk.</p>
            <p>Contaminants that may be present in source water include:</p>
            <ul>
              <li><span style={{color: '#003347'}}>Microbial contaminants</span>, such as viruses and bacteria, which may come from sewage treatment plants, septic systems, agricultural livestock operations, and wildlife.</li>
              <li><span style={{color: '#003347'}}>Inorganic contaminants</span>, such as salts and metals, which can be naturally-occurring or result from urban storm water runoff, industrial or domestic wastewater discharges, oil and gas production, mining, or farming.</li>
              <li><span style={{color: '#003347'}}>Pesticides and herbicides</span>, which may come from a variety of sources such as agriculture, urban storm water runoff, and residential uses.</li>
              <li><span style={{color: '#003347'}}>Organic chemical contaminants</span>, including synthetic and volatile organics, which are by-products of industrialprocesses and petroleum production, and can also come from gas stations, urban storm water runoff and septic systems.</li>
              <li><span style={{color: '#003347'}}>Radioactive contaminants</span>, which can be naturally occurring or be the result of oil and gas production and mining activities.</li></ul>
            <p>Click on each contaminant to learn more information about the concentrations that we measured and possible violations of National Primary Drinking Water Regulations.</p>
          </Grid>
        </Grid>
          </FullpageSection>
          <FullpageSection style={{
            backgroundColor: '#F5FFFE',
            padding: '10em',
          }}>
          <Grid container>
          <Grid item md='12'>
            <h1 style={{fontSize:'2.7em'}}>Do we comply with National Primary Drinking Water Regulations?</h1>
          </Grid>
          <Grid item md='6'>
            <div>
              <Box className={classes.enhancedBox}>
                <Grid container>
                  
                <Timer initialTime={41541105736}>
                <Grid item md='3' style={{fontSize: '2em', textAlign:'center'}}><h2 style={{marginBottom:'10px'}}><Timer.Days /></h2><br/>days</Grid>
                <Grid item md='3' style={{fontSize: '2em', textAlign:'center'}}><h2 style={{marginBottom:'10px'}}><Timer.Hours /></h2><br/>hours</Grid>
                <Grid item md='3' style={{fontSize: '2em', textAlign:'center'}}><h2 style={{marginBottom:'10px'}}><Timer.Minutes /></h2><br/>minutes</Grid>
                <Grid item md='3' style={{fontSize: '2em', textAlign:'center'}}><h2 style={{marginBottom:'10px'}}><Timer.Seconds /></h2><br/>seconds</Grid>
                </Timer><br/>
                <Grid item md='12' style={{marginTop: '30px', textAlign:'center', fontSize: '1.2em'}}>
                  ...since the last measured violation.
                </Grid>
                </Grid>
                </Box>
            </div>
          </Grid>

          <Grid item md='6'>
            <p style={{padding:'5%'}}>We are pleased to report that during the past year, the water delivered to your home or business met or surpassed state and federal drinking water requirements!</p>
            <div style={{textAlign:'center'}}><CheckCircleTwoToneIcon style={{fontSize: '10em', color: 'rgba(0,175,150,1)'}}/></div>
          </Grid>
        </Grid>
          </FullpageSection>
          <FullpageSection style={{
            backgroundColor: '#F5FFFE',
            padding: '5em',
          }}>
          <Grid container spacing={4}>
          <Grid item md='12'>
            <h1 style={{fontSize:'3em'}}>Would you like to learn more?</h1>
          </Grid>
          <Grid item md='6'>
            <Box className={classes.enhancedBox}>
            <Grid container>
                <Grid item xs={4} style={{paddingLeft:'10%', paddingTop:'5%'}}><User size={64}/></Grid>
                <Grid item xs={8}>
                  <h3 style={{fontSize:'2em'}}>John Doe</h3>
                  <p><b>Tel.</b>: (626) 502-9242</p>
                  <p><b>Email</b>: john.doe@venthic.com</p>
                </Grid>
              </Grid>
              
            </Box><br/>
            <Box className={classes.enhancedBox}>
              <Grid container>
            <Grid item xs={4} style={{paddingLeft:'10%', paddingTop:'5%'}}><Headphones size={64}/></Grid>
                <Grid item xs={8}>
                  <h3 style={{fontSize:'2em'}}>EPA’s Safe Drinking Water Hotline</h3>
                  <p><b>Tel.</b>: 1-800-426-4791</p>
                  <p><b>Web</b>: https://www.epa.gov/safewater</p>
                </Grid>
                </Grid>
            </Box>
          </Grid>
          <Grid item md='6'>
            <Box className={classes.enhancedBox}>
              <h2 style={{fontSize:'2em'}}>Public participation opportunities</h2>
              <p>The regular meetings of the Venthic County Water District Utitlity are held every Tuesday at 9:30 a.m. in the Board’s Hearing Room located at 500 Garden Street, San Diego.  On Tuesdays following a Monday holiday, the meetings begin at 1:00 p.m.</p>
              <p>For questions or comments regarding water quality or this report, please contact Mr. John Doe.</p>
            </Box>
          </Grid>
        </Grid>
          </FullpageSection>
        </FullPageSections>

      </Fullpage>
    )
});

export default CCRReport;