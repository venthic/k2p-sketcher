import {observer} from "mobx-react-lite";
import * as React from "react";
import {StackedNormalizedBarSeries, StackedNormalizedBarChart, Gradient, Bar, GradientStop, RangeLines, multiCategory, schemes} from "reaviz";

import { withStyles } from "@material-ui/styles";
import theme from "../config/Theme.js";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import Tooltip from '@material-ui/core/Tooltip';
import chroma from 'chroma-js';
import Paper from '@material-ui/core/Paper';
import BubbleChartTwoToneIcon from '@material-ui/icons/BubbleChartTwoTone';
import RestorePageTwoToneIcon from '@material-ui/icons/RestorePageTwoTone';
import Button from '@material-ui/core/Button';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PictureAsPdfTwoToneIcon from '@material-ui/icons/PictureAsPdfTwoTone';
import ArrowBackTwoToneIcon from '@material-ui/icons/ArrowBackTwoTone';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: theme.palette.common.white,
    color: 'rgba(0, 0, 0, 0.87)',
    boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
    fontSize: 14,
    fontWeight: 500
  },
}))(Tooltip);


function valueFormat(value) {
  if (value <= 99) {
    return parseInt(value);
  } else if ( value <= 99.9) {
    return value.toFixed(1);
  } else if ( value <= 99.99) {
    return value.toFixed(2);
  } else if ( value <= 99.999) {
    return value.toFixed(3);
  } else {
    return value.toFixed(4);
  }
}

function colorFormat(value) {
  if (value < 1) {
    return '#fe5252';
  } else if ( value < 2) {
    return 'orange';
  } else if ( value < 3) {
    return '#ffcb00';
  } else if ( value < 4) {
    return '#6ec980';
  } else {
    return '#1d9b4e';
  }
}

const LRVDiagram = observer(({context}) => {
  const rx = 0;
  const ry = 0;
  const height = 250;
  const width = 200;
  const rangeWidth = 3;
  const hasGradient = true;
  const hasRangelines = true;
  const rounded = false;
  const color = chroma
    .scale(['#4E71BE', '#DF8344'])
    .correctLightness()
    .colors(2);

  

  const data = (context.calculationData.stackChart ) ? context.calculationData.stackChart.xAxis.categories.map((object, i) => {
    
    return {
      key: context.calculationData.stackChart.xAxis.categories[i],
      data: [ 
        {
            key: "Percent % in Liquid Effluent",
            data: context.calculationData.stackChart.series[0].data[i] 
        },
        {
          key: "Percent % in Sludge/Biosolids",
          data: context.calculationData.stackChart.series[1].data[i] 
        }
      ]
    }
  }): [];

const gradient = hasGradient ? (
  <Gradient
    stops={[
      <GradientStop offset="5%" stopOpacity={0.1} key="start" />,
      <GradientStop offset="90%" stopOpacity={0.7} key="stop" />
    ]}
  />
) : null;

const rangelines = hasRangelines ? (
  <RangeLines position="top" strokeWidth={rangeWidth} />
) : null;

return (

  <div>
    <h4 style={{paddingTop: 0, marginTop: 0}}>Calculation results</h4>
    <div id="bars">
    {/* <ReactEchartsCore
        echarts={echarts}
        option={option}
        notMerge={true}
        style={{height:'230px'}}
        lazyUpdate={true}
        ref={(e) => {context.setBarRef(e)}}
        // opts={{
        //     // Exporting format, can be either png, or jpeg
        //     type: 'png',
        //     // Resolution ratio of exporting image, 1 by default.
        //     pixelRatio: 2,
        //     // Background color of exporting image, use backgroundColor in option by default.
        //     backgroundColor: '#fff',
        // }}
    /> */}

      <StackedNormalizedBarChart
      width={width}
      height={height}
      data={data}
      series={
        <StackedNormalizedBarSeries
          bar={
            <Bar
              key="stacked-normalized-bar"
              rx={rx}
              ry={ry}
              rounded={rounded}
              gradient={gradient}
              rangeLines={rangelines}
            />
          }
          colorScheme={color}
        />
      }
    />
  </div>
  {(context.calculationData.table) ? 
    <div><h4>Predicted Log10 Reduction Values</h4>
        <Table size="small" aria-label="pathogen group table">
            <TableRow>
              <TableCell align="left"><b>Virus</b></TableCell>
              <TableCell align="left" style={{color: colorFormat(context.calculationData.table.values[0])}}>{context.calculationData.table.values[0]}</TableCell>
              <TableCell style={{color: "#787777"}}>
                ({ valueFormat(100*(1 - Math.pow(10, -context.calculationData.table.values[0]))) }%)
              </TableCell>
            </TableRow>
          <TableBody>
              <TableRow>
                <TableCell align="left"><b>Bacteria</b></TableCell>
                <TableCell align="left" style={{color: colorFormat(context.calculationData.table.values[1])}}>{context.calculationData.table.values[1]}</TableCell>
                <TableCell style={{color: "#787777"}}>
                ({ valueFormat(100*(1 - Math.pow(10, -context.calculationData.table.values[1]))) }%)
              </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="left"><b>Protozoa</b></TableCell>
                <TableCell align="left" style={{color: colorFormat(context.calculationData.table.values[2])}}>{context.calculationData.table.values[2]}</TableCell>
                <TableCell style={{color: "#787777"}}>
                ({ valueFormat(100*(1 - Math.pow(10, -context.calculationData.table.values[2]))) }%)
              </TableCell>
              </TableRow>
              <TableRow>
                <TableCell align="left"><b>Helminths</b></TableCell> 
                <TableCell align="left" style={{color: colorFormat(context.calculationData.table.values[3])}}>{context.calculationData.table.values[3]}</TableCell>
                <TableCell style={{color: "#787777"}}>
                ({ valueFormat(100*(1 - Math.pow(10, -context.calculationData.table.values[3]))) }%)
              </TableCell>
              </TableRow>
          </TableBody>
        </Table>
        <div style={{textAlign:'center', marginTop: '20px' }}>
        <LightTooltip title='Click here to see the predicted pathogen flow chart. The width of the flow bars are proportional to the predicted number of pathogens remaining at each point in the system.'>
              <Button style={{color: 'rgb(66, 145, 158)'}} startIcon={<BubbleChartTwoToneIcon />} variant="primary" aria-label="view chart" component="span" onClick={() => {context.setPathogenType(0); context.setSankeyModalState(true);}}>
                Pathogen Flow chart
              </Button>
              </LightTooltip></div>
        </div> : null
    }
    <br/>
    <ButtonGroup>
      <Button variant='outlined' startIcon={<ArrowBackTwoToneIcon />} onClick={() => context.toggleCalculationComplete()}>Back to Sketch</Button>
      <Button startIcon={<PictureAsPdfTwoToneIcon/>} variant="outlined" color="secondary" aria-label="Generate Report" onClick={()=> context.setGenerateReportModalState(true)} disabled={!context.calculationComplete}>          
                            Generate Report
                        </Button>
    </ButtonGroup> 
    </div>
); 
});


export default LRVDiagram;
