import { observer } from "mobx-react-lite";
import { makeStyles } from '@material-ui/core/styles';
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React from "react";
import styled from "@emotion/styled";
import Dialog from "@material-ui/core/Dialog/Dialog";
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import GrainTwoToneIcon from '@material-ui/icons/GrainTwoTone';
import GetAppRoundedIcon from '@material-ui/icons/GetAppRounded';

const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
      maxWidth: 752,
    },
    demo: {
      backgroundColor: theme.palette.background.paper,
    },
    title: {
      margin: theme.spacing(4, 0, 2),
    },
    large: {
      width: theme.spacing(7),
      height: theme.spacing(7),
    },
  }));

const ImportModal = observer(({ context }) => {
    const graph = context.graph;
    const classes = useStyles();

    return (
        <div>
        <Dialog
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={context.importModalState}
            onClose={(e) => {
                context.setImportModalState(false)
            }}
            maxWidth='lg'
            fullWidth
            onEnter={()=>context.fetchCatalogueSketches()}
        >
            <DialogTitle>
                <center>Import a sketch</center>
            </DialogTitle>
            
                <DialogContent >
                    <Grid container spacing={3}>
                        <Grid item xs={12} md={8} lg={8} style={{borderRight:'1px solid #ccc'}}>
                            
                            {(context.catalogueSketches.length > 0) ?
                            <div>
                                <Typography variant="h6" className={classes.title}>
                                  Use templates from the K2P Database
                                </Typography>
                                    <div className={classes.demo}>
                                        <List>
                                            {context.catalogueSketches.map((sketch) =>
                                            <ListItem>
                                                <ListItemAvatar>
                                                    {(sketch.resources.filter(res => res.mimetype.includes('image')).length > 0) ?
                                                <Avatar className={classes.large} src={sketch.resources.filter(res => res.mimetype.includes('image'))[0].url} />:
                                                <Avatar>
                                                    <GrainTwoToneIcon />
                                                </Avatar>
                                                }
                                                </ListItemAvatar>
                                                <ListItemText
                                                primary={sketch.title}
                                                secondary={sketch.notes ? sketch.notes : null}
                                                />
                                                <ListItemSecondaryAction>
                                                <IconButton edge="end" aria-label="load" onClick={()=>{context.fetchSketch(sketch.resources.filter(res => res.mimetype=='application/json')[0].url)}}>
                                                    <GetAppRoundedIcon />
                                                </IconButton>
                                                </ListItemSecondaryAction>
                                            </ListItem>,
                                            )}
                                        </List>
                                    </div>
                                </div> : <div>Loading...</div>
                        }
                    
                        </Grid>
                        <Grid item xs={12} md={4} lg={4}>
                        <Typography variant="h6" className={classes.title}>
                                Upload sketch file
                            </Typography>
                        <div style={{height:'250px', display: ((graph.calculationComplete) ? 'none': 'block'), backgroundColor:'#ddd', borderRadius:'5px', marginTop: '50px', textAlign:'center'}}>
                            
                        
                            
                            <PostFormLabel>
                            <input 
                                accept=".json" 
                                id="raised-button-file" 
                                multiple 
                                type="file" 
                                style={{marginTop: '100px'}}
                                onChange={(e)=>context.handleFile(e.target.files[0])}
                                /> 
                                <label htmlFor="raised-button-file"> 
                                </label> 
                            </PostFormLabel>
                        </div>
                        </Grid>
                    </Grid>
                </DialogContent>
            
            <DialogActions>
                <Button onClick={() => {
                    context.setImportModalState(false)
                }} color="primary">
                    Cancel
                    </Button>
                
            </DialogActions>
        </Dialog>
        
        </div>
    )
});

const PostFormLabel = styled.label`
    font-family: 'Cabin', sans-serif;
    display: block;
    margin-bottom: 20px !important;
    font-size: 16px;
    font-weight: 400;
`;

export default ImportModal;
