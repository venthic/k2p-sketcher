import React from "react";
import { pdf , Page, Text, View, Document, StyleSheet, Image, Font } from '@react-pdf/renderer';
import styled from '@react-pdf/styled-components';
import {subTypes} from '../config/node-config';
import cover from "../static/img/cover.jpg";
import qr from "../static/img/qr.png";
import gwpp from "../static/img/logo_dark_blue.png";
import cabin from "../static/fonts/cabin-v14-latin-regular.woff";
import { SOURCE_TYPE } from "../config/graph-config.js";


Font.register({ family: 'Cabin', src: cabin });

const styles = StyleSheet.create({
    page: {
      flexDirection: 'column',
      backgroundColor: 'white',
      fontFamily: 'Cabin'
    },
    footer: {
        backgroundColor: '#a0a0a0',
        width: '100%',
        display:'block',
        padding: '15px',
        textAlign: 'center',
        color: '#fff',
        fontSize:15
      },
    pageNumber: {
        backgroundColor: '#003347',
        width: '120px',
        display:'block',
        padding: '15px',
        textAlign: 'center',
        color: '#fff',
          fontSize:15
      },
      section: {
        marginTop: 20,
        marginBottom: 20,
        marginLeft: 24,
        paddingLeft: 24,
        marginRight: 20,
        paddingRight: 20,
        paddingBottom: 20,
        flexGrow: 1,
        fontFamily: 'Cabin'
      },
  	whiteLine: {
      display:'block',
      width:'50px',
      height: '7px',
      marginTop: '15px',
      backgroundColor: '#fff'
    },
    cover:{
        minWidth: '100%',
        minHeight: '100%',
        height: '100%',
        width: '100%',
        objectFit: 'cover'
    },
    coverHolder: {
      position:'absolute',
      top: '150px',
      backgroundColor: '#003347',
      padding: 40,
      opacity: 0.9
    },
    coverTitle: {
        zIndex:2000,
        fontSize:42,
      	color: '#fff',
    },
  	coverSubtitle: {
        fontSize:32,
      	color: '#fff',
    	marginTop: 10
    },
    coverMeta: {
        fontSize:20,
      	color: '#fff',
    	marginTop: 120
    },
    coverSubMeta: {
        fontSize:14,
      	color: '#fff',
    marginTop:10
    },
  	coverFooter: {
      position:'absolute',
      bottom: '10px',
    	left: '10px'
    },
    qr: {
        height: '100px',
        width: '100px',
        bottom:'10px',
        position:'absolute'
      },
      gwpp: {
        height: '80px',
        left: '120px',
        bottom:'15px'
      },
    image2:{
        width: 250,
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    image:{
        width: 400,
        padding: 20,
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    imageFlow:{
        width: 500,
        padding: 20,
        marginRight: 'auto',
        marginLeft: 'auto'
    },
    table: { 
        marginRight: 10,
        marginLeft:10,
        marginBottom:10,
        borderStyle: "solid", 
        borderWidth: 1, 
        borderRightWidth: 0, 
        borderBottomWidth: 0 
    },
    tableRow: { 
        margin: "auto", 
        flexDirection: "row" 
    },
    tableCol: { 
        borderStyle: "solid", 
        borderWidth: 1, 
        borderLeftWidth: 0, 
        borderTopWidth: 0 
    },
    tableCell: { 
        margin: "auto", 
        padding: 5, 
        fontSize: 8 
    },
    caption: {
        fontSize: 10,
        fontFamily: 'Cabin',
        marginTop: "10px",   
    }
});

const Title = styled.Text`
    margin: 20px;
    font-size: 22px;
    font-family: 'Helvetica';
`;

const Subtitle = styled.Text`
    margin: 20px;
    font-size: 10px;
    font-family: 'Helvetica';
`;

const Header = styled.Text`
    margin: 20px;
    font-size: 18px;
    font-family: 'Helvetica';
`;

const Paragraph = styled.Text`
    margin: 20px;
    font-size: 12px;
    font-family: 'Helvetica';
`;

const Caption = styled.Text`
    font-size: 12px;
    font-family: 'Helvetica';
`;

const Subcaption = styled.Text`
    font-size: 8px;
    font-family: 'Helvetica';
`;

const saveBlob = (blob, filename) => {
    var a = document.createElement("a");
    document.body.appendChild(a);
    a.style.display = "none";
    let url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = filename;
    a.click();
    window.URL.revokeObjectURL(url);
};
  
const savePdf = async (document, filename) => {
    saveBlob(await pdf(document).toBlob(), filename);
};

const calculateRate = (nodes) => {
    if (nodes.length > 0) {
        var rate = 0;
        for (var i = 0; i < nodes.length; i++ ) {
            rate += parseInt(nodes[i].payload["flowRate"]); 
        }
        return rate;
    }
    return -1;
}

const getCurrentDate = () => {
    const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    const d = new Date();
    return "Created on: "+monthNames[d.getMonth()]+" "+d.getDate()+", "+d.getFullYear();
}


const MyDocument  = (report, imageUrl, nodes, urlBars, flowUrls, predictedTable) => {
    const sewageFlowRate = calculateRate(nodes.filter(node => node.type === SOURCE_TYPE && node.payload.subType=== 'sewerage'));
    const fecalFlowRate = calculateRate(nodes.filter(node => node.type === SOURCE_TYPE && node.payload.subType=== 'fecal sludge'));

    const figureNo = (report.photo !== '') ? 1 : 0;

    return <Document>
        
        <Page size="A4" wrap={true} style={styles.page}>
        
        <View>
              
              <Image
                  style={styles.cover}
                  src={cover}
              />
            <View style={styles.coverHolder}>
              <Text style={styles.coverTitle}>
                      Pathogen Flow Report
              </Text>
              <View style={styles.whiteLine}></View>
                <View><Text style={styles.coverSubtitle}>{report.name}</Text></View>
                <View style={styles.coverMeta}>
                    
                <Text>{report.authorName}</Text>
                <Text style={styles.coverSubMeta}>{report.organization}</Text>
                <Text style={styles.coverSubMeta}>{report.city}, {report.country}</Text>
                <Text style={styles.coverSubMeta}>{report.email}</Text>
                {(report.phone && report.phone !== '') ? <Text style={styles.coverSubMeta}>{report.phone}</Text>:<Text></Text> }
                
              </View>
            </View>
            <View style={styles.coverFooter}>
              <Image
                  style={styles.qr}
                  src={qr}
              />
              <Image
                    style={styles.gwpp}
                    src={gwpp}
                />
            </View>
          </View>
            
        </Page>
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
                <Text style={{textAlign: 'center', marginTop:'10px', fontFamily: 'Cabin'}}>
                    <Title style={{fontFamily: 'Cabin'}}>Pathogen Flow Report: {report.name}</Title>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Subtitle style={{fontFamily: 'Cabin'}}>Prepared by {report.authorName} using the Treatment Plant Sketcher Tool</Subtitle>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Subtitle style={{fontFamily: 'Cabin'}}>{getCurrentDate()}</Subtitle>
                </Text>
                
                <Text style={{paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Introduction</Header>
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                    This report presents a summary of the anticipated bacterial, viral, protozoan and helminth log reductions achieved through
                    the {report.name} which is {report.existing} that {report.existing.includes('existing') ? <Text>serves</Text>:<Text>would serve</Text>} a population of {report.population} and {report.existing.includes('existing') ? <Text>treats </Text>:<Text>would treat </Text>} 
                    an average daily flow rate of { (sewageFlowRate !== -1) && <Text>{sewageFlowRate} m3/day of sewage</Text>}{(fecalFlowRate !== -1  && fecalFlowRate) && <Text> and</Text>} {(fecalFlowRate !== -1) && <Text>{fecalFlowRate} m3/day of fecal sludge</Text>}, with a maximum capacity of { (report.capacitySewage) && <Text>{report.capacitySewage} m3/day of sewage</Text>}{(report.capacitySewage && report.capacityFecal) && <Text> and</Text>} {(report.capacityFecal) && <Text>{report.capacityFecal} m3/day of fecal sludge</Text>}. 
                    </Paragraph>   
                    
                </Text>
                
                {(report.systemDetails && report.systemDetails !== '') ?
                        (<Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                            <Paragraph style={{fontFamily: 'Cabin'}}>
                                {report.systemDetails}
                            </Paragraph>
                        </Text>):
                        <Text></Text>
                }
                <Text style={{paddingTop: '20px', paddingLeft: '0px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Objective</Header>
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                    The objective of this analysis is to estimate the overall reduction of pathogens (viruses, bacteria, protozoa, and helminth eggs) in the {report.name} using the K2P Treatment Plant Sketcher Tool. 
                    </Paragraph>
                </Text>
                <Text style={{paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Treatment Plant Configuration & Design</Header>
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                    {/* The [xxxxx treatment plant  ] treats [sewage and/or fecal sludge] using the following technologies and treatment unit processes: [anaerobic pond, facultative pond, settler/sedimentation, sludge drying bed]. Figure 1 shows a photo of the system.  */}
                        The {report.name} treats {findSources(nodes)} using the following technologies: {findUnitProcess(nodes)}.
                    </Paragraph>
                </Text>
                {(report.photo !== '') && [
                    
                    <Text style={{paddingTop: '10px', marginTop:'10px'}}>
                        <Caption style={styles.caption}>Figure 1. Photograph of the treatment plant (photo credit: {report.photoCredit}).</Caption>   
                    </Text>,
                    <View style={styles.image}>
                        <Image src={report.photo}></Image>
                    </View>
                    ]
                }
                </View>
                </Page>
                <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
                
                <Text style={{paddingTop: '10px', marginTop:'10px'}}>
                    <Caption style={styles.caption}>Figure {figureNo+1}. Sketch showing the configuration of the treatment plant.</Caption>      
                </Text>
                <View style={styles.image}>
                    <Image src={imageUrl}></Image>
                </View>
                
                <Text>
                    <Caption style={styles.caption}>Table 1. Design, operational, and environmental factors associated with each of the unit processes in the treatment plant.</Caption>      
                </Text>
                <View style={[styles.table, {marginTop: '10px'}]}> 
                    {/* TableHeader */} 
                    <View style={styles.tableRow}>
                        <View  style={[styles.tableCol,{width:'5%'}]}> 
                            <Text style={styles.tableCell}>ID</Text> 
                        </View>
                        <View  style={[styles.tableCol,{width:'30%'}]}>  
                            <Text style={styles.tableCell}>Unit Process</Text> 
                        </View> 
                        <View  style={[styles.tableCol,{width:'25%'}]}> 
                            <Text style={styles.tableCell}>Factor 1</Text> 
                        </View> 
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>Factor 2</Text> 
                        </View> 
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>Factor 3</Text> 
                        </View> 
                    </View> 
                    {/* TableContent */}
                    {Table1Rows(nodes)}
                </View>
                
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        <Page size="A4" wrap style={styles.page}>
            <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
                `${pageNumber}`
            )} fixed />
            <View style={styles.section}>
                <Text style={{paddingTop: '10px', marginTop:'10px', fontFamily: 'Cabin' }}>
                    <Header style={{fontFamily: 'Cabin'}}>Predicted Pathogen Reduction</Header>
                </Text>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                        The reduction of pathogens from the wastewater and the overall reduction of pathogens throughout the treatment system are shown in Table 2. Figure 2 
                        shows the breakdown of pathogens leaving the system in the liquid effluent compared to the sludge/biosolids.
                    </Paragraph>
                </Text>
                <Text style={{paddingTop: '10px', marginTop:'10px', fontFamily: 'Cabin'}}>
                    <Caption style={styles.caption}>Table 2. Summary of the estimated log10 reduction of pathogens in the system.</Caption>      
                </Text>
                
                <View style={[styles.table, {marginTop: '10px'}]}> 

                    <View style={styles.tableRow}>
                        <View  style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}></Text> 
                        </View>
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>{predictedTable.labels[0]}</Text> 
                        </View> 
                        <View  style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>{predictedTable.labels[1]}</Text> 
                        </View> 
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>{predictedTable.labels[2]}</Text> 
                        </View> 
                        <View  style={[styles.tableCol,{width:'20%'}]}>  
                            <Text style={styles.tableCell}>{predictedTable.labels[3]}</Text> 
                        </View> 
                    </View> 
                    { (report.virusRemoval !== '' || report.bacteriaRemoval !== '' || report.protozoaRemoval !== '' || report.helminthRemoval !== '') &&
                    <View style={styles.tableRow}> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>Measured log10 reduction (laboratory analysis)</Text> 
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            {(report.virusName !== '' && report.virusRemoval !== '') ? 
                                [<Text style={styles.tableCell}>
                                    {-Math.log10(1 - parseFloat(report.virusRemoval)/100).toFixed(1)} ({report.virusRemoval}%)
                                </Text>,
                                <Text style={{margin: "auto", fontSize: 8 }}>
                                    {report.virusName}
                                </Text>
                                ] 
                                :
                                <Text style={styles.tableCell}>
                                    N/A
                                </Text> 
                            }
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            {(report.bacteriaRemoval !== '') ? 
                                [<Text style={styles.tableCell}>
                                    {-Math.log10(1 - parseFloat(report.bacteriaRemoval)/100).toFixed(1)} ({report.bacteriaRemoval}%)
                                </Text>,
                                <Text style={{margin: "auto", fontSize: 8 }}>
                                    {report.bacteriaName}
                                </Text>
                                ] 
                                :
                                <Text style={styles.tableCell}>
                                    N/A
                                </Text> 
                            }
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            {(report.protozoaName !== '' && report.protozoaRemoval !== '') ? 
                                [<Text style={styles.tableCell}>
                                    {-Math.log10(1 - parseFloat(report.protozoaRemoval)/100).toFixed(1)} ({report.protozoaRemoval}%)
                                </Text>,
                                <Text style={{margin: "auto", fontSize: 8 }}>{report.protozoaName}</Text>] 
                                :
                                <Text style={styles.tableCell}>
                                    N/A
                                </Text> 
                            } 
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            {(report.helminthName !== '' && report.helminthRemoval !== '') ? 
                                [<Text style={styles.tableCell}>
                                    {-Math.log10(1 - parseFloat(report.helminthRemoval)/100).toFixed(1)} ({report.helminthRemoval}%) 
                                </Text>, 
                                <Text style={{margin: "auto", fontSize: 8 }}>
                                    {report.helminthName}
                                </Text>]
                                :
                                <Text style={styles.tableCell}>
                                    N/A
                                </Text> 
                            }
                        </View> 
                    </View> 
                    }
                    <View style={styles.tableRow}> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>Predicted log10 reduction values</Text> 
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>{predictedTable.values[0]}</Text> 
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>{predictedTable.values[1]} </Text> 
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>{predictedTable.values[2]}</Text> 
                        </View> 
                        <View style={[styles.tableCol,{width:'20%'}]}> 
                            <Text style={styles.tableCell}>{predictedTable.values[3]}</Text> 
                        </View> 
                    </View> 
                </View>
                
                <Text style={{paddingBottom: '10px', marginBottom:'10px'}}>
                    <Caption style={styles.caption}>Figure {figureNo+2}. Estimated proportion (percentage) of pathogens discharged in 
                    the liquid effluent vs. the sludge/biosolids from the treatment plant. (Blue: Percent of pathogens discharged 
                    in liquid effluent; Orange: Percent of pathogens discharged in sludge/biosolids).</Caption>      
                </Text>
                <View style={styles.image2}>
                    <Image src={urlBars}></Image>
                </View>
                <Text style={{paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                        {report.resultDetails}
                    </Paragraph>
                </Text>
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
                <Text style={{paddingTop: '10px', marginTop:'10px'}}>
                    <Caption style={styles.caption}>Figure {figureNo+3}. Pathogen flows showing the estimated percentage of pathogens moving through the treatment plant.</Caption>      
                </Text>
                <View style={[styles.imageFlow, {paddingTop: '10px', textAlign:'center'}]}>
                    <Image style={{margin:'auto'}} src={flowUrls[0]}></Image>
                </View>
                <View style={[styles.imageFlow, {paddingTop: '10px', textAlign:'center'}]}>
                    <Image style={{margin:'auto'}} src={flowUrls[1]}></Image>
                </View>
                <View style={[styles.imageFlow, {paddingTop: '10px', textAlign:'center'}]}>
                    <Image style={{margin:'auto'}} src={flowUrls[2]}></Image>
                </View>
                <View style={[styles.imageFlow, {paddingTop: '10px', textAlign:'center'}]}>
                    <Image style={{margin:'auto'}} src={flowUrls[3]}></Image>
                </View>
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
        `${pageNumber}`
        )} fixed />
            <View style={styles.section}>
                <Header style={{fontFamily: 'Cabin'}}>Conclusions and Recommendations</Header>
            
                <Text style={{ fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>
                        {report.conclusion}
                    </Paragraph>
                </Text>
                
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
        
            
        
        <Page size="A4" wrap style={styles.page}>
        <Text style={styles.pageNumber} render={({ pageNumber, totalPages }) => (
        `${pageNumber}`
      )} fixed />
            <View style={styles.section}>
            <Text style={{textAlign: 'center', marginTop:'10px', fontFamily: 'Cabin'}}>
                    <Title style={{fontFamily: 'Cabin'}}>Contact us!</Title>
                </Text>
                <Text style={{textAlign: 'center', paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Case Study Lead Author</Header>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '10px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>{report.authorName}</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>{report.organization}</Paragraph>
                </Text>
                {(report.state && report.state !== '') ?
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>{report.street}</Paragraph>
                </Text>:<Text></Text>}
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>{report.city}, {(report.state && report.state !== '') ? <Text>{report.state}, </Text>:<Text></Text>}{report.country}</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>{report.email}</Paragraph>
                </Text>
                {(report.phone && report.phone !== '') ?
                    <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                        <Paragraph style={{fontFamily: 'Cabin'}}>{report.phone}</Paragraph>
                    </Text>:<Text></Text>
                }
               

                <Text style={{textAlign: 'center', paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Header style={{fontFamily: 'Cabin'}}>Treatment Plant Sketcher Tool</Header>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '20px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>Matthew E. Verbyla, Ph.D.</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>Department of Civil, Construction, and Environmental Engineering</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>San Diego State University</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>San Diego, California, USA</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>mverbyla@sdsu.edu</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>+1 (619) 594-0711</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '50px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>Panagis Katsivelis</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>Venthic Technologies</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>Athens, Greece</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>www.venthic.com</Paragraph>
                </Text>
                <Text style={{textAlign: 'center',paddingTop: '5px', fontFamily: 'Cabin'}}>
                    <Paragraph style={{fontFamily: 'Cabin'}}>panagis.katsivelis@venthic.com</Paragraph>
                </Text>
                <Text style={{paddingTop: '120px', fontFamily: 'Cabin'}}>
                    <Subcaption style={{fontFamily: 'Cabin'}}>The reduction of pathogens from wastewater or fecal sludge reported here, as well as the distribution of pathogens to the liquid effluent vs. 
                        the sludge/biosolids, is estimated using the Pathogen Flow Model (Musaazi et al. 2020). Pathogen removal is calculated based on 
                        differences in the estimated loadings in the sewage influent and in the liquid effluent only. The overall reduction of pathogens is calculated
                        based on differences in estimated loadings entering the system (in sewage and/or fecal sludge) and leaving the system (in liquid effluent
                        and treated sludge/biosolids). More details about the Tool can be found on our  website (https://www.waterpathogens.org/tools/treatment-plant-sketcher-tool).
                    </Subcaption>
                </Text>
            </View>
            <View style={styles.footer} fixed><Text>www.waterpathogens.org</Text></View>
        </Page>
    </Document>
}

const ReportPDF = (report,imageUrl, nodes, urlBars, predictedTable, flowCharts, setButton) => {
    savePdf(MyDocument(report, imageUrl, nodes, urlBars, predictedTable, flowCharts), "report.pdf").then(() => setButton())
}
const Table1Rows = (nodes) =>{
    let rows =nodes.sort(function compare (a,b) { return (a.id > b.id) ? 1:-1 }).map( node =>{
        if (node.type === "unitProcess"){
            return(
                <View style={styles.tableRow}> 
                    <View style={[styles.tableCol,{width:'5%'}]}> 
                        <Text style={styles.tableCell}>{node.id}</Text> 
                    </View> 
                    <View style={[styles.tableCol,{width:'30%'}]}> 
                        <Text style={styles.tableCell}>{node.payload.subType}</Text> 
                    </View> 
                    <View style={[styles.tableCol,{width:'25%'}]}> 
                        <Text style={styles.tableCell}>{findFactor(1, node)} </Text> 
                    </View> 
                    <View style={[styles.tableCol,{width:'20%'}]}> 
                        <Text style={styles.tableCell}>{findFactor(2, node)}</Text> 
                    </View> 
                    <View style={[styles.tableCol,{width:'20%'}]}> 
                        <Text style={styles.tableCell}>{findFactor(3, node)}</Text> 
                    </View> 
                </View> 
            )
        }
        return;
    })
    return rows;
}

const findFactor = (numOfFactor, node) => {
    let  factors = subTypes.filter(type => type.machineName === node.payload.subType)[0].fields.map((field) => {
        if (field.conditional){
            return field.label+": "+node.payload[field.propName];
        }
        return 'N/A';
    })
    if (numOfFactor > factors.length) return 'N/A'
    return factors[numOfFactor-1];
}

const findSources = (nodes) =>{
    let sewerage = false;
    let fecalSludge = false;
    for (var i =0; i < nodes.length; i++){
        var node = nodes[i]
        if (node.type === "source"){
            if(node.payload.subType === "sewerage")
                sewerage = true;
            if(node.payload.subType === "fecal sludge")
                fecalSludge = true;
        }
    }
    if (sewerage && fecalSludge) return "sewage and fecal sludge";
    else if (sewerage) return "sewage";
    else if (fecalSludge) return "fecal sludge";

    return "undefined";
}

const findUnitProcess = (nodes) =>{
    let flattened = nodes.filter(node => node.type === "unitProcess").reduce((total, currentValue) => total.concat(currentValue.payload.subType), []);
    return flattened.join(", ");
}

export default ReportPDF;