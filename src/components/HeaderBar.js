import AppBar from "@material-ui/core/AppBar/AppBar";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import Tooltip from "@material-ui/core/Tooltip";
import Grid from "@material-ui/core/Grid/Grid";
import GrainTwoToneIcon from '@material-ui/icons/GrainTwoTone';
import Link from '@material-ui/core/Link';
import React from "react";
import logo from '../static/img/logo_dark_blue.png';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import ButtonGroup from '@material-ui/core/ButtonGroup';
import PublishTwoToneIcon from '@material-ui/icons/PublishTwoTone';
import Button from "@material-ui/core/Button/Button";
import GetAppTwoToneIcon from '@material-ui/icons/GetAppTwoTone';
import FolderOpenTwoToneIcon from '@material-ui/icons/FolderOpenTwoTone';
import { observer } from "mobx-react-lite";
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
/*import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';*/
import IconButton from '@material-ui/core/IconButton';
import HelpTwoToneIcon from '@material-ui/icons/HelpTwoTone';
import PhotoCameraTwoToneIcon from '@material-ui/icons/PhotoCameraTwoTone';
import Typography from '@material-ui/core/Typography';
/*import GraphConfig, {
    UNIT_PROCESS_TYPE,
    SOURCE_TYPE,
    END_USE_TYPE,
    NODE_KEY
} from '../config/graph-config';*/

const useStyles = makeStyles(theme => ({
    root: {
      padding: theme.spacing(1, 2),
    },
    link: {
      display: 'flex',
      marginTop: '5px'
    },
    k2pLink: {
        fontWeight: '800',
        fontFamily: '"Roboto", sans-serif',
        color: '#003347',
        borderLeft: '1px dotted #ccc',
        paddingLeft: '20px',
      },
    icon: {
      marginRight: theme.spacing(0.5),
      width: 20,
      height: 20,
    },
  }));

  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const HeaderBar = observer(({ context, nodes }) => {
    const classes = useStyles();
    const graph = context; 

    return <AppBar position="static" style={{zIndex:1201}} color="default">
        <Toolbar>
            <Grid container>
                <Grid item xs={6} style={{paddingTop: '10px'}}>
                    <img src={logo} style={{float:'left', paddingRight:'20px'}} width={100}/>
                    <Breadcrumbs aria-label="breadcrumb" style={{float:'left'}}>
                    
                    <Link color="inherit" href="#" className={classes.link}>
                    <GrainTwoToneIcon className={classes.icon} />
                        Treatment Plant Sketcher Tool
                    </Link>
                </Breadcrumbs>
                </Grid>
                <Grid item xs={6} >
                    <ButtonGroup aria-label="Publishing options" size='small' style={{float:'right', paddingTop:'12px'}}>
                        <LightTooltip title='Click here to upload a previously created sketch in the format of a JSON file.'>
                        <Button id="importButton" style={{ 
                                    color: '#fff',
                                    backgroundColor: 'rgb(56, 194, 122)',
                                    width: '120px'
                                }}
                                onClick={(e) => {context.setImportModalState(true)}}
                                startIcon={<FolderOpenTwoToneIcon fontSize="small"/>}>
                             Import
                        </Button>
                        </LightTooltip>
                        <LightTooltip title='Click here to save your sketch file and its associated data as a JSON file so it can be used later and retrieved using the import button.'>
                        <Button style={{ 
                                    color: '#fff',
                                    backgroundColor: 'rgb(24, 150, 85)',
                                    width: '120px'
                                }}
                                onClick={(e) => {context.download()}}
                                startIcon={<GetAppTwoToneIcon fontSize="small"/>}>Save
                        </Button>
                        </LightTooltip>
                        <LightTooltip title='Click here to submit your sketched treatment system to the K2P database; note that anything published here will be submitted to the K2P Data Portal (data.waterpathogens.org) where it will be accessible to anyone, and can be viewed, curated, and re-used by others.'>
                        <Button id='publishButton' size='small' style={{
                                    backgroundColor:'rgb(13, 132, 70)',
                                    color: '#fff',
                                    width: '120px'
                                }} 
                                onClick={(e)=>{(context.isValid()) && context.setCatalogueModalState(true)}}
                                startIcon={<PublishTwoToneIcon fontSize="small"/>}>Publish
                        </Button>
                        </LightTooltip>
                    </ButtonGroup>
                    <div style={{float: 'right'}}>
                        {/*<FormControl style={{width: '130px'}}>
                            <InputLabel id="pan-label">Pan to Node</InputLabel>
                            <Select
                                labelId="pan-label"
                                id="pan-select"
                                value={(graph.selectedNode != undefined) ? graph.selectedNode[NODE_KEY]: ''}
                                onChange={graph.onSelectPanNode}
                                style={{ width: '100%'}}
                            >
                            <MenuItem value="">
                                <em>None</em>
                            </MenuItem>
                            {nodes.map(node => (
                                <MenuItem value={node[NODE_KEY]}>{node.title}</MenuItem>
                            ))}
                            </Select>
                            </FormControl>*/}
                            <div style={{marginRight:'20px'}}>
                            <LightTooltip title='Click here to take a screenshot of only the sketched diagram to download and save it.'>
                                <IconButton id="imageButton" style={{paddingTop:'17px', paddingLeft:'0px'}} color="secondary" aria-label="Screenshot" onClick={graph.takeScreenshot}>
                            <PhotoCameraTwoToneIcon size='large'/>
                        </IconButton>
                        </LightTooltip>

                        <Button style={{height:'30px'}} startIcon={<HelpTwoToneIcon/>} variant="outlined" color="secondary" aria-label="More Information" onClick={()=> graph.setInfoState(true)}>          
                            Help
                        </Button>
                        </div>
                        
                    </div>
                </Grid>
            </Grid>
        </Toolbar>
    </AppBar>
});

export default HeaderBar;