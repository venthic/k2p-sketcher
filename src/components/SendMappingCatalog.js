import { observer } from "mobx-react-lite";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import TextField from "@material-ui/core/TextField/TextField";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";
import React from "react";
import styled from "@emotion/styled";
import Dialog from "@material-ui/core/Dialog/Dialog";
import Checkbox from "@material-ui/core/Checkbox/Checkbox";
import FormControl from "@material-ui/core/FormControl/FormControl";
import FormControlLabel from "@material-ui/core/FormControlLabel/FormControlLabel";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Select from "@material-ui/core/Select/Select";
import Typography from "@material-ui/core/Typography/Typography";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import ChipInput from 'material-ui-chip-input';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import GraphConfig, {
    NODE_KEY
} from '../config/graph-config';

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

const SendMappingCatalog = observer(({ context }) => {
    const graph = context.graph;

    return (
        <div>
        <Dialog
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={context.sendToCatalogueModal}
            onClose={(e) => {
                context.setCatalogueModalState(false)
            }}
        >
            <DialogTitle>
                <center>Submit to the K2P Database</center>
            </DialogTitle>
            {context.isSendingToCatalogue &&
                <DialogContent>
                    Sending files to Catalogue...
                </DialogContent>
            }
            {!context.isSendingToCatalogue &&
                <DialogContent>
                    <PostFormLabel>
                        <PostFormInput label="Title" name="title" type="text" value={context.title} variant="outlined"
                            onChange={(e) => {
                                context.editTitle(e.target.value)
                            }} onKeyPress={e => {
                                if (e.key === 'Enter') e.preventDefault();
                            }} />
                    </PostFormLabel>
                    < PostFormLabel>

                        <TextField style={{ width: '100%' }} label="Description of the system" multiline rowsMax="4"
                            value={context.description} onChange={(e) => {
                                context.editDescription(e.target.value)
                            }} variant="outlined" />
                    </PostFormLabel>
                    <PostFormLabel>
                        <ChipInput
                            defaultValue={[]}
                            label="Keywords"
                            variant='outlined'
                            style={{ width: '100%' }}
                            placeholder="Type a keyword and press 'Enter'"
                            onChange={(chips) => context.setKeywords(chips)}
                        />
                    </PostFormLabel>
                    {/* <PostFormLabel>
                        <FormControlLabel
                            value={context.extraNodes}
                            control={<Checkbox color="primary" onChange={() => context.toggleExtraNodes()} />}
                            label="I have data on the performance of this system (coliforms / E. coli / pathogens)"
                            labelPlacement="end"
                        />
                    </PostFormLabel>
                    <PostFormLabel>
                        {(context.extraNodes) ? <div>
                            <span><FormControl variant="filled" style={{ width: '45%' }}>

                                <InputLabel id="influent-label">Influent node</InputLabel>
                                <Select
                                    labelId="influent-label"
                                    id="influent"
                                    value={context.influent}
                                    onChange={context.setInfluent}
                                >
                                    <MenuItem value="" ><em>None</em></MenuItem>
                                    {graph.nodes.map(node => (
                                        <MenuItem value={node[NODE_KEY]}>
                                            {node.title}
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>
                            </span><span>
                                <FormControl variant="filled" style={{ width: '45%', float: 'right' }}>

                                    <InputLabel id="effluent-label">Effluent node</InputLabel>
                                    <Select
                                        labelId="effluent-label"
                                        id="effluent"
                                        value={context.effluent}
                                        onChange={context.setEffluent}
                                    >
                                        <MenuItem value="" ><em>None</em></MenuItem>
                                        {graph.nodes.map(node => (
                                            <MenuItem value={node[NODE_KEY]}>
                                                {node.title}
                                            </MenuItem>
                                        ))}
                                    </Select>
                                </FormControl>
                            </span>
                        </div> : null
                        }
                    </PostFormLabel> */}
                </DialogContent>
            }
            <DialogActions>
                <Button onClick={() => {
                    context.setCatalogueModalState(false)
                }} color="primary">
                    Cancel
                    </Button>
                <Button onClick={() => {
                    context.postData()
                }} color="primary" autoFocus>
                    Save
                    </Button>
            </DialogActions>
        </Dialog>
        <Snackbar
        autoHideDuration={6000} 
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          open={context.uploadResponse == 'ok'}
          onClose={()=>context.setUploadResponse('')}
        >
          <Alert severity="success">
            <p>
              The sketch has been uploaded successfully!
            </p>
          </Alert>
          </Snackbar>
          <Snackbar
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          autoHideDuration={6000} 
          open={context.uploadResponse == 'error'}
          onClose={()=>context.setUploadResponse('')}
        >
          <Alert severity="error">
            <p>
              An unexpected error occurred.
            </p>
          </Alert>
          </Snackbar>
        </div>
    )
});

const PostFormLabel = styled.label`
    font-family: 'Roboto', sans-serif;
    display: block;
    margin-bottom: 20px !important;
    font-size: 16px;
    font-weight: 400;
`;

const PostFormInput = styled(TextField)`
    width: 270px;
    height: 20px;
    margin-bottom:20px;
`;

export default SendMappingCatalog;
