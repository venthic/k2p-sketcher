import {observer} from "mobx-react-lite";
import * as React from "react";
import SpeedDial from '@material-ui/lab/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Tooltip from '@material-ui/core/Tooltip';
import { ThemeProvider } from "@material-ui/styles";
import theme from "../config/Theme.js";

  const useStyles = makeStyles(theme => ({
    root: {
      height: 380,
      transform: 'translateZ(0px)',
      flexGrow: 1,
    },
    speedDial: {
      position: 'absolute',
      top: theme.spacing(2),
      left: theme.spacing(2),
    }
    
  }));
  const tooltipStyles = makeStyles(theme => ({
    tooltip: {
      fontSize: '15px'
    }
  }));
  const LightTooltip = withStyles((theme) => ({
    tooltip: {
      backgroundColor: theme.palette.common.white,
      color: 'rgba(0, 0, 0, 0.87)',
      boxShadow: '0 1px 15px rgba(0,0,0,.04),0 1px 6px rgba(0,0,0,.04)',
      fontSize: 14,
      fontWeight: 500
    },
  }))(Tooltip);

const NodeDial = observer(({context}) => {
    const graph = context;
    const classes = useStyles();
    const tooltipClasses = tooltipStyles();
    const [open, setOpen] = React.useState(false);
    const [hidden, setHidden] = React.useState(false);
    const actions = [
        { icon: <i className="fa fa-poo fa-2x"></i>, name: 'Source', type: 'source', background: '#e3cdcd'  },
        { icon: <i className="fa fa-cog fa-2x"></i>, name: 'Unit Process', type: 'unitProcess', background: '#e3cdcd' },
        { icon: <i className="fa fa-tint fa-2x"></i>, name: 'End Use', type: 'endUse', background: '#e3cdcd' }
      ];
    const handleVisibility = () => {
        setHidden(prevHidden => !prevHidden);
      };
    
      const handleOpen = () => {
        setOpen(true);
      };
    
      const handleClose = () => {
        setOpen(false);
      };

    return(
        
<ThemeProvider theme={theme()}>
<LightTooltip title='Select a type of node for your planned sketch. You can also use
the Clone button to create a duplicate of any given node.'><SpeedDial
        id="nodeDial"
        ariaLabel="Add new node"
        className={classes.speedDial}
        hidden={hidden}
        icon={<SpeedDialIcon />}
        onClose={handleClose}
        onOpen={handleOpen}
        open={open}
        direction='down'
        color='primary'
      >
        {actions.map(action => (
          <SpeedDialAction
            key={action.name}
            icon={action.icon}
            tooltipTitle={action.name}
            tooltipPlacement='right'
            TooltipClasses={tooltipClasses}
            style={{backgroundColor: action.background}}
            className={classes.iconButton}
            onClick={() => graph.addNode(action.type)}
            disabled={graph.calculationComplete}
          />
        ))}
      </SpeedDial>
      </LightTooltip>
      </ThemeProvider>
    )
});

export default NodeDial;
