import {observable, computed, action, reaction} from "mobx";
import {subTypes} from '../config/node-config';

class SketchNodeModel {
    @observable number;
    @observable ntype;
    @observable subType;
    @observable temperature;
    @observable retentionTime;
    @observable matrix;
    @observable flowRate;
    @observable surfaceArea;
    @observable pH;
    @observable concentration;
    @observable contactTime;
    @observable depth;
    @observable dose;
    @observable depth;
    @observable volume;
    @observable ports;
    @observable useCategory;
    @observable soilType;
    @observable depthGroundwater;
    @observable HydraulicLoading;
    @observable solidsRetention;
    @observable typeOfFlow;
    @observable description;
    @observable moistureContent;
    @observable holdingTime;
    @observable name;

    constructor(...props){
        //super(...props);
        this.number = (props[0] || props[0]==0) ? ((props[0]+1)+""):null;
        this.ntype = '';
        this.subType = 'None';
        this.temperature = 0;
        this.surfaceArea = 0;
        this.retentionTime = 0;
        this.flowRate = 0;
        this.volume = 0;
        this.contactTime = 0;
        this.concentration = 0;
        this.pH = 7;
        this.dose = 0;
        this.matrix = 'liquid';
        this.depth = 0;
        this.children = [];
        this.useCategory = '';
        this.soilType = '';
        this.depthGroundwater = 0
        this.HydraulicLoading = 0;
        this.solidsRetention = 0;
        this.typeOfFlow = '';
        this.description = '';
        this.moistureContent = '';
        this.holdingTime = '';
        this.name = this.number;
    }

    @computed get complete() {
        if (this.subType === 'None') {
            return false;
        }
        else {
            const fields = subTypes.filter(type => type.machineName === this.subType)[0].fields.filter(field => field.required);
            for (var i in fields) {
                if ( this[fields[i].propName] === '' || this[fields[i].propName] === 'None') {
                    return false;
                }
            }
        } 
        return true;
    }
    

    @action setProp(name,value){
        this[name] = value;
        if(name=='subType') {
            this.name = this.number + ' ' + value;
            this.resetMatrix(value);
            //refreshCanvas();
        }
    }
    @action resetMatrix(value){
        if(['anaerobic pond', 'facultative pond', 'maturation pond','aerated pond','fws','ss','imhoff tank','anaerobic baffled reactor','media filter','uasb reactor','activated sludge','trickling filter','sewerage','discharge to surface water','irrigation','groundwater recharge'].includes(value)){
            this.matrix = 'liquid';
        }else{
            this.matrix = 'solid';
        }
    }
    @action addChild(id) {
        this.children.push(id);
    }
    @action removeChildren() {
        this.children = [];
    }
    @action removeChild(id) {
        const index = this.children.indexOf(id);
        if (index > -1) {
            this.children.splice(index, 1);
        }
    }
    @action cloneSelf() {
        var newNode = new SketchNodeModel(); 
        newNode.ntype = this.ntype;
        newNode.subType = this.subType;
        newNode.temperature = this.temperature;
        newNode.retentionTime = this.retentionTime;
        newNode.matrix = this.matrix;
        newNode.pH = this.pH;
        newNode.depth = this.depth;
        newNode.contactTime = this.contactTime;
        newNode.concentration = this.concentration;
        newNode.dose = this.dose;
        newNode.flowRate = this.flowRate;
        newNode.surfaceArea = this.surfaceArea;
        newNode.children = [];
        newNode.useCategory = this.useCategory;
        newNode.soilType = this.soilType;
        newNode.volume = this.volume;
        newNode.depthGroundwater = this.depthGroundwater;
        newNode.HydraulicLoading = this.HydraulicLoading;
        newNode.solidsRetention = this.solidsRetention;
        newNode.typeOfFlow = this.typeOfFlow;
        newNode.description = this.description;
        newNode.moistureContent = this.moistureContent;
        newNode.holdingTime = this.holdingTime;
        newNode.name = this.name;
        return newNode;
    }
}
export default SketchNodeModel;
