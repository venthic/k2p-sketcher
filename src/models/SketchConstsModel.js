import GraphConfig, {
    EMPTY_EDGE_TYPE,
    UNIT_PROCESS_TYPE,
    SOURCE_TYPE,
    END_USE_TYPE,
  } from '../config/graph-config';
import SketchNodeModel from './SketchNodeModel';

  const node1 = new SketchNodeModel();
  node1.addChild('2');
  node1.setProp("number","1");
  const node2 = new SketchNodeModel();
  node2.addChild('3');
  node2.setProp("number","2");
  const node3 = new SketchNodeModel();
  node3.setProp("number","3");
export default {    
    edges: [
      {
        handleText: '',
        source: '1',
        target: '2',
        type: EMPTY_EDGE_TYPE,
      },
      {
        handleText: '',
        source: '2',
        target: '3',
        type: EMPTY_EDGE_TYPE,
      }
    ],
    nodes: [
      {
        id: '1',
        title: '1',
        type: SOURCE_TYPE,
        payload: node1,
        x: 100,
        y: 100,
      },
      {
        id: '2',
        title: '2',
        type: UNIT_PROCESS_TYPE,
        payload: node2,
        x: 300,
        y: 300,
      },
      {
        id: '3',
        title: '3',
        type: END_USE_TYPE,
        payload: node3,
        x: 500,
        y: 500,
      }
    ],
  };