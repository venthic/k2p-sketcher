import {observable, computed, action} from "mobx";
import {
	DiagramEngine,
	DiagramModel,
    DefaultPortModel
} from '@projectstorm/react-diagrams';
import SketchConstsModel from './SketchConstsModel';
import SketchNodeModel from './SketchNodeModel';
import Lodash from 'lodash';
import fileDownload from 'js-file-download';
import {SketchNodeFactory} from "./SketchNodeFactory";

class SketchboardModel{
    constructor(root){
        const me = this;
        this.root = root;
        const consts = new SketchConstsModel();



        //LISTENERS
        this.nodeListener = {
            entityRemoved: (e)=>{
                me.removeNode(e.entity);
                e.entity.setProp('selected',false);
                me.selectNode(e.entity);
                me.reorderNodes();
            },
            selectionChanged: (e)=>{
                me.selectNode(e.entity)
            }
        };
        this.modelListener = {
            nodesUpdated: function(e,b){
                console.log('nodes updated',e);
                //e.node.addListener(this.nodeListener)
            },
            linksUpdated: function(e){
                console.log('links updated',e);
                me.links.push(e);
            },
        };
        //PROPS
        this.engine = new DiagramEngine();
        this.engine.installDefaultFactories();
        this.engine.registerNodeFactory(new SketchNodeFactory());
        this.model = new DiagramModel();
        // Set the grid size HERE!
        this.model.setGridSize(consts.gridSize);

        //SET nodes
        for(var i=0;i<consts.nodes.length;i++){
            //console.log(consts.nodes[i].color);
        	var node = new SketchNodeModel(consts.nodes[i].number+' '+consts.nodes[i].ntype,consts.nodes[i].color,consts.nodes[i].number);
            node.setProp('ntype',consts.nodes[i].ntype);
            node.setProp('number',consts.nodes[i].number);
        	node.x = consts.nodes[i].x;
        	node.y = consts.nodes[i].y;
            //ports
        	var source = consts.ports.find((port)=>(port.forNode==i)&&(port.isSource));
			if(source) {
			    node.addPort(new DefaultPortModel(source.isSource, source.from, source.name));
            }
            var target = consts.ports.find((port)=>(port.forNode==i)&&(!port.isSource));
            if(target) {
                node.addPort(new DefaultPortModel(target.isSource, target.from, target.name));
            }
            //listener
            node.addListener(this.nodeListener); //listen123: search this to see other references in this file
            //add to model
            this.nodes.push(node);
            this.model.addNode(node);
		}
		//SET links
		for(var i=0;i<consts.links.length;i++){
		    var link;
		    var sourcePort = this.nodes[consts.links[i].from].getOutPorts().find((port)=>{return port;});
		    var outPort = this.nodes[consts.links[i].to].getInPorts().find((port)=>{return port;});
		    if(sourcePort&&outPort){
                link = sourcePort.link(outPort);
                //listener
                link.addListener({selectionChanged: (e)=>{}}); //listen345: search this to see other references in this file
                //add to model
                this.links.push(link);
                this.model.addLink(link);
            }
        }

        //listeners for the new nodes and links
        this.model.addListener(this.modelListener);

        this.engine.setDiagramModel(this.model);
        setTimeout ( function () {
            refreshCanvas();
        },200);

    }
    //CLASS
    @observable.ref model = null;
    @observable sendToCatalogueModal = false;
    @observable isSendingToCatalogue = false;
    @observable name;
    @observable title;
    @observable description;
    @observable keywords;
    @observable extraNodes = false;
    @observable influentNode;
    @observable effluentNode;
    @observable.ref nodes = [];
    @observable.ref links = [];
    @observable.ref selectedNode = null;
    @computed get exportNodes(){
        return this.nodes.map((node)=>{
            return {
                name:node.name,
                parents:Object.keys(node.ports).filter((port)=>node.ports[port].in).map((port)=>Object.keys(node.ports[port].links).map((link) => { return node.ports[port].links[link].sourcePort.parent.name})),
                children:Object.keys(node.ports).filter((port)=>(!node.ports[port].in)).map((port)=>Object.keys(node.ports[port].links).map((link) => node.ports[port].links[link].targetPort.parent.name)),
                ntype:node.ntype,
                subType:node.subType,
                temperature: node.temperature,
                retentionTime: node.retentionTime,
                depth: node.depth,
                useCategory: node.useCategory ,
                soilType: node.soilType ,
                depthGroundwater: node.depthGroundwater ,
                HydraulicLoading: node.HydraulicLoading ,
                solidsRetention: node.solidsRetention ,
                typeOfFlow: node.typeOfFlow ,
                description: node.description ,
                moistureContent: node.moistureContent,
                holdingTime: node.holdingTime,
                matrix: node.matrix,
                number: node.number,
                influentNode: (node.number == this.influentNode),
                effluentNode: (node.number == this.effluentNode)
            }
        });
    }
    @computed get nodePropsVisible(){
        return this.selectedNode !== null;
    }
    @action toggleExtraNodes(){
        this.extraNodes = !this.extraNodes;
    }
    @action editName(value){
        this.name = value;
    }
    @action editTitle(value){
        this.title = value;
    }
    @action editDescription(desc){
        this.description = desc;
    }
    @action editKeywords(words){
        this.keywords = words;
    }
    @action setCatalogueModalState(state){
        this.sendToCatalogueModal = state;
    }
    @action setSendingToCatalogue(value){
        this.isSendingToCatalogue = value;
    }
    @action postData(callback){
        const me = this;
        var json = {sketch:this.model.serializeDiagram(), details:this.exportNodes};
        const dataToSend = {
            name:this.title.replace(/\s/g, '').toLowerCase(),
            title:this.title,
            description:this.description,
            author:'k2p sketch tool',
            keywords:this.keywords,
            location:'Uganda',
            fileJson: json,
        };
        me.setCatalogueModalState(true);
        fetch('http://server.waterpathogens.org/flowtool/sendSketchToCatalogue',{
            method: "POST",
            credentials: "include",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(dataToSend)
        }).then((res)=>res.json()).then((data)=>{
            if(data.response=="name_exists"){
                alert('This name already exists, please write another name for your dataset and try again.');
            }else{
                //setTimeout(function(){document.location.href = `http://${ window.location.host }`},500);
                me.setCatalogueModalState(false);
                if(callback) callback();
            }
        });
    }
    @action load(str){
        var json = JSON.parse(str);
        console.log(json);
        this.model  = new DiagramModel();
        this.model.deSerializeDiagram(json.sketch, this.engine);
        this.engine.setDiagramModel(this.model);
        this.nodes = [];
        for (var node in this.model.nodes){
            this.model.nodes[node].addListener(this.nodeListener);
            var details = json.details.find((det)=>det.name==this.model.nodes[node].name);
            for(var data in details){
                console.log(data);
                this.model.nodes[node][data] = details[data];
            }
            this.nodes.push(this.model.nodes[node]);
        }
        console.log(this.nodes);
        refreshCanvas();
    }
    @action save(){
        var json = {sketch:this.model.serializeDiagram(), details:this.exportNodes};
        var jsonBlob = new Blob([JSON.stringify(json)],{type:'application/json'});
        fileDownload(jsonBlob, 'saved.json');
    }

    @action download(){
        const json = this.engine.getDiagramModel().serializeDiagram();
        const validLinks = !json.links.find((link)=>(!link.source)||(!link.target));
        const validNodes = !json.nodes.find((node)=>node.ports.find((port)=>port.links.length==0));
       /* CSV
            const fields =
            [
            'name',
            'type',
            'subtype',
            'temperature',
            'volume'
            ]
        ;
        const opts = { fields };*/
        if(validLinks&&validNodes) {
           console.log(this.exportNodes);
            try {
                /* CSV
                const csv = json2csv(this.exportNodes, opts);
                var csvBlob = new Blob([csv], {type: 'text/csv'});
                fileDownload(csvBlob, 'sketch.csv');*/
                var jsonBlob = new Blob([JSON.stringify(this.exportNodes)],{type:'application/json'});
                fileDownload(jsonBlob, 'sketch.json');
            } catch (err) {
                console.error(err);
            }
        } else {
            alert('There was an error parsing your diagram. Please make sure all nodes are connected.');
        }
    }
    @action runModel(cb){
        var me = this;
        //validate
        const json = this.engine.getDiagramModel().serializeDiagram();
        const validLinks = !json.links.find((link)=>(!link.source)||(!link.target));
        const validNodes = !json.nodes.find((node)=>node.ports.find((port)=>port.links.length==0));
        const validNodeTypes = !me.nodes.find(node => node.subType==="None");
        if(validLinks&&validNodes) {
            if(validNodeTypes) {
                fetch(process.env.REACT_APP_SERVER + 'flowtool/treatment', {
                    method: "POST",
                    credentials: "include",
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(this.exportNodes)
                }).then((res) => res.json()).then((data) => {
                    console.log(data);
                    if (data.response == 'err') {
                        alert(data.error);
                    } else {
                        if (cb) cb();
                    }
                });
            } else {
                alert('There are nodes that have not been sufficiently described. Please fix your diagram and run the model again.')
            }
        } else {
            alert('There are unlinked nodes or arrows. Please fix your diagram and run the model again.');
        }
    }
    @action removeNode(node){
        this.nodes = this.nodes.filter((n)=>n!==node);
        console.log(node,'nodes removed',this.nodes);
    }
    @action reorderNodes(){
        console.log('nn');
        this.nodes.map((node,index)=>{
            node.setProp('number',index+1);
            node.setProp('name',index+1+' '+((node.subType!="None")?node.subType:node.ntype))

            console.log(index,index+1+' ',node);
        });
    }
    @action selectNode(node){
        if(!node.selected) {
            this.selectedNode = null;
        }
        if(node.selected) {
            this.selectedNode = node;
        }
    }
    onSketchDrop(event){
        var data = JSON.parse(event.dataTransfer.getData('storm-diagram-node'));
        var nodesCount = Lodash.keys(this.engine.getDiagramModel().getNodes()).length;
        var node = null;
        if (data.type === 'out') {
            node = new SketchNodeModel((nodesCount + 1) + ' source', 'rgb(145, 103, 103)');
            node.addPort(new DefaultPortModel(false, 'out-1', 'Out'));
            node.setProp('ntype','source');
            node.setProp('number',nodesCount + 1);
        } else if  (data.type === 'in') {
            node = new SketchNodeModel((nodesCount + 1) + ' end use', 'rgb(0, 178, 134)');
            node.addPort(new DefaultPortModel(true, 'in-1', 'In'));
            node.setProp('ntype','end use');
            node.setProp('number',nodesCount + 1);
        } else {
            node = new SketchNodeModel((nodesCount + 1) + ' unit process', 'rgb(114, 179, 201)');
            node.addPort(new DefaultPortModel(true, 'in-1', 'In'));
            node.addPort(new DefaultPortModel(false, 'out-1', 'Out'));
            node.setProp('ntype','unit process');;
            node.setProp('number',nodesCount + 1);
        }
        var points = this.engine.getRelativeMousePoint(event);
        node.x = points.x;
        node.y = points.y;
        node.addListener(this.nodeListener);
        this.nodes.push(node);
        this.model.addNode(node);
        refreshCanvas();
    }
}
export function refreshCanvas () {
    var node = document.getElementsByClassName('srd-diagram')[0];
    if (node ){
        var clickEvent = document.createEvent ('MouseEvents');
        clickEvent.initEvent ("mousedown", true, true);
        node.dispatchEvent (clickEvent);
        clickEvent.initEvent ("mouseup", true, true);
        node.dispatchEvent (clickEvent);
    }

}
export default SketchboardModel;
